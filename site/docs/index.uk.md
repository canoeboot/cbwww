---
title: Документація
...

Завжди перевіряйте Canoeboot для останніх оновлень
Canoeboot. Новини, включаючи оголошення про випуски, може бути знайдено
в [основній секції новин](../news/).

[Відповіді на поширені запитання про Canoeboot](../faq.md).

What is Canoeboot? An article is available for that; please read the
article titled [What is Canoeboot?](../about.md).

Встановлення Canoeboot
=====================

-   [На яких системах я можу встановлювати Canoeboot?](hardware/)
-   [Як встановити Canoeboot](install/)

Документація, яка має відношення до операційних систем
============================

-   [Як встановити BSD на x86 хостову систему](bsd/)
-   [Керівництва GNU+Linux](gnulinux/)

Інформація для розробників
==========================

-   [Як зібрати джерельний код Canoeboot](build/)
-   [Документація розробника системи побудови](maintain/)
-   [Корисне навантаження GRUB](grub/)
-   [Корисне навантаження U-Boot](uboot/)

Інша інформація
=================

-   [Різне](misc/)
-   [Список кодових назв](misc/codenames.md)

If you're using *release archives*, this documentation and accompanying images
are under `src/www/` and `src/img/` respectively, in the source release archive.
This fact is true for Canoeboot 20231026 and newer.
