---
title: ThinkPad R500
x-toc-enable: true
...

<div class="specs">
<center>
![ThinkPad R500]()
</center>

| ***Specifications***       |                                                |
|----------------------------|------------------------------------------------|
| **Manufacturer**           | Lenovo                                         |
| **Name**                   | ThinkPad R500                                  |
| **Released**               | 2009                                           |
| **Chipset**                | Intel Cantiga GM45                             |
| **CPU**                    | Intel Core 2 Duo (Penryn/Merom family) or 
                                                   Celeron M (Merom L family) |
| **Graphics**               | Intel GMA 4500MHD (or ATI Mobility Radeon HD 
                                                         3470 on some models) |
| **Display**                | 1280x800/1680x1050 TFT                         |
| **Memory**                 | 512MB, 2GB or 4GB (Upgradable to 8GB)          |
| **Architecture**           | x86_64                                         |
| **EC**                     | Proprietary                                    |
| **Original boot firmware** | LenovoBIOS                                     |
| **Intel ME/AMD PSP**       | Present. Can be completly disabled.            |
| **Flash chip**             | SOIC-8/SOIC-16/WSON-8 4MiB/8MiB (Upgradable 
                                                                    to 16MiB) |
```
W+: Works; 
N: Doesn't work; 
U: Untested; 
P+: Partially works; 
```

| ***Features*** |                                       |
|----------------|---------------------------------------|
| **Internal flashing with original boot firmware** | N  |
| **Display**                                       | W+ |
| **Audio**                                         | W+ |
| **RAM Init**                                      | W+ |
| **External output**                               | W+ |
| **Display brightness**                            | P+ | 

| ***Payloads supported***  |           |
|---------------------------|-----------|
| **GRUB**              | Works     |
| **SeaBIOS**               | Works     |
| **SeaBIOS with GRUB** | Works     |
</div>
Dell Latitude E6400
===================

**If you haven't bought an R500 yet: the [Dell Latitude
E6400](e6400.md) is much easier to flash; no disassembly required,
it can be flashed entirely in software from Dell BIOS to Canoeboot. It is the
same hardware generation (GM45), with same CPUs, video processor, etc.**

Introduction
============

This board as basically identical to the T500, and has very similar disassembly.
You must take it apart and flash the chip externally.

The chip is 4MiB NOR flash (SPI protocol) is SOIC8 form factory.

Refer to the following guide:\
[Externally rewrite 25xx NOR flash via SPI protocol](../install/spi.md)

Unlike other GM45+ICH9M thinkpads in Canoeboot, the R500 doesn't have an Intel
PHY (for Gigabit Ethernet). However, Canoeboot still includes an Intel flash
descriptor, but with just the descriptor and BIOS region. The `ich9gen` program
supports this fully.

Therefore, you do not have to worry about the MAC address. The onboard NIC for
ethernet is made by Broadcom (and works in linux-libre).

Refer to T500 disassembly guide. The R500 disassembly procedure is almost
identical.
