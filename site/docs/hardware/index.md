---
title: Hardware compatibility list 
x-toc-enable: true
...

This sections relates to known hardware compatibility in Canoeboot.

For installation instructions, refer to [../install/](../install/).

NOTE: For T60/R60 thinkpads, make sure that it has an Intel GPU, not an ATI GPU
because coreboot lacks native video initialization for the ATI GPUs on these
machines.

(for later machines like T500, T400, ATI GPU doesn't matter, because it also
has an Intel GPU, and Canoeboot uses the Intel one)

Supported hardware
==================

Canoeboot currently supports the following systems:

### Servers (AMD, Intel, x86)

-   [ASUS KGPE-D16 motherboard](kgpe-d16.md)
-   [ASUS KFSN4-DRE motherboard](kfsn4-dre.md)

### Desktops (AMD, Intel, x86)

-   [ASUS KCMA-D8 motherboard](kcma-d8.md)
-   [Gigabyte GA-G41M-ES2L motherboard](ga-g41m-es2l.md)
-   [Acer G43T-AM3](acer_g43t-am3.md)
-   [Intel D510MO and D410PT motherboards](d510mo.md)
-   [Apple iMac 5,2](imac52.md)
-   [Intel D945GCLF](d945gclf.md)

### Laptops (Intel, x86)

-   **[Dell Latitute E6400, E6400 XFR and E6400 ATG, with Intel
    GPU](e6400.md) (easy to flash, no disassembly, similar
    hardware to X200/T400)**
-   ThinkPad X60 / X60S / X60 Tablet
-   ThinkPad T60 (with Intel GPU)
-   [Lenovo ThinkPad X200 / X200S / X200 Tablet](x200.md)
-   Lenovo ThinkPad X301
-   [Lenovo ThinkPad R400](r400.md)
-   [Lenovo ThinkPad T400 / T400S](t400.md)
-   [Lenovo ThinkPad T500](t500.md)
-   [Lenovo ThinkPad W500](t500.md)
-   [Lenovo ThinkPad R500](r500.md)
-   [Apple MacBook1,1 and MacBook2,1](macbook21.md)

### Laptops (ARM, with U-Boot payload)

-   [ASUS Chromebook Flip C101 (gru-bob)](../install/chromebooks.md)
-   [Samsung Chromebook Plus (v1) (gru-kevin)](../install/chromebooks.md)

### Emulation

-   [Qemu x86](../misc/emulation.md)
-   [Qemu arm64](../misc/emulation.md)

TODO: More hardware is supported. See `config/coreboot/` in cbmk. Update
the above list!

'Supported' means that the build scripts know how to build ROM images
for these systems, and that the systems have been tested (confirmed
working). There may be exceptions; in other words, this is a list of
'officially' supported systems.

EC update on i945 (X60, T60) and GM45 (X200, X301, T400, T500, R400, W500, R500)
==============================================================

It is recommended that you update to the latest EC firmware version. The
[EC firmware](../../faq.md#ec-embedded-controller-firmware) is separate from
Canoeboot, so we don't actually provide that, but if you still have
Lenovo BIOS then you can just run the Lenovo BIOS update utility, which
will update both the BIOS and EC version. See:

-   [../install/#flashprog](../install/#flashprog)
-   <http://www.thinkwiki.org/wiki/BIOS_update_without_optical_disk>

NOTE: Canoeboot standardises on [flashprog](https://flashprog.org/wiki/Flashprog)
now, as of 3 May 2024, which is a fork of flashrom.

NOTE: this can only be done when you are using Lenovo BIOS. How to
update the EC firmware while running Canoeboot is unknown. Canoeboot
only replaces the BIOS firmware, not EC.

Updated EC firmware has several advantages e.g. better battery
handling.

How to find what EC version you have (i945/GM45)
------------------------------------------------

In GNU+Linux, you can try this:

	grep 'at EC' /proc/asound/cards

Sample output:

	ThinkPad Console Audio Control at EC reg 0x30, fw 7WHT19WW-3.6

7WHT19WW is the version in different notation, use search engine to find
out regular version - in this case it's a 1.06 for x200 tablet

Alternatively, if `dmidecode` is available, run the following command (as `root`) to
find the currently flashed BIOS version:

	dmidecode -s bios-version

On a T400 running the latest BIOS this would give `7UET94WW (3.24 )` as result.
