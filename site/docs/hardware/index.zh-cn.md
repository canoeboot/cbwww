---
title: 兼容硬件列表
x-toc-enable: true
...

这一部分说明了 canoeboot 已知兼容的硬件。

安装指南，请参看 [../install/](../install/)。

注意：对 T60/R60 thinkpad 而言，请确认它拥有的是 Intel GPU 而非 ATI GUI，因为 coreboot 对这些机器缺少 ATI GPU 的原生图像初始化。

（对 T500、T400 等后续机器而言，有 ATI GPU 也没问题，因为它也有 Intel GPU，而 canoeboot 会用 Intel 的）

已支持的硬件
==================

该版本的 canoeboot 目前支持以下机器：

### 服务器（AMD，x86）

-   [ASUS KFSN4-DRE 主板](kfsn4-dre.md)
-   [ASUS KGPE-D16 主板](kgpe-d16.md)

### Desktops (AMD, Intel, x86)

-   [Acer G43T-AM3](acer_g43t-am3.md)
-   [Apple iMac 5,2](imac52.md)
-   [ASUS KCMA-D8 主板](kcma-d8.md)
-   [Gigabyte GA-G41M-ES2L 主板](ga-g41m-es2l.md)
-   [Intel D510MO 及 D410PT 主板](d510mo.md)
-   [Intel D945GCLF](d945gclf.md)

### 笔记本（Intel，x86）

-   [Apple MacBook1,1 及 MacBook2,1](macbook21.md)
-   [Dell Latitude E6400, E6400 XFR 及 E6400 ATG，皆支持 Nvidia 或 Intel GPU](e6400.md) **（刷入简单，无需拆解，硬件类似 X200/T400）**
-   [Lenovo ThinkPad R400](r400.md)
-   [Lenovo ThinkPad R500](r500.md)
-   [Lenovo ThinkPad T400 / T400S](t400.md)
-   [Lenovo ThinkPad T500](t500.md)
-   Lenovo ThinkPad T60（Intel GPU 款）
-   [Lenovo ThinkPad W500](t500.md)
-   [Lenovo ThinkPad X200 / X200S / X200 Tablet](x200.md)
-   Lenovo ThinkPad X301
-   Lenovo ThinkPad X60 / X60S / X60 Tablet

### 笔记本（ARM，配 U-Boot payload）

-   [ASUS Chromebook Flip C101 (gru-bob)](../install/chromebooks.md)
-   [Samsung Chromebook Plus (v1) (gru-kevin)](../install/chromebooks.md)

### 模拟

-   [Qemu x86](../misc/emulation.md)
-   [Qemu arm64](../misc/emulation.md)


计划：支持更多硬件。见 cbmk 中的 `config/coreboot/`。更新上面的列表！

所谓“支持”，即指构建脚本知道如何构建这些机器的 ROM 镜像，并且机器经过测试（确认能够工作）。也可能会有例外；换言之，这是“官方”支持的机器列表。

在 i945（X60、T60）及 GM45（X200、X301、T400、T500、R400、W500、R500）上更新 EC
==============================================================

建议更新到最新 EC 固件版本。[EC 固件](../../faq.md#ec-embedded-controller-firmware) 与 canoeboot 是独立的，所以我们实际上并不会提供这些固件，但如果你仍还有 Lenovo BIOS，那你可以直接运行 Lenovo BIOS 更新工具，它会同时更新 BIOS 和 EC 版本。见：

-   [../install/#flashrom](../install/#flashrom)
-   <http://www.thinkwiki.org/wiki/BIOS_update_without_optical_disk>

注意：只有在运行 Lenovo BIOS 的时候，你才能这样做。如何在运行 canoeboot 的时候更新 EC 固件尚不清楚。canoeboot 只会替换 BIOS 固件，而不会替换 EC。

更新的 EC 固件有一些好处，例如电池管理更加好。

如何得知你的 EC 版本（i945/GM45）
------------------------------------------------

在 Linux，你可以试试这条命令：

	grep 'at EC' /proc/asound/cards

输出样例：

	ThinkPad Console Audio Control at EC reg 0x30, fw 7WHT19WW-3.6

7WHT19WW 另一种形式的版本号，使用搜索引擎来找出正常的版本号——这个例子中的 x200 tablet，版本号是 1.06。

或者，如果能用 `dmidecode`，则（以 `root`）运行以下命令，来得知目前刷入的 BIOS 版本：

	dmidecode -s bios-version

运行最新 BIOS 的 T400 上，它的输出结果为 `7UET94WW (3.24 )`。
