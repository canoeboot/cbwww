---
title: Apple iMac 5,2 
...

<div class="specs">
<center>
![iMac5,2]()
</center>

| ***Specifications***       |                                                |
|----------------------------|------------------------------------------------|
| **Manufacturer**           | Apple                                          |
| **Name**                   | iMac 17-inch "Core 2 Duo" 1.83                 |
| **Released**               | 2006                                           |
| **Chipset**                | Intel Calistoga 945GM                          |
| **CPU**                    | Intel Core 2 Duo T5600                         |
| **Graphics**               | Intel GMA 950                                  |
| **Display**                | 1440x900 TFT                                   |
| **Memory**                 | 512MB, 1GB (upgradable to 2GB)                 |
| **Architecture**           | x86_64                                         |
| **EC**                     | Proprietary                                    |
| **Original boot firmware** | Apple EFI                                      |
| **Intel ME/AMD PSP**       | Not present.                                   |
| **Flash chip**             | SOIC-8 2MiB (Probably upgradable to 16MiB)     |

```
W+: Works; 
N: Doesn't work; 
U: Untested; 
P+: Partially works; 
```

| ***Features*** |                                       |
|----------------|---------------------------------------|
| **Internal flashing with original boot firmware** | U  |
| **Display**                                       | U  |
| **Audio**                                         | U  |
| **RAM Init**                                      | U  |
| **External output**                               | U  |
| **Display brightness**                            | U  |

| ***Payloads supported***  |           |
|---------------------------|-----------|
| **GRUB**              | Works     |
| **SeaBIOS**               | Works     |
| **SeaBIOS with GRUB** | Works     |
</div>
Information to be written soon, but this board is merged in Canoeboot.

This board is very similar to the [MacBook2,1](./macbook21.md).

Just refer back to the [hardware section](./) and [install guides](../install/)
