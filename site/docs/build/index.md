---
title: Compile Canoeboot from source
x-toc-enable: true
...

If you need to build Canoeboot from source, this guide is for you.

Open source BIOS/UEFI firmware
------------

Canoeboot's build system is named `cbmk`, short for `CanoeBoot MaKe`, and this
document describes how to use it. With this guide, you can know how to compile
canoeboot from the available source code.

The following document describes how `cbmk` works, and how you can make changes
to it: [canoeboot maintenance manual](../maintain/)

### WARNING: eCryptfs file name limits

Do not run the build system on a eCryptfs file system, because it has
very short file name limits and Canoeboot's build system deals with very
long file names. We commonly get reports from this by Linux Mint users
who encrypt their home directory with eCryptfs; regular LUKS encryption will
do nicely.

System requirements
-------------------

You must ensure that you have the correct operating system, CPU, RAM, disk space
and so on.

System requirements are documented in
the [cbmk maintenance manual](../maintain/#system-requirements).

Multi-threaded builds
---------------------

Canoeboot's build system defaults to a single build thread, but you can change
it by doing e.g.

	export XBMK_THREADS=4

This would make cbmk run on 4 threads.

More specifically: when compiling source trees via `script/trees`, `-jTHREADS`
is passed, where THREADS is the number of threads. This is also set when running
xz commands for compression, using the `-t` option.

Environmental variables
-----------------------

Please read about environmental variables in [the build
instructions](../maintain/), before running cbmk. You should set
your variables accordingly, though you do not technically need to; some
of them may be useful, e.g. `XBMK_THREADS` (sets the number of build threads).

Sources
-------

This version, if hosted live on canoeboot.org, assumes that you are using
the `cbmk` git repository, which
you can download using the instructions on [the code review page](../../git.md).

Git
---

Canoeboot's build system uses Git, extensively. You should perform the steps
below, *even if you're using a release archive*.

Before you use the build system, please know: the build system itself uses
Git extensively, when downloading software like coreboot and patching it.

You should make sure to initialize your Git properly, before you begin or else
the build system will not work properly. Do this:

	git config --global user.name "John Doe"
	git config --global user.email johndoe@example.com

Change the name and email address to whatever you want, when doing this.

You may also want to follow more of the steps here:
<https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup>

How to compile Canoeboot

Actual development/testing is always done using cbmk directly, and this
includes when building from source. Here are some instructions to get you
started:

### Zero..st, check time/date

Make sure date/hwclock report the correct time and date on your system,
because parts of the build process download from HTTPS servers and wrong
time or date can cause connections to be dropped during negotiation.

### First, install build dependencies

Check `config/dependencies/` for list of supported distros.

Canoeboot includes a script that automatically installs build dependencies
according to the selected Linux distro. 

For example:

	./mk dependencies ubuntu

or

	./mk dependencies debian

or

	./mk dependencies fedora41

or

	./mk dependencies arch

NOTE: For versioned files, such as `fedora41`, typically other versions will
be available too, e.g. `fedora38`. Make sure to check `config/dependencies/`,
so that you know whether or not a file is available for your distro.

NOTE: In case of Ubuntu 20.04 LTS or derived distros for that specific release,
use the dedicated configuration file:

	./mk dependencies ubuntu2004

Technically, any Linux distribution can be used to build canoeboot.
However, you will have to write your own script for installing build
dependencies. 

### Debian Trixie/Sid

Debian Trixie, the testing release as of 3 January 2025, and Debian Sid,
provide `gnat` and `gcc` as you expect, but `gnat` resolves to `gnat-13` and
installs `gcc-13` as a dependency, while `gcc` resolves to `gcc-14` and other
toolchain components correspond to this version.

The GCC/GNAT versions need to match during build time, so Canoeboot's build
system hacks the `PATH` environmental variable, setting up symlinks, matching
GNAT to GCC or GNAT to GCC. When you run `./mk dependencies debian`, you get
GNAT 13 and GCC 14. This seems to make most boards compile; in our testing, the
KGPE-D16 board failed to compile in this configuration. This PATH hack is only
done for compiling the coreboot crossgcc toolchain, and nothing else; after that,
coreboot's toolchain is used.

GNAT is used by coreboot, because some of the Intel graphics devices are
initialised natively, with code written in Ada spark (called `libgfxinit`).

When updating from Debian stable to Debian Trixie(testing) or Sid, you should
also check for orphaned packages, using `aptitude search '~o'`. Do this,
removing what was leftover from the old release, and make sure to re-run the
Debian dependencies script, but do it like this:

	./mk dependencies debian --reinstall

For better reliability, you should, after running the dependencies script,
remove `gnat` and install `gnat-14` instead, which is available on this day
of 3 December 2025, but currently marked experimental. When you install
GNAT 14, GNAT 13 is removed but `gnat` (in `PATH`) still won't resolve to
anything. Canoeboot *still* accomodates this, detecting and matching the GCC
and GNAT versions, which would in this instance match version 14 between them,
so that `gnat` and `gcc` are both in PATH at build time, resolving to v14.x.
When we tested with this configuration, the KGPE-D16 images also compiled.

NOTE: Ubuntu 24.10 also has the issue described above. Some other distros may
also have it, if they're based on Debian Testing/Sid or Ubuntu 24.10.

### MIPS cross compiler

Canoeboot has support for the Sony PlayStation (PS1/PSX), based on
the PCSX-Redux Open BIOS. If you're doing a full release build, and/or
specifically building the PSX BIOS, you need a MIPS cross compiler.

Arch-based systems have a mipsel cross compiler available from AUR, and most
Debian-based systems have a mipsel cross compiler in apt; for these, the normal
dependencies installation command will provide them. We know Void Linux and
Fedora don't have a MIPS compiler, for instance.

If your distro doesn't have the MIPS compiler available,
the [PlayStation](../install/playstation.md) page provides instructions for
manual installation; please do this in addition to the normal dependencies.

### Next, build ROM images

Canoeboot MaKe (cbmk) automatically runs all necessary commands; for
example, `./mk -b coreboot` will automatically build the required payloads
if not already compiled.

As a result, you can now (after installing the correct build dependencies) run
just a single command, from a fresh Git clone, to build all ROM images:

	./mk -b coreboot

or even just build specific ROM images, e.g.:

	./mk -b coreboot x60

or get a list of supported build targets:

	./mk -b coreboot list

### Or maybe just build payloads?

If you wish to build payloads, you can also do that. For example:

	./mk -b grub

	./mk -b seabios

	./mk -b u-boot

Previous steps will be performed automatically. However, you can *still* run
individual parts of the build system manually, if you choose. This may be
beneficial when you're making changes, and you wish to test a specific part of
cbmk.

Want to modify Canoeboot?
-------------------------

Check the [cbmk maintenance manual](../maintain/) for guidance. You may for
example want to modify a config, e.g.:

	./mk -m coreboot x200_8mb

Or perhaps add a new board! The maintenance manual will teach you how the
Canoeboot build system (cbmk) works!

A note about documentation (and this page)
-------------------------------

So you compiled your Canoeboot image? Congratulations!

Before you flash, please make sure that you *dumped* two copies of the original
firmware just in case (verifying the hashes of each dump, to ensure that they
match), using the `-r` option in flashprog.

NOTE: Canoeboot standardises on [flashprog](https://flashprog.org/wiki/Flashprog)
now, as of 3 May 2024, which is a fork of flashrom.
