---
title: Build from source
x-toc-enable: true
...

WARNING: Flash from bin/, NOT elf/
==================================

**WARNING: When you build a ROM image from the Canoeboot build system, please
ensure that you flash the appropriate ROM image from `bin/`, NOT `elf/`.
The `elf/` coreboot ROMs do not contain payloads. Canoeboot's build system
builds no-payload ROMs under `elf/`, and payloads separately under `elf/`. Then
it copies from `elf/` and inserts payloads from `elf/`, and puts the final ROM
images (containing payloads) in `bin/`. This design is more efficient, and
permits many configurations without needless duplication of work. More info
is available in the [cbmk maintenance manual](../maintain/)**

Introduction
============

Canoeboot's build system is named `cbmk`, short for `CanoeBoot MaKe`, and this
document describes how to use it. With this guide, you can know how to compile
canoeboot from the available source code.

The following document describes how `cbmk` works, and how you can make changes
to it: [canoeboot maintenance manual](../maintain/)

Sources
=======

This version, if hosted live on canoeboot.org, assumes that you are using
the `cbmk` git repository, which
you can download using the instructions on [the code review page](../../git.md).

A note about documentation (and this page)
------------------------------------------

Including Canoeboot 20231026 and newer, *all* releases have `cbwww.git` (the
website) and `cbwww-img.git` (images for the website) archived in the *src* tar
archive for that release; Canoeboot documentation is written in Markdown (pandoc
variant). You can find markdown files and images under `src/www/`
and `src/img/`, respectively.

If you're working with *release* documentation, you don't get the full HTML
files (such as the one you're viewing now, if you're reading *this* page in a
web browser), so either read the Markdown files directly, or compile them to
HTML using the [Untitled Static Site Generator](https://untitled.vimuser.org/)
(which is what the Canoeboot project uses to generate HTML from those files).

NOTE: `av.canoeboot.org` is hardcoded as the domain name where images are
pointed to, in `cbwww.git`, so you will need to replace these references in
your local version, unless you're happy to just continue using those.

Git
===

Canoeboot's build system uses Git, extensively. You should perform the steps
below, *even if you're using a release archive*.

Before you use the build system, please know: the build system itself uses
Git extensively, when downloading software like coreboot and patching it.

You should make sure to initialize your Git properly, before you begin or else
the build system will not work properly. Do this:

	git config --global user.name "John Doe"
	git config --global user.email johndoe@example.com

Change the name and email address to whatever you want, when doing this.

You may also want to follow more of the steps here:
<https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup>

Python
======

You should ensure that the `python` command runs python 3, on your system.
Python2 is unused by cbmk or anything that it pulls down as modules. 

If building on Debian/Ubuntu based systems, you can achieve that via:

	sudo apt install python-is-python3

On Fedora, you can use the following

	sudo dnf install python-unversioned-command

How to compile Canoeboot
========================

Actual development/testing is always done using cbmk directly, and this
includes when building from source. Here are some instructions to get you
started:

First, install build dependencies
---------------------------------

Canoeboot includes a script that automatically installs build dependencies
according to the selected linux distro. 
The currently supported distros are: Debian/Ubuntu/Linux Mint/Pop!\_OS, 
Fedora, Arch Linux/Parabola or Void Linux.

Some examples (run them as root, use use e.g. `sudo`, `doas`):

	./build dependencies ubuntu

or

	./build dependencies debian

or

	./build dependencies fedora38

or

	./build dependencies arch

NOTE: In case of Ubuntu 20.04 LTS or derived distros for that specific release,
use the dedicated configuration file:

	./build dependencies ubuntu2004

Check: `config/dependencies/` for list of supported distros.

Technically, any Linux distribution can be used to build canoeboot.
However, you will have to write your own script for installing build
dependencies. 

Next, build ROM images
----------------------

Canoeboot MaKe (cbmk) automatically runs all necessary commands; for
example, `./build roms` will automatically run `./build grub`
if the required GRUB payload (under `elf/grub/`) does not exist.

As a result, you can now (after installing the correct build dependencies) run
just a single command, from a fresh Git clone, to build all ROM images:

	./build roms all

or even just build specific ROM images, e.g.:

	./build roms x60

or get a list of supported build targets:

	./build roms list

Or maybe just build payloads?
-----------------------------

If you wish to build payloads, you can also do that. For example:

	./build grub

	./update trees -b seabios

	./update trees -b u-boot

Previous steps will be performed automatically. However, you can *still* run
individual parts of the build system manually, if you choose. This may be
beneficial when you're making changes, and you wish to test a specific part of
cbmk.

Want to modify Canoeboot?
-------------------------

Check the [cbmk maintenance manual](../maintain/) for guidance. You may for
example want to modify a config, e.g.:

	./update trees -m coreboot x200_8mb

Or perhaps add a new board! The maintenance manual will teach you how the
Canoeboot build system (cbmk) works!
