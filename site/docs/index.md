---
title: Installing Canoeboot Free/Opensource BIOS/UEFI firmware
...

Always check the Canoeboot website for the latest updates to
Canoeboot. News, including release announcements, can be found in
the [main news section](../news/).

[Answers to Frequently Asked Questions about Canoeboot](../faq.md).

Need help?
----------

Help is available on [Canoeboot IRC](../contact.md) and other channels.

Installing Canoeboot
--------------------

-   [How to install Canoeboot](install/)

Installing operating systems
----------------------------

-   [Install BSD operating systems on Canoeboot](bsd/)
-   [Install Linux on a Canoeboot system](linux/)

Information for developers
--------------------------

-   [How to compile the Canoeboot source code](build/)
-   [Build system developer documentation](maintain/)
-   [GRUB payload](grub/)
-   [U-Boot payload](uboot/)

Other information
-----------------

-   [Miscellaneous](misc/)
-   [List of codenames](misc/codenames.md)

If you're using *release archives*, this documentation and accompanying images
are under `src/www/` and `src/img/` respectively, in the source release archive.
This fact is true for Canoeboot 20231026 and newer.
