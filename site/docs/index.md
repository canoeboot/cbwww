---
title: Documentation
...

Always check the Canoeboot website for the latest updates to
Canoeboot. News, including release announcements, can be found in
the [main news section](../news/).

[Answers to Frequently Asked Questions about Canoeboot](../faq.md).

What is Canoeboot? An article is available for that; please read the
article titled [What is Canoeboot?](../about.md).

Installing Canoeboot
====================

-   [What systems can I use Canoeboot on?](hardware/)
-   [How to install Canoeboot](install/)

Documentation related to operating systems
============================

-   [How to install BSD operating systems](bsd/)
-   [How to install GNU+Linux](gnulinux/) (and other Linux-based systems)

Information for developers
==========================

-   [How to compile the Canoeboot source code](build/)
-   [Build system developer documentation](maintain/)
-   [GRUB payload](grub/)
-   [U-Boot payload](uboot/)

Other information
=================

-   [Miscellaneous](misc/)
-   [List of codenames](misc/codenames.md)

If you're using *release archives*, this documentation and accompanying images
are under `src/www/` and `src/img/` respectively, in the source release archive.
This fact is true for Canoeboot 20231026 and newer.
