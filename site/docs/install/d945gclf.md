---
title: Intel D945GCLF flashing tutorial 
...

This guide is for those who want Canoeboot on their Intel D945GCLF
motherboard while they still have the original BIOS present.

D945GCLF2D also reported working by a user.

For information about this board, go to
[../hardware/d945gclf.md](../hardware/d945gclf.md)

Flashing instructions {#clip}
=====================

Refer to [spi.md](spi.md) for how to re-flash externally.

Here is an image of the flash chip:\
![](https://av.canoeboot.org/d945gclf/d945gclf_spi.jpg)
