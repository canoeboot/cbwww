---
title: Install Canoeboot Open Source BIOS/UEFI boot firmware
x-toc-enable: true
...

Open source BIOS/UEFI boot firmware
------------------------------

With x86 machines, you can use the SeaBIOS or the GNU boot loader named GRUB as
a payload. On ARM
systems, you can use the U-Boot payload (coreboot still initialises hardware).
An x86/x86\_64 U-Boot UEFI payload is also available.

This article will teach you how to install Canoeboot, on any of the supported
laptop, desktop and server motherboards of Intel/AMD x86/x86\_64 and ARM64
platform.
Canoeboot is a [Free Software](https://writefreesoftware.org/learn) project
that replaces proprietary BIOS/UEFI firmware.

**ALWAYS remember to make a backup of the current flash, when overwriting it,
regardless of what firmware you currently have and what firmware you're
re-flashing it with; this includes updates between Canoeboot releases. Use
the `-r` option in flashprog instead `-w`, to read from the flash.**

Install Canoeboot via external flashing
---------------------------------------

Refer to the following article:\
[Externally rewrite 25xx NOR flash via SPI protocol](spi.md)

You are strongly advised to *have* an external flashing setup, and make sure
it works, before attempting internal flashing. This, in addition to making
a backup of the current flash contents, prior to flashing, whether you dump
externally or internally - if only external flashing is available, then it's
usually the case that only external dumping is available too.

Need help?
----------

Help is available on [Canoeboot IRC](../../contact.md) and other channels.

Which systems are supported by Canoeboot?
-----------------------------------------

Before actually reading the installation guides, please ensure that your
system is fully supported by Canoeboot. More information about the Canoeboot
build system can be found in the [cbmk maintenance manual](../maintain/).

Canoeboot currently supports the following systems:

### Games consoles

-   [Sony Playstation](playstation.md) (PS1/PSX)

### Servers (AMD, x86)

-   [ASUS KFSN4-DRE motherboard](kfsn4-dre.md)
-   [ASUS KGPE-D16 motherboard](kgpe-d16.md)

### Desktops (AMD, Intel, x86)

-   [Acer G43T-AM3](acer_g43t-am3.md)
-   Apple iMac 5,2
-   [ASUS KCMA-D8 motherboard](kcma-d8.md)
-   [Dell OptiPlex 780 variants e.g. MT, USFF](dell780.md)
-   [Gigabyte GA-G41M-ES2L motherboard](ga-g41m-es2l.md)
-   Intel D510MO and D410PT motherboards
-   [Intel D945GCLF](d945gclf.md)

### Laptops (Intel, x86)

-   [Apple MacBook1,1 and MacBook2,1](macbook21.md)
-   [Dell Latitude E4300, E6400, E6400 XFR and E6400 ATG](latitude.md)
-   [Lenovo ThinkPad R400](r400.md)
-   Lenovo ThinkPad R500
-   [Lenovo ThinkPad T400 / T400S](t400.md)
-   [Lenovo ThinkPad T500 / W500](t500.md)
-   Lenovo ThinkPad T60, X60, X60S, X60 Tablet (with Intel GPU)
-   [Lenovo ThinkPad X200 / X200S / X200 Tablet](x200.md)

### Laptops (ARM, with U-Boot payload)

-   [ASUS Chromebook Flip C101 (gru-bob)](chromebooks.md)
-   [Samsung Chromebook Plus (v1) (gru-kevin)](chromebooks.md)

### Emulation

-   [Qemu x86 and arm64](../misc/emulation.md)

Disable security before flashing
--------------------------------

**Before internal flashing, you must first disable `/dev/mem` protections. Make
sure to re-enable them after you're finished.**

**See: [Disabling /dev/mem protection](devmem.md)**

ROM image file names
--------------------

Canoeboot ROM images are named like
this: `payload_board_inittype_displaytype_keymap.rom`

The `payload` option can be SeaBIOS, SeaGRUB or U-Boot. If GRUB is available
on a given board, in flash, both SeaBIOS and SeaGRUB are provided; SeaBIOS
images still have GRUB available via the SeaBIOS menu, and SeaGRUB means that
SeaBIOS automatically loads GRUB from flash first (but you can still choose
something else, by pressing ESC in SeaBIOS when prompted).

Inittype can be `libgfxinit`, `vgarom` or `normal`. The `libgfxinit` option
means coreboot provides native video initialisation, for onboard graphics.
The `vgarom` option means coreboot executes a VGA option ROM for video
initialisation. The `normal` option means coreboot provides no video
initialisation, via VGA ROM or native code.

Displaytype can be `txtmode` or `corebootfb` - if inittype is `normal`, this
is ignored because `txtmode` is assumed.

If `payload` is `seabios` instead of `seagrub`, no keymaps are inserted into
flash and only US QWERTY is assumed, otherwise the keymap refers to what is used
in GRUB on `seagrub` payload setups.

If you use a libgfxinit image on a desktop machine, you can still insert a
graphics card and it'll work just fine; its own VGA option ROM will be
executed instead, if the primary payload is SeaBIOS, whether that be pure
SeaBIOS or a SeaGRUB setup.

EC firmware updates
-------------------

Obviously, free EC firmware would be preferable, but it is not the case on
all machine. We would like to have free EC firmware on more machines, but for
now, we must rely on the vendor in a lot of cases. The EC is usually on a
separate flash, so you wouldn't think about it unless you knew it was there;
this is exactly why it's mentioned, so that you think about it,
[because proprietary software is bad](../../news/policy.md).

In many cases, the EC firmware must be updated on a separate IC to the main
boot flash, and this can usually only be done with the vendor's own tool,
running from the vendor boot firmware, and usually only on Windows, because
they provide EC and BIOS/UEFI updates in the same utility. Find out what you
need to do for your machine before installing Canoeboot.

It is recommended that you update to the latest EC firmware version. The
[EC firmware](../../faq.md#ec-embedded-controller-firmware)

Updating the EC can sometimes provide benefit depending on the vendor. For
example, they might fix power issues that could then enhance battery life.

### ThinkPads

See: <http://www.thinkwiki.org/wiki/BIOS_update_without_optical_disk>

Otherwise, check the Lenovo website to find the update utility for your
motherboard.

### Other

The same wisdom applies to other laptop vendors.

Non-laptops typically do not have embedded controllers in them.

Canoeboot installation instructions
-----------------------------------

In general, if Canoeboot is already running, you can skip
towards the final section on this page, which provides general internal
flashing instructions. Internal flashing is when you flash the target machine
from the target machine, inside an operating system running on it.

Some boards require special steps, even if Canoeboot is already running,
for example if you [locked down the flash](../linux/grub_hardening.md).

Therefore, before following generic guides, make sure to check first whether
your board has special instructions, otherwise use the generic guide at the
end of this article.

### Intel GbE MAC address (IFD-based systems)

On all Intel platforms except X4X (e.g. Gigabyte GA-G41M-ES2L) and i945
ones (e.g. ThinkPad X60, ThinkPad T60, MacBook2,1), an Intel Flash Descriptor is
used. If the board has Intel gigabit ethernet, the MAC address is included in
flash, and can (must) be changed prior to installation.

You can use [nvmutil](nvmutil.md) to change the MAC address. You will perform
this modification to the ROM image, before flashing it.

### Flash lockdown / boot security

This is referred to informally as *Secure canoeBoot*.

Full flash lockdown is possible, with cryptographic verification of your
Linux kernel and other files, using special features in the GRUB payload.

There are also some Intel X4X platforms that use an ICH10 southbridge,
supported in Canoeboot, but these are flashed in a *descriptorless* setup,
which means that the MAC address is irrelevant (either there will be an Intel
PHY module that is now unusable, and you use an add-on card, or it doesn't use
an Intel PHY module and the onboard NIC is usable).

Install via host CPU (internal flashing)
========================================

See: [GRUB hardening / Secure canoeBoot](../linux/grub_hardening.md)

If you already did this, it's possible that you may no longer be able to
flash internally. If that is the case, you must [flash externally](spi.md).

### Updating an existing installation

Unless otherwise stated, in sections pertaining to each motherboard below,
an existing Canoeboot installation can be updated via internal flashing,
without any special steps; simply follow the general internal flashing
guide, in the final section further down this page.

If you have an existing Canoeboot installation but you *locked down the flash*,
updating it will require external flashing.

If you currently have the factory firmware, you probably need to flash
externally; on *some* machines, internal flashing is possible, usually with
special steps required that differ from updating an existing installation.

The next sections will pertain to specific motherboards, where indicated,
followed by general internal flashing instructions where applicable.

### Dell Latitude laptops (vendor BIOS)

See: [Dell Latitude flashing guide](latitude.md)

This applies to all supported Dell Latitude models. Remember to [update the
MAC address with nvmutil](nvmutil.md), before flashing.

### ThinkPad X200/T400/T500/W500/R400/R500

If you're running one of these with Lenovo BIOS, you must externally flash
Canoeboot, because the original firmware restricts writes to the flash.

There machines all use SOIC8/SOIC16 flash ICs. Refer to pages specifically for
each machine:

* [ThinkPad X200](x200.md)
* [ThinkPad T400](t400.md)
* [ThinkPad R400](r400.md)
* [ThinkPad T500/W500](t500.md) (R500 is similar)

NOTE: T400S, X200S and X200 Tablet require different steps, because these have
WSON8 flash ICs on them, which will require some soldering. Please read
the [external flashing guide](spi.md) in the section pertaining to WSON.

You can find WSON8 probes online, that are similar to a SOIC8/SOIC16 clip. Your
mileage may very, but WSON8 has the same pinout as SOIC8 so you might have some
luck with that.

### Intel D510MO/D410PT (vendor BIOS)

See: [External flashing guide](spi.md) - both boards are compatible with
the same image.

### Gigabyte GA-G41M-ES2L (vendor BIOS)

Internal flashing is possible, from factory BIOS to Canoeboot, but special
steps are required.

See: [Gigabyte GA-G41M-ES2L installation guide](ga-g41m-es2l.md)

### Acer G43T-AM3 (vendor BIOS)

See: [Acer G43T-AM3](acer_g43t-am3.md)

### MacBook 1,1 / 2,1 / iMac 5,2 (vendor BIOS)

MacBook *1,1* requires [external flashing](spi.md). MacBook *2,1* can always
be flashed internally. iMac 5,2 can be flashed internally.

Also check the [Macbook2,1 hardware page](macbook21.md)

### ASUS KCMA-D8 / KGPE-D16 (vendor BIOS)

[You must flash it externally](spi.md) (DIP-8 section) - also look at
the [KGPE-D16 hardware page](kgpe-d16.md).

Further information is available on the [KCMA-D8 page](kcma-d8.md).

KGPE-D16 installation is essentially the same, with the same type of flash
IC (DIP-8). Refer to the external flashing guide.

### ASUS KFSN4-DRE (vendor BIOS)

This board uses LPC flash in a PLCC32 socket. This coreboot page shows an
example of the push pin as a proof of concept:
<http://www.coreboot.org/Developer_Manual/Tools#Chip_removal_tools>

See: [ASUS KFSN4-DRE guide](kfsn4-dre.md)

Hot-swap the flash IC with another one while it's running, and flash it
internally.

### Intel D945GCLF (vendor BIOS)

See: [Intel D945GCLF flashing guide](d945gclf.md)

### ThinkPad T60/X60/X60Tablet/X60S

Only the Intel GPU is compatible. Do not flash the ATI GPU models.

External flashing guides:

* [ThinkPad X60](x60_unbrick.md)
* [ThinkPad X60 Tablet](x60tablet_unbrick.md)
* [ThinkPad T60](t60_unbrick.md)

These machines can also be flashed internally, by exploiting a bug
in the original Lenovo BIOS. If there's a BIOS password at boot, you should
just flash externally.

Internal flashing instructions:

First, please ensure that your CR2032/CMOS battery is working. This is what
powers the SRAM containing BIOS settings, and it powers the real-time clock.
It also holds the BUC.TS value - this is what we need.

BUC (Backup Control) register contains a bit called Top Swap (TS). The 64KB
bootblock at the top of flash is complemented by a backup Top Swap just above
it. The one at the end can't be flashed internally while Lenovo BIOS is running,
but the rest of it can be flashed (everything above the main bootblock).

By setting the TS bit, you can make the machine boot from the backup bootblock.

Download the Libreboot 20160907 utils archive, and in there you will find
these binaries:

* `flashprog`
* `flashprog_i945_sst`
* `flashprog_i945_mx`

You'll also find the bucts tool. Run it as root:

	./bucts 1

Now run both of these as root:
	
	./flashrom_i945_sst -p internal -w coreboot.rom
	./flashrom_i945_mx -p internal -w coreboot.rom

You'll see a lot of errors. This is normal. You should see something like:

```
Reading old flash chip contents... done.
Erasing and writing flash chip... spi_block_erase_20 failed during command execution at address 0x0
Reading current flash chip contents... done. Looking for another erase function.
spi_block_erase_52 failed during command execution at address 0x0
Reading current flash chip contents... done. Looking for another erase function.
Transaction error!
spi_block_erase_d8 failed during command execution at address 0x1f0000
Reading current flash chip contents... done. Looking for another erase function.
spi_chip_erase_60 failed during command execution
Reading current flash chip contents... done. Looking for another erase function.
spi_chip_erase_c7 failed during command execution
Looking for another erase function.
No usable erase functions left.
FAILED!
Uh oh. Erase/write failed. Checking if anything has changed.
Reading current flash chip contents... done.
Apparently at least some data has changed.
Your flash chip is in an unknown state.
```

If you see this, rejoice! It means that the flash was successful. Please do not
panic. Shut down now, and wait a few seconds, then turn back on again.

The main bootblock still isn't flashed, but you can shut down, wait a few
seconds and boot up again. When you do, you'll have Canoeboot. Please make
sure to flash a second time, like so:
	
	flashprog -p internal -w coreboot.rom

Canoeboot recommends `flashprog` now, which is a fork of flashrom, but we used
flashrom in the 2016 release. The macronix/ssh flashrom binaries there are
specifically patched; check the Libreboot 20160907 source code for the actual
patches. The patches modify some flash chip definitions in flashrom, to exploit
the bug in Lenovo BIOS enabling internal flashing.

You must ensure that the second flash is performed, upon reboot, because
otherwise if the CR2032 battery dies, bucts will be reset and it will no
longer boot.

When you've done the second flash, which includes overwriting the main
bootblock, set bucts back to zero:

	./bucts 0

The second flash can be done by simply following the general internal flashing
guide further down on this page.

### ARM-based Chromebooks

See: [Chromebook flashing instructions](chromebooks.md)

NOTE: The generic flashing instructions (later on this page) apply only to
the x86 machines, because the Chromebooks still use flashrom with
the `-p host` argument instead of `-p internal` when flashing, and you typically
need to flash externally, due to Google's security model.

### QEMU (arm64 and x86)

Canoeboot can be used on QEMU (virtual machine), which is useful for debugging
payloads and generally trying out Canoeboot, without requiring real hardware.

See: [Canoeboot QEMU guide](../misc/emulation.md)

Install via host CPU (internal flashing)
----------------------------------------

NOTE: This mainly applies to the x86 machines.

Please check other sections listed above, to see if there is anything
pertaining to your motherboard. Internal flashing means that you boot Linux or
BSD on the target machine, and run `flashprog` there, flashing the machine
directly.

**If you can't flash internally, you must [flash externally](spi.md).**

Internal flashing is often unavailable with the factory firmware, but it is
usually possible when Canoeboot is running (barring special circumstances).

### Run flashprog on host CPU

#### Flash chip size

Use this to find out:

	flashprog -p internal

In the output will be information pertaining to your boot flash.

#### Howto: read/write/erase the boot flash

How to read the current chip contents:

	sudo flashprog -p internal:laptop=force_I_want_a_brick,boardmismatch=force -r dump.bin

You should still make several dumps, even if you're flashing internally, to
ensure that you get the same checksums. Check each dump using `sha1sum`

How to erase and rewrite the chip contents:

	sudo flashprog -p internal:laptop=force_I_want_a_brick,boardmismatch=force -w canoeboot.rom

NOTE: `force_I_want_a_brick` is not scary. Do not be scared! This merely disables
the safety checks in flashprog. Flashprog and coreboot change a lot, over the years,
and sometimes it's necessary to use this option. If you're scared, then just
follow the above instructions, but remove that option. So, just use `-p internal`.
If that doesn't work, next try `-p internal:boardmismatch=force`. If that doesn't
work, try `-p internal:boardmismatch=force,laptop=force_I_want_a_brick`. So long
as you *ensure* you're using the correct ROM for your machine, it will be safe
to run flashprog. These extra options just disable the safetyl checks in flashprog.
There is nothing to worry about.

If successful, it will either say `VERIFIED` or it will say that the chip
contents are identical to the requested image.
