---
title: Modifying grub.cfg in CBFS
x-toc-enable: true
...

NOTE: Canoeboot standardises on [flashprog](https://flashprog.org/wiki/Flashprog)
now, as of 3 May 2024, which is a fork of flashrom.

Before you follow this guide, it is advisable that you have the ability to
flash externally, just in case something goes wrong.

This guide assumes that you use the GRUB bootloader as your default
payload. In this configuration, GRUB is flashed alongside coreboot and runs
on *bare metal* as a native coreboot payload and does *not* use BIOS or UEFI
services (but it *can* load and execute SeaBIOS, in addition to any other
coreboot payload, by chainloading it).

In most circumstances, this guide will not benefit you. Canoeboot's default
GRUB configuration file contains scripting logic within it that intelligently
searches for GRUB partitions installed onto a partition on your SSD, HDD or
USB drive installed on your computer. If such a file is found, Canoeboot's
default GRUB configuration is configured to switch automatically to that
configuration. While not perfect, the logic *does* work with most
configurations.

Therefore, you should only follow *this* guide if the automation (described
above) does not work. It goes without saying that modifying the default GRUB
configuration is risky, because a misconfiguration could create what's called
a *soft brick* where your machine is effectively useless and, in that scenario,
may or may not require external flashing equipment for restoring the machine to
a known state.

Compile flashprog and cbfstool
=============================

Canoeboot does not currently distribute utilities pre-compiled. It only
provides ROM images pre-compiled, where feasible. Therefore, you have to build
the utilities from source.

As for the ROM, there are mainly three methods for obtaining a Canoeboot ROM
image:

1. Dump the contents of the the main *boot flash* on your system, which already
   has Canoeboot installed (with GRUB as the default payload). Extract the
   GRUB configuration from *that* ROM image.
2. Extract it from a Canoeboot ROM image supplied by the Canoeboot project, on
   the Canoeboot website or mirrors of the Canoeboot website.
3. Build the ROM yourself, using the Canoeboot build system. Instructions for
   how to do this are covered in the following article:
   [How to build Canoeboot from source](../build/)

In either case, you will use the `cbfstool` supplied in the Canoeboot build
system.
This can be found under `coreboot/*/util/cbfstool/` as source code,
where `*` can be any coreboot source code directory for a given mainboard.
The directory named `default` should suffice.

Install the build dependencies. For Ubuntu 20.04 and similar, you can run
the following command in the Canoeboot build system, from the root directory
of the Canoeboot Git repository.

	./build dependencies ubuntu2004

Then, download coreboot:

	./update trees -f coreboot

Finally, compile the `cbutils` module:

	./build grub

Among other things, this will produce a `cbfstool` executable under any of the
subdirectories in `src/coreboot/` under `util/cbfstool/cbfstool

For example: `src/coreboot/default/util/cbfstool/cbfstool`

The `cbfstool` utility is what you shall use. It is used to manipulate CBFS
(coreboot file system) which is a file system contained within the coreboot
ROM image; as a *coreboot distribution*, Canoeboot inherits this technology.

You will also want to build `flashprog` which canoeboot recommends for reading
from and/or writing to the boot flash. In the canoeboot build system, you can
build it by running this command:

	./update trees -b flashprog

An executable will be available at `src/flashprog/flashprog` after you have done
this.

Dump the boot flash
===================

If you wish to modify your *existing* canoeboot ROM, which was installed on
your computer, you can use `flashprog` to acquire it.

Simply run the following, after using canoeboot's build system to compile
flashprog:

	sudo ./src/flashprog/flashprog -p internal -r dump.bin

If flashprog complains about multiple flash chip definitions, do what it says to
rectify your command and run it again.

You may want to use the following, instead of `-p internal`:
`-p internal:laptop=force_I_want_a_brick,boardmismatch=force`

Do not let the word *brick* fools you. This merely disables the safety checks
in flashprog, which is sometimes necessary depending on what ROM was already
flashed, versus the new ROM image.

The `internal` option assumes that internal read/write is possible; this is
when you read from and/or write to the boot flash from an operating systems
(usually GNU+Linux) that is *running on* the target system.

In other cases, you may need to connect an SPI programmer externally (with the
machine powered down) and read the contents of the boot flash.

[Learn how to externally reprogram these chips](../install/spi.md)

Extract grub.cfg
================

Canoeboot 20231026 or newer
-----------------------------------

Releases or or after Canoeboot 20231026 contain `grub.cfg` inside GRUB memdisk,
inaccessible directly from CBFS, but the memdisk is inside `grub.elf` which
gets put inside CBFS.

An override is possible, on new Canoeboot revisions. If `grub.cfg` is present
in CBFS, Canoeboot's GRUB will use *that* and not the memdisk one; it will not
auto-switch to `grubtest.cfg`, but the test config will be available in the
menu to switch to, if present. This contrasts behaviour in older revisions.

You can find `grub.cfg` under cbmk (for this purpose, it's best to use the
cbmk one, not the release one - unless you're using a release image).
Find it at path (in current cbmk): `config/grub/config/grub.cfg`.

So, you can *add* `grubtest.cfg` as normal, test that, and
then *add* `grub.cfg` once you're happy, and it will override the default.

In older revisions of Canoeboot, GRUB memdisk always loaded the config from
CBFS, which you would have to replace; this meant it was possible to brick
your GRUB installation if you accidentally flashed without `grub.cfg`. In
Canoeboot 20231026 and higher, such error is impossible; this behaviour is
also present in Libreboot 20231021, upon which Canoeboot 20231026 is based.

This new behaviour is therefore much safer, under user error. However, it's
still possible to flash a bad `grub.cfg` by misconfiguring it, which is why
you should add `grubtest.cfg` first; when present, it will be available in
the default menu, for testing, but the GRUB memdisk config will always be used
first if no `grub.cfg` (as opposed to `grubtest.cfg`) exists in CBFS.

Insert new grub.cfg
===================

You can find the default config under `config/grub/config/grub.cfg` in the
Canoeboot build system,

Remove the old `grub.cfg` (substitute with `grubtest.cfg` as desired):

	cbfstool dump.bin remove -n grub.cfg

Add your modified `grub.cfg` (substitute with `grubtest.cfg` as desired):

	cbfstool dump.bin add -f grub.cfg -n grub.cfg -t raw

Flash the modified ROM image
============================

Your modified `dump.bin` or other modified Canoeboot ROM can then be re-flashed
using:

	sudo ./flashprog -p internal -w dump.bin

If a `-c` option is required, use it and specify a flash chip name. This is
only useful when `flashprog` complains about multiple flash chips being
detected.

If flashprog complains about wrong chip/board, make sure that your ROM is for
the correct system. If you're sure, you can disable the safety checks by running
this instead:

	sudo ./flashprog -p internal:laptop=force_I_want_a_brick,boardmismatch=force -w dump.bin

If you need to use external flashing equipment, see the link above to the
Raspberry Pi page.
