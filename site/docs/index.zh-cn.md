---
title: 文档
...

Canoeboot 的最新更新，可以在 [canoeboot.org](https://canoeboot.org) 上找到。新闻，包括新版发布公告，可以在[新闻主页](../news/)中找到。

[Canoeboot 常见问题解答](../faq.md).

What is Canoeboot? An article is available for that; please read the
article titled [What is Canoeboot?](../about.md).

Need help?
----------

Help is available on [Canoeboot IRC](../contact.md) and other channels.

安装 Canoeboot
--------------

-   [如何安装 Canoeboot](install/)

操作系统相关文档
----------------

-   [如何在 x86 机器上安装 BSD](bsd/)
-   [Linux 指南](linux/)

开发者信息
----------

-   [如何编译 canoeboot 源代码](build/)
-   [构建系统开发者文档](maintain/)
-   [GRUB payload](grub/)
-   [U-Boot payload](uboot/)

其它信息
--------

-   [杂项](misc/)
-   [代号列表](misc/codenames.md)

If you're using *release archives*, this documentation and accompanying images
are under `src/www/` and `src/img/` respectively, in the source release archive.
This fact is true for Canoeboot 20231026 and newer.
