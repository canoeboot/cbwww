---
title: Проект Canoeboot
x-toc-enable: true
...

Проект *Canoeboot* надає
[вільну](https://writefreesoftware.org/learn) *завантажувальну
прошивку*, яка ініціалізує апаратне забезпечення (наприклад, контролер пам'яті, ЦП,
периферію) на [конкретних цілях Intel/AMD x86 та ARM](docs/hardware/), що
потім розпочинає завантажувач для вашої операційної системи. [GNU+Linux](docs/gnulinux/)
та [BSD](docs/bsd/) добре підтримуються. Це заміняє пропрієтарну BIOS/UEFI
прошивку. Допомога доступна
через [\#canoeboot](https://web.libera.chat/#canoeboot)
на [Libera](https://libera.chat/) IRC.

<img tabindex=1 class="r" src="https://av.canoeboot.org/t60logo.jpg" /><span class="f"><img src="https://av.canoeboot.org/t60logo.jpg" /></span>

**НОВИЙ ВИПУСК: Останній випуск Canoeboot 20231107, випущено 7 Листопад 2023.
Дивіться: [Оголошення про випуск Canoeboot 20231107](news/canoeboot20231107.md).**

Canoeboot was *originally* named [nonGeNUine Boot](news/nongenuineboot20230717.html),
provided as a proof of concept for the [GNU Boot](https://libreboot.org/news/gnuboot.html)
or *gnuboot* project to use a more modern Libreboot base, but
they went in their own direction instead. Canoeboot development was continued,
and it maintains sync with the Libreboot project, as a parallel development
effort. See: [How are Canoeboot releases engineered?](about.md#how-releases-are-engineered)

Canoeboot adheres to the *GNU Free System Distribution Guidelines* as policy,
whereas Libreboot adheres to its own [Binary Blob Reduction
Policy](https://libreboot.org/news/policy.html). Canoeboot and Libreboot
are *both* maintained by the same person, Leah Rowe, sharing code back and forth.

Чому вам варто використовувати *Canoeboot*?
----------------------------

Canoeboot надає вам [свободи](https://writefreesoftware.org/learn), які в
іншому випадку ви не можете отримати з більшістю інших завантажувальних
прошивок. Він надзвичайно [потужний](docs/gnulinux/grub_hardening.md)
та [налаштовується](docs/maintain/) для багатьох випадків використання.

У вас є права. Право на конфіденційність, свобода мислення, свобода висловлювання
та право читати. В цьому контексті, Canoeboot надає вам ці права.
Ваша свобода має значення.
[Право на ремонт](https://yewtu.be/watch?v=Npd_xDuNi9k) має значення.
Багато людей використовують пропрієтарну (невільну)
завантажувальну прошивку, навіть якщо вони використовують [вільну операційну систему](https://www.openbsd.org/).
Пропрієтарна прошивка часто [містить](faq.uk.html#intel) [лазівки](faq.uk.html#amd),
та може бути глючною. Проект Canoeboot було засновано в жовтень 2023 року, з
явною метою зробити прошивку coreboot доступною для нетехнічних користувачів.

Проект Canoeboot використовує [coreboot](https://www.coreboot.org/) для [ініціалізації апаратного забезпечення](https://doc.coreboot.org/getting_started/architecture.html).
Coreboot помітно складний для встановлення для більшості нетехнічних користувачів; він
виконує тільки базову ініціалізацію та перестрибує до окремої програми
[корисного навантаження](https://doc.coreboot.org/payloads.html) (наприклад,
[GRUB](https://www.gnu.org/software/grub/),
[Tianocore](https://www.tianocore.org/)), які також мають бути налаштованими.
*Програмне забезпечення Canoeboot вирішує цю проблему*; це *дистрибутив coreboot* з
[автоматизованою системою побудови](docs/build/index.uk.md), яка збирає завершені *образи ROM*, для
більш надійної установки. Документація надається.

Чим Canoeboot відрізняється від звичайного coreboot?
---------------------------------------------

<img tabindex=1 class="l" style="max-width:25%;" src="https://av.canoeboot.org/dip8/adapter.jpg" /><span class="f"><img src="https://av.canoeboot.org/dip8/adapter.jpg" /></span>

Таким же самим чином, як *Debian* це дистрибутив Linux, Canoeboot це
*дистрибутив coreboot*. Якщо ви хочете зібрати образ ROM з нуля, вам
інакше довелось би виконати налаштування експертного рівня coreboot, GRUB та
будь-якого іншого потрібного програмного забезпечення, для підготування образа ROM. З *Canoeboot*,
ви можете буквально завантажити з Git або архіву джерельного коду, та запустити `make`, і це
побудує всі образи ROM. Автоматизована система побудови, названа `cbmk`
(Canoeboot MaKe), збирає ці образи ROM автоматично, без будь-якого вводу користувача
або потрібного втручання. Налаштування вже виконано заздалегідь.

Якщо би ви збирали звичайний coreboot, не використовуючи автоматизовану систему побудови Canoeboot,
це вимагало би набагато більше втручання та гідних технічних
знань для створення робочої конфігурації.

Звичайні бінарні випуски Canoeboot надають ці
образи ROM попередньо зібраними, і ви можете просто встановити їх, не маючи спеціальних
знань або навичок, окрім можливості
слідувати [спрощеним інструкціям, написаним для нетехнічних
користувачів](docs/install/).
