---
title: завантажувальну прошивку BIOS/UEFI прошивку
x-toc-enable: true
...

Проект *Canoeboot* надає
[вільну](https://writefreesoftware.org/learn) *завантажувальну
прошивку*, яка ініціалізує апаратне забезпечення (наприклад, контролер пам'яті, ЦП,
периферію) на [конкретних цілях Intel/AMD x86 та ARM](docs/install/#which-systems-are-supported-by-canoeboot), що
потім розпочинає завантажувач для вашої операційної системи. [Linux](docs/linux/)
та [BSD](docs/bsd/) добре підтримуються. Це заміняє пропрієтарну BIOS/UEFI
прошивку. Допомога доступна
через [\#canoeboot](https://web.libera.chat/#canoeboot)
на [Libera](https://libera.chat/) IRC.

<img tabindex=1 class="r" src="https://av.canoeboot.org/t60logo.jpg" /><span class="f"><img src="https://av.canoeboot.org/t60logo.jpg" /></span>

Canoeboot provides GNU boot loader "[GRUB](docs/linux/)" and SeaBIOS payloads
on x86/x86\_64
Intel/AMD motherboards, and a [U-Boot UEFI payload](docs/uboot/) *for coreboot*
on ARM64(Aarch64) motherboards.
An [x86/x86\_64 U-Boot UEFI payload](docs/uboot/uboot-x86.md) is also available
on some boards. The x86, x86\_64 and arm64 U-Boot payloads provide a lightweight
UEFI boot implementation, which can boot many Linux distros and BSD systems.
The SeaBIOS and GRUB payloads also boot Linux/BSD systems. Which one you use
depends on your preferences. Canoeboot's [design](docs/maintain/) incorporates
all of these boot methods in a single image, so you can choose which one you use
at boot time, and more payloads (e.g. Linux kexec payload) are planned for
future releases.

**НОВИЙ ВИПУСК: Останній випуск Canoeboot 20250107, випущено 7 January 2025.
Дивіться: [Оголошення про випуск Canoeboot 20250107](news/canoeboot20250107.md).**

Чому вам варто використовувати *Canoeboot*?
----------------------------

Canoeboot надає вам [свободи](https://writefreesoftware.org/learn), які в
іншому випадку ви не можете отримати з більшістю інших завантажувальних
прошивок. Він надзвичайно [потужний](docs/linux/grub_hardening.md)
та [налаштовується](docs/maintain/) для багатьох випадків використання.

У вас є права. Право на конфіденційність, свобода мислення, свобода висловлювання
та право читати. В цьому контексті, Canoeboot надає вам ці права.
Ваша свобода має значення.
[Право на ремонт](https://en.wikipedia.org/wiki/Right_to_repair) має значення.
Багато людей використовують пропрієтарну (невільну)
завантажувальну прошивку, навіть якщо вони використовують вільну операційну систему.
Пропрієтарна прошивка часто [містить](faq.uk.html#intel) [лазівки](faq.uk.html#amd),
та може бути глючною. Проект Canoeboot було засновано в жовтень 2023 року, з
явною метою зробити прошивку coreboot доступною для нетехнічних користувачів.

Проект Canoeboot використовує [coreboot](https://www.coreboot.org/) для [ініціалізації апаратного забезпечення](https://doc.coreboot.org/getting_started/architecture.html).
Coreboot помітно складний для встановлення для більшості нетехнічних користувачів; він
виконує тільки базову ініціалізацію та перестрибує до окремої програми
[корисного навантаження](https://doc.coreboot.org/payloads.html) (наприклад,
[GRUB](https://www.gnu.org/software/grub/),
[Tianocore](https://www.tianocore.org/)), які також мають бути налаштованими.
*Програмне забезпечення Canoeboot вирішує цю проблему*; це *дистрибутив coreboot* з
[автоматизованою системою побудови](docs/build/index.uk.md), яка збирає завершені *образи ROM*, для
більш надійної установки. Документація надається.

Чим Canoeboot відрізняється від звичайного coreboot?
---------------------------------------------

<img tabindex=1 class="l" style="max-width:25%;" src="https://av.vimuser.org/uboot-canoe.png" /><span class="f"><img src="https://av.vimuser.org/uboot-canoe.png" /></span>

Таким же самим чином, як *Debian* це дистрибутив Linux, Canoeboot це
*дистрибутив coreboot*. Якщо ви хочете зібрати образ ROM з нуля, вам
інакше довелось би виконати налаштування експертного рівня coreboot, GRUB та
будь-якого іншого потрібного програмного забезпечення, для підготування образа ROM. З *Canoeboot*,
ви можете буквально завантажити з Git або архіву джерельного коду, та запустити `make`, і це
побудує всі образи ROM. Автоматизована система побудови, названа `cbmk`
(Canoeboot MaKe), збирає ці образи ROM автоматично, без будь-якого вводу користувача
або потрібного втручання. Налаштування вже виконано заздалегідь.

Якщо би ви збирали звичайний coreboot, не використовуючи автоматизовану систему побудови Canoeboot,
це вимагало би набагато більше втручання та гідних технічних
знань для створення робочої конфігурації.

Звичайні бінарні випуски Canoeboot надають ці
образи ROM попередньо зібраними, і ви можете просто встановити їх, не маючи спеціальних
знань або навичок, окрім можливості
слідувати [спрощеним інструкціям, написаним для нетехнічних
користувачів](docs/install/).
