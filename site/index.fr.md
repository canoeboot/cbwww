---
title: Libre et Open Source BIOS/UEFI firmware
x-toc-enable: true
...

Canoeboot est un micrologiciel de démarrage [libéré](https://writefreesoftware.org/learn)
qui initialise le matériel (càd le contrôleur mémoire, CPU,
périphériques) sur [des ordinateurs x86/ARM spécifiques](docs/install/#which-systems-are-supported-by-canoeboot)
et lance un chargeur d'amorçage pour votre système d'exploitation. [Linux](docs/linux/) et [BSD](docs/bsd/) sont bien supportés. C'est un
remplacement pour le micrologiciel UEFI/BIOS propriétaire.
Des canaux d'aide sont disponibles 
dans le canal [\#canoeboot](https://web.libera.chat/#canoeboot) sur le serveur IRC [Libera](https://libera.chat/).

<img tabindex=1 class="r" src="https://av.canoeboot.org/t60logo.jpg" /><span class="f"><img src="https://av.canoeboot.org/t60logo.jpg" /></span>

Canoeboot provides GNU boot loader "[GRUB](docs/linux/)" and SeaBIOS payloads
on x86/x86\_64
Intel/AMD motherboards, and a [U-Boot UEFI payload](docs/uboot/) *for coreboot*
on ARM64(Aarch64) motherboards.
An [x86/x86\_64 U-Boot UEFI payload](docs/uboot/uboot-x86.md) is also available
on some boards. The x86, x86\_64 and arm64 U-Boot payloads provide a lightweight
UEFI boot implementation, which can boot many Linux distros and BSD systems.
The SeaBIOS and GRUB payloads also boot Linux/BSD systems. Which one you use
depends on your preferences. Canoeboot's [design](docs/maintain/) incorporates
all of these boot methods in a single image, so you can choose which one you use
at boot time, and more payloads (e.g. Linux kexec payload) are planned for
future releases.

**NOUVELLE VERSION: La dernière version est [Canoeboot 20250107](news/canoeboot20250107.md), sortie
le 7 January 2025.**

Pourquoi devriez-vous utiliser *Canoeboot*?
-----------------------------------

Canoeboot vous donne des [libertés](https://writefreesoftware.org/learn)
que nous n'auriez pas autrement avec d'autre micrologiciel de démarrage. Il est
extremement [puissant](docs/linux/grub_hardening.md)
et [configurable](docs/maintain) pour  plein de cas d'utilisations.

Vous avez des droits. Un droit à la vie privée, liberté de pensée, liberté d'espression et le droit de lire. Dans ce contexte là, Canoeboot vous permet d'avoir ces droits.
Votre liberté compte.
Le [Droit à la réparation](https://en.wikipedia.org/wiki/Right_to_repair) est important.
Beaucoup de personnes utilisent un micrologiciel de 
démarrage propriétare (non libre), même
si ils utilisent un système d'exploitation libre.
Les micrologiciels propriétaires [contiennent](faq.html#intel) souvent 
des [portes dérobées](faq.html#amd) et peuvent être instable. Canoeboot 
a été fondé en Octobre 2023 avec le but de rendre le libre
au niveau du micrologiciel accessible pour les utilisateurs non-techniques. 

Canoeboot utilise [coreboot](https://www.coreboot.org) pour
[l'initialisation matérielle](https://doc.coreboot.org/getting_started/architecture.html)
Coreboot est renommé comme être difficilement installable par des utilisateurs 
non technique; il se charge seulement de l'initialisation basique
puis bascule sur un programme de [charge utile](https://doc.coreboot.org/payloads.html)
(par ex. [GRUB](https://www.gnu.org/software/grub/),
[Tianocore](https://www.tianocore.org/)), qui doit lui aussi être configuré.
*Canoeboot règle ce problème*; c'est une *distribution de coreboot* avec
un [système de compilation automatisé](docs/builds/), crééant des 
*images ROM* complètes pour une installation plus robuste. De la documentation est disponible.

De quelle façon Canoeboot diffère de Coreboot?
------------------------------------------------

<img tabindex=1 class="l" style="max-width:25%;" src="https://av.vimuser.org/uboot-canoe.png" /><span class="f"><img src="https://av.vimuser.org/uboot-canoe.png" /></span>

Contrairement à l'opinion populaire, le but principal de Canoeboot n'est
pas de fournir un Coreboot déblobbé; ceci n'est simplement qu'une
des politiques de Canoeboot, une importante certes, mais qui n'est qu'un
aspect mineur de Canoeboot.

De la même façon que *Debian* est une distribution Linux, Canoeboot 
est une *distribution coreboot*. Si vous voulez compilé une image ROM
en partant des bases, vous devez alors effectuer une configuration experte 
de Coreboot, GRUB et n'importe quel autre logiciel dont vous avez besoin 
afin de préparer la ROM. Avec *Canoeboot*,
vous pouvez télécharger la source depuis Git ou une archive, exécuter
`make` etça compilera une image ROM entières. Le système de compilation 
automatisé de Canoeboot nommé `cbmk` (Canoeboot MaKe), compile ces images 
ROM automatiquement, sans besoin d'entrées utilisateur or intervention
requise. La configuration est faite à l'avance.

Si vous devriez compiler du coreboot classique sans utiliser le système 
de build automatisé de Canoeboot, ça demanderait bien plus d'effort et 
de connaissances techniques décente pour écrire une configuration qui marche.

Les versions de Canoeboot fournissent ces images ROM pré-compilés et vous 
pouvez les installez simplement, sans connaissance ou compétence particulière 
à savoir, sauf [suivre des instructions simplifiés écrite pour des utilisateurs non techniques](docs/install/).
