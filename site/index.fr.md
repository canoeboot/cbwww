---
title: Projet Canoeboot
x-toc-enable: true
...

Canoeboot est un micrologiciel de démarrage [libéré](https://writefreesoftware.org/learn)
qui initialise le matériel (càd le contrôleur mémoire, CPU,
périphériques) sur [des ordinateurs x86/ARM spécifiques](docs/hardware/)
et lance un chargeur d'amorçage pour votre système d'exploitation. [GNU+Linux](docs/gnulinux/) et [BSD](docs/bsd/) sont bien supportés. C'est un
remplacement pour le micrologiciel UEFI/BIOS propriétaire.
Des canaux d'aide sont disponibles 
dans le canal [\#canoeboot](https://web.libera.chat/#canoeboot) sur le serveur IRC [Libera](https://libera.chat/).

<img tabindex=1 class="r" src="https://av.canoeboot.org/t60logo.jpg" /><span class="f"><img src="https://av.canoeboot.org/t60logo.jpg" /></span>

**NOUVELLE VERSION: La dernière version est [Canoeboot 20240612](news/canoeboot20240612.md), sortie
le 12 June 2024.**

Pourquoi devriez-vous utiliser *Canoeboot*?
-----------------------------------

Canoeboot vous donne des [libertés](https://writefreesoftware.org/learn)
que nous n'auriez pas autrement avec d'autre micrologiciel de démarrage. Il est
extremement [puissant](docs/gnulinux/grub_hardening.md)
et [configurable](docs/maintain) pour  plein de cas d'utilisations.

Vous avez des droits. Un droit à la vie privée, liberté de pensée, liberté d'espression et le droit de lire. Dans ce contexte là, Canoeboot vous permet d'avoir ces droits.
Votre liberté compte.
Le [Droit à la réparation](https://yewtu.be/watch?v=Npd_xDuNi9k) est important.
Beaucoup de personnes utilisent un micrologiciel de 
démarrage propriétare (non libre), même
si ils utilisent un système d'exploitation libre.
Les micrologiciels propriétaires [contiennent](faq.html#intel) souvent 
des [portes dérobées](faq.html#amd) et peuvent être instable. Canoeboot 
a été fondé en Octobre 2023 avec le but de rendre le libre
au niveau du micrologiciel accessible pour les utilisateurs non-techniques. 

Canoeboot utilise [coreboot](https://www.coreboot.org) pour
[l'initialisation matérielle](https://doc.coreboot.org/getting_started/architecture.html)
Coreboot est renommé comme être difficilement installable par des utilisateurs 
non technique; il se charge seulement de l'initialisation basique
puis bascule sur un programme de [charge utile](https://doc.coreboot.org/payloads.html)
(par ex. [GRUB](https://www.gnu.org/software/grub/),
[Tianocore](https://www.tianocore.org/)), qui doit lui aussi être configuré.
*Canoeboot règle ce problème*; c'est une *distribution de coreboot* avec
un [système de compilation automatisé](docs/builds/), crééant des 
*images ROM* complètes pour une installation plus robuste. De la documentation est disponible.

De quelle façon Canoeboot diffère de Coreboot?
------------------------------------------------

<img tabindex=1 class="l" style="max-width:25%;" src="https://av.canoeboot.org/dip8/adapter.jpg" /><span class="f"><img src="https://av.canoeboot.org/dip8/adapter.jpg" /></span>

Contrairement à l'opinion populaire, le but principal de Canoeboot n'est
pas de fournir un Coreboot déblobbé; ceci n'est simplement qu'une
des politiques de Canoeboot, une importante certes, mais qui n'est qu'un
aspect mineur de Canoeboot.

De la même façon que *Trisquel* est une distribution GNU+Linux, Canoeboot 
est une *distribution coreboot*. Si vous voulez compilé une image ROM
en partant des bases, vous devez alors effectuer une configuration experte 
de Coreboot, GRUB et n'importe quel autre logiciel dont vous avez besoin 
afin de préparer la ROM. Avec *Canoeboot*,
vous pouvez télécharger la source depuis Git ou une archive, exécuter
`make` etça compilera une image ROM entières. Le système de compilation 
automatisé de Canoeboot nommé `cbmk` (Canoeboot MaKe), compile ces images 
ROM automatiquement, sans besoin d'entrées utilisateur or intervention
requise. La configuration est faite à l'avance.

Si vous devriez compiler du coreboot classique sans utiliser le système 
de build automatisé de Canoeboot, ça demanderait bien plus d'effort et 
de connaissances techniques décente pour écrire une configuration qui marche.

Les versions de Canoeboot fournissent ces images ROM pré-compilés et vous 
pouvez les installez simplement, sans connaissance ou compétence particulière 
à savoir, sauf [suivre des instructions simplifiés écrite pour des utilisateurs non techniques](docs/install/).
