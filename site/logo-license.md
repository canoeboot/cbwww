---
title: Canoeboot logo license
...

You can download the logo files from `cbwww-img.git`. See:
[git.md](git.md) - licensing available there. The Canoeboot logo design is
Copyright 2023 Cuan Knaggs, provided to the Canoeboot project under license:
Creative Commons Attribution-ShareAlike 4.0 International - and the version
used by Canoeboot is a modified version of the original. The original is included
in that Git repository, `cbwww-img.git` (linked above), along with the modified
versions. A version of that is also provided in `cbmk.git` as a GRUB
background, and under `cbwww.git` as the favicon icon for the Canoeboot website
hosted at *canoeboot.org*.
