---
title: How to use Git and/or send patches to Canoeboot
x-toc-enable: true
...

Canoeboot repositories
----------------------

Information about who works on canoeboot and who runs the project can be
found on [who.md](who.md)

The `canoeboot` project has 3 main Git repositories:

* Build system: <https://codeberg.org/canoeboot/cbmk>
* Website (+docs): <https://codeberg.org/canoeboot/cbwww>
* Images (for website): <https://codeberg.org/canoeboot/cbwww-img>

Development of canoeboot is done using the Git version control system.
Refer to the [official Git documentation](https://git-scm.com/doc) if you don't
know how to use Git.

The `bucts` repository is hosted by the Canoeboot project, because the original
repository on `stuge.se` is no longer available, last time we checked. The
`bucts` program was written by Peter Stuge. You need `bucts` if you're flashing
internally an Canoeboot ROM onto a ThinkPad X60 or T60 that is currently running
the original Lenovo BIOS. Instructions for that are available here:\
[Canoeboot installation guides](docs/install/)

### cbmk (canoeboot-make)

This is the core build system in canoeboot. You could say that `cbmk` *is*
canoeboot! Download the Git repository:

	git clone https://codeberg.org/canoeboot/cbmk

The `git` command, seen above, will download the canoeboot build system `cbmk`.
You can then go into it like so:

	cd cbmk

Make whatever changes you like, or simply build it. For instructions on how to
build `cbmk`, refer to the [build instructions](docs/build/).

Information about the build system itself, and how it works, is available in
the [cbmk maintenance guide](docs/maintain/).

### cbwww and cbwww-img

The *entire* canoeboot website and documentation is hosted in a Git repository.
Download it like so:

	git clone https://codeberg.org/canoeboot/cbwww

Images are hosted on <https://av.canoeboot.org/> and available in a separate
repository:

	git clone https://codeberg.org/canoeboot/cbwww-img

Make whatever changes you like. See notes below about how to send patches.

The entire website is written in Markdown, specifically the Pandoc version of
it. The static HTML pages are generated with [Untitled](https://untitled.vimuser.org/).
Leah Rowe, the founder of canoeboot, is also the founder of the Untitled static
site generator project.

If you like, you can set up a local HTTP server and build your own local
version of the website. Please note that images will still link to the ones
hosted on <https://av.canoeboot.org/>, so any images that you add to `cbwww-img`
will not show up on your local `cbwww` site if you make the image links (for
images that you add) link to `av.canoeboot.org`. However, it is required that such
images be hosted on av.canoeboot.org.

Therefore, if you wish to add images to the website, please also submit to the
`cbwww-img` repository, with the links to them being
<https://av.canoeboot.org/path/to/your/new/image/in/cbwww-img> for each one.
When it is merged on the canoeboot website, your images will appear live.

If adding a photo, compress it for web distribution. Images should be about
800px wide, and usually under 100KiB in size:

First, scale your image down to approximately 800px width, using your favourite
image manipulation program. For example, with `imagemagick` you can do the
following (make sure the image isn't already smaller or equal than preferred).

	convert original.jpg -resize 600000@ -quality 70% web.jpg

You should always run `jpegoptim` on jpg images before submitting them.
It strips useless metadata and *losslessly* optimises them further by cleverly
rearranging the huffman tables used in them.

	jpegoptim -s --all-progressive web.jpg

If the image is a (line) drawing, vector graphics are preferable to bitmaps.
Therefore, if possible, save them as SVGs. Those are easy to modify,
and will surely make translators' work easier as well.

PNG images should be optimised with `zopfli` (this is lossless as well).
For example, this reduced the Canoeboot boot logo from around 11k to 3k:

	zopflipng -ym image.png image.png

For development purposes, you might make your images local links first, and
then adjust the URLs when you submit your documentation/website patches.

Instructions are on the Untitled website, for how to set up your local version
of the website. Download untitled, and inside your `untitled` directory, create
a directory named `www/` then go inside the www directory, and clone the `cbwww`
repository there. Configure your local HTTP server accordingly.

Again, instructions are available on the Untitled website for this purpose.

### Name not required

Contributions that you make are publicly recorded, in a Git repository which
everyone can access. This includes the name and email address of the
contributor.

In Git, for author name and email address, you do not have to use identifying
data. You can use `canoeboot Contributor` and your email address could be
specified as contributor@canoeboot.org. You are permitted to do this, if
you wish to maintain privacy. We believe in privacy. If you choose to remain
anonymous, we will honour this.

Of course, you can use whichever name and/or email address you like.

Legally speaking, all copyright is automatic under the Berne Convention of
international copyright law. It does not matter which name, or indeed whether
you even declare a copyright (but we do require that certain copyright
licenses are used - read more about that on this same page).

If you use a different name and email address on your commits/patches, then you
should be fairly anonymous. Use
[git log](https://git-scm.com/book/en/v2/Git-Basics-Viewing-the-Commit-History)
and [git show](https://git-scm.com/docs/git-show) to confirm that before you
push changes to a public Git repository.

### Licenses (for contributors)

Make sure to freely license your work, under a libre license. Canoeboot no
longer sets arbitrary restrictions on what licenses are accepted, and many
licenses out there already exist. We will audit your contribution and tell
you if there are problems with it (e.g. no license).

*Always* declare a license on your work! Not declaring a license means that
the default, restrictive copyright laws apply, which would make your work
proprietary, subject to all of the same restrictions.

The MIT license is a good one to start with, and it is the preferred license
for all new works in Canoeboot, but we're not picky. Canoeboot has historically
used mostly GPL licensing; much of that remains, and is likely to remain.
It's your work; obviously, if you're deriving from an existing work,
it may make sense to use the same license on your contribution, for license
compatibility.

You can find common examples of licenses
[here](https://opensource.org/licenses).

If you *are* deriving from an existing work, it's important that your license
(for your contribution) be compatible with the licensing of the work from which
yours was derived. The MIT license is good because it's widely compatible
with many other licenses, and permits many freedoms (such as the freedom to
sublicense) that other licenses do not:

<https://opensource.org/licenses/MIT>

### Send patches

Make an account on <https://codeberg.org/> and navigate (while logged in) to the
repository that you wish to work on. Click *Fork* and in your account,
you will have your own repository of canoeboot. Clone your repository, make
whatever changes you like to it and then push to your repository, in your
account on Codeberg. You can also do this on a new branch, if you wish.

In your Codeberg account, you can then navigate to the official canoeboot
repository and submit a Pull Request. The way it works is similar to other
popular web-based Git platforms that people use these days.

You can submit your patches there. Alternative, you can log onto the canoeboot
IRC channel and notify the channel of which patches you want reviewed, if you
have your own Git repository with the patches.

Once you have issued a Pull Request, the canoeboot maintainers will be notified
via email. If you do not receive a fast enough response from the project, then
you could also notify the project via the `#canoeboot` channel on Libera Chat.

Another way to submit patches is to email Leah Rowe directly:
[info@minifree.org](mailto:info@minifree.org) is Leah's project email address.

However, for transparency of the code review process, it's recommended that you
use Codeberg, for the time being.

Mirrors of cbmk.git
-------------------

The `cbmk` repository contains Canoeboot's automated build system, which
produces Canoeboot releases (including compiled ROM images).

You can run `git clone` on any of these links (the links are also clickable,
to view changes in your Web browser):

* <https://git.sr.ht/~canoeboot/cbmk>
* <https://git.disroot.org/canoeboot/cbmk>
* <https://gitea.treehouse.systems/canoeboot/cbmk>
* <https://0xacab.org/canoeboot/cbmk/>
* <https://framagit.org/canoeboot/canoeboot>
* <https://gitlab.com/canoeboot/cbmk>
* <https://notabug.org/canoeboot/cbmk>

Mirrors of pico-serprog.git
--------------------------

* <https://notabug.org/libreboot/pico-serprog>

### cbwww.git mirror

The `cbwww` repository contains Markdown files (pandoc variant), for use
with the [Untitled Static Site Generator](https://untitled.vimuser.org/); this
is what Canoeboot uses to provide HTML web pages, *including* the page that
you are reading right now!

You can run `git clone` on these links, and/or click to view changes in your
Web browser. See:

* <https://git.sr.ht/~canoeboot/cbwww>
* <https://git.disroot.org/canoeboot/cbwww>
* <https://gitea.treehouse.systems/canoeboot/cbwww>
* <https://0xacab.org/canoeboot/cbwww>
* <https://framagit.org/canoeboot/cbwww/>
* <https://gitlab.com/canoeboot/cbwww>
* <https://notabug.org/canoeboot/cbwww>

cbwww-img.git mirror
----------------

* <https://git.sr.ht/~canoeboot/cbwww-img>
* <https://git.disroot.org/canoeboot/cbwww-img>
* <https://gitea.treehouse.systems/canoeboot/cbwww-img>
* <https://0xacab.org/canoeboot/cbwww-img>
* <https://framagit.org/canoeboot/cbwww-img/>
* <https://gitlab.com/canoeboot/cbwww-img>
* <https://notabug.org/canoeboot/cbwww-img>
