---
title: Contact the Canoeboot project
x-toc-enable: true
...

User support
-------------

IRC or Reddit are recommended, if you wish to ask for support (IRC recommended).
See below for information about IRC and Reddit.

Development discussion
--------------------

Mailing lists are planned for the future. For now, see notes
on [the Git page](git.md) for information about how to assist with development.

Instructions are also on that page for sending patches (via pull requests).

IRC chatroom
-------------

IRC is the main way to contact the Canoeboot project. `#canoeboot` on Libera
IRC.

Webchat:
<https://web.libera.chat/#canoeboot>

Libera is one of the largest IRC networks, used for Libre Software projects.
Find more about them here: <https://libera.chat/>

If you wish to connect using your preferred client (such as weechat or irssi),
the connection info is as follows:

* Server: `irc.libera.chat`
* Channel: `#canoeboot`
* Port (TLS): `6697`
* Port (non-TLS): `6667`

We recommend that you use port `6697` with TLS encryption enabled.  

It is recommend that you use SASL for authentication. These pages on the Libera
website tells you how:

* WeeChat SASL guide: <https://libera.chat/guides/weechat>
* Irssi SASL guide: <https://libera.chat/guides/irssi>
* HexChat SASL guide: <https://libera.chat/guides/hexchat>

In general, you should check the documentation provided by your IRC software.

Social media
-------------

Canoeboot exists officially on many places.

### Mastodon

The founder and lead developer, Leah Rowe, is on Mastodon:

* <https://mas.to/@libreleah>

Leah can also be contacted by this email address:
[leah@libreboot.org](mailto:leah@libreboot.org)

### Reddit

Mostly used as a support channel, and also for news announcements:
<https://www.reddit.com/r/canoeboot/>
