---
title: Who develops Canoeboot?
x-toc-enable: true
...

Leah Rowe, that's who. Leah single-handedly maintains Canoeboot, re-basing
upon newer releases of Libreboot every now and then. Leah Rowe is the founder
and lead developer for *both* projects; Leah maintains both
Canoeboot *and* Libreboot.

If you have a patch, or beef, talk to Leah.
