---
title: Download Canoeboot Free/Opensource BIOS/UEFI firmware
x-toc-enable: true
...

New releases are announced in the [main news section](news/).

If you're more interested in canoeboot development, go to the
[canoeboot development page](../git.md), which also includes links to the
Git repositories. The page on [/docs/maintain/](docs/maintain/) describes how
Canoeboot is put together, and how to maintain it. If you wish to build
Canoeboot from source, [read this page](docs/build/).

GPG signing key
---------------

**The latest release is Canoeboot 20250107, under the `canoeboot` directory.**

### NEW KEY

Full key fingerprint: `8BB1 F7D2 8CF7 696D BF4F  7192 5C65 4067 D383 B1FF

Use this to verify the most recent releases.

### OLD KEY

The releases from calendar year 2023 use this key.

Full key fingerprint: `98CC DDF8 E560 47F4 75C0  44BD D0C6 2464 FA8B 4856`

This is the same key used to sign [Libreboot releases](https://libreboot.org/download.html), which is also used to
sign Canoeboot releases since it's the same person maintaining both projects.

Download the key here: [lbkey.asc](lbkey.asc)

Canoeboot releases are signed using GPG.

	sha512sum -c sha512sum.txt
	gpg --verify sha512sum.txt.sig

Git repository
==============

Links to regular release archives are listed on this page.

However, for the absolute most bleeding edge up-to-date version of Canoeboot,
there is a Git repository that you can download from. Go here:

[How to download Canoeboot from Git](git.md)

Download Canoeboot from mirrors
===============================

Canoeboot releases are hosted on the same Rsync server as Libreboot, and
mirrors pick this up; look in the `canoeboot` directory on Libreboot mirrors.
For your convenience, these are linked below (on the mirror lists).

HTTPS mirrors {#https}
-------------

**The latest release is Canoeboot 20250107, under the `canoeboot` directory.**

These mirrors are recommended, since they use TLS (https://) encryption.

You can download Canoeboot from these mirrors:

* <https://www.mirrorservice.org/sites/libreboot.org/release/canoeboot/> (University
of Kent, UK)
* <https://mirrors.mit.edu/libreboot/canoeboot/> (MIT university, USA)
* <https://mirror.math.princeton.edu/pub/libreboot/canoeboot/> (Princeton
university, USA)
* <https://mirror.shapovalov.website/libreboot/canoeboot/> (shapovalov.website, Ukraine)
* <https://mirror.koddos.net/libreboot/canoeboot/> (koddos.net, Netherlands)
* <https://mirror-hk.koddos.net/libreboot/canoeboot/> (koddos.net, Hong Kong)
* <https://mirror.cyberbits.eu/libreboot/canoeboot/> (cyberbits.eu, France)

RSYNC mirrors {#rsync}
-------------

The following rsync mirrors are available publicly:

* <rsync://rsync.mirrorservice.org/libreboot.org/release/canoeboot/> (University of Kent,
UK)
* <rsync://mirror.math.princeton.edu/pub/libreboot/canoeboot/> (Princeton university, USA)
* <rsync://rsync.shapovalov.website/libreboot/canoeboot/> (shapovalov.website, Ukraine)
* <rsync://ftp.linux.ro/libreboot/canoeboot/> (linux.ro, Romania)
* <rsync://mirror.koddos.net/libreboot/canoeboot/> (koddos.net, Netherlands)
* <rsync://mirror-hk.koddos.net/libreboot/canoeboot/> (koddos.net, Hong Kong)

Are you running a mirror? Contact the canoeboot project, and the link will be
added to this page!

You can make your rsync mirror available via your web server, and also configure
your *own* mirror to be accessible via rsync. There are many resources online
that show you how to set up an rsync server.

How to create your own rsync mirror:

Useful for mirroring Canoeboot's entire set of release archives. You can put
an rsync command into crontab and pull the files into a directory on your
web server.

If you are going to mirror the entire set, it is recommended that you allocate
at least 25GiB. Canoeboot's rsync is currently about 12GiB, so allocating 25GiB
will afford you plenty of space for the future. At minimum, you should ensure
that at least 15-20GiB of space is available, for your Canoeboot mirror.

*It is highly recommended that you use the canoeboot.org mirror*, if you wish
to host an official mirror. Otherwise, if you simply want to create your own
local mirror, you should use one of the other mirrors, which sync from
canoeboot.org.

**NOTE: the rsync commands below will only download canoeboot. Remove
the `canoeboot/` part at the end of each path, if you also want to download
all of Libreboot; Libreboot and Canoeboot share the same Rsync server.**

Before you create the mirror, make a directory on your web server. For 
example:

	mkdir /var/www/html/libreboot/canoeboot/

Now you can run rsync, for instance:

	rsync -avz --delete-after rsync://rsync.canoeboot.org/mirrormirror/ /var/www/html/libreboot/canoeboot/

You might put this in an hourly crontab. For example:

	crontab -e

Then in crontab, add this line and save/exit (hourly crontab):

	0 * * * * rsync -avz --delete-after rsync://rsync.canoeboot.org/mirrormirror/ /var/www/html/libreboot/canoeboot/

**It's extremely important to have the final forward slash (/) at the end of each path,
in the above rsync command. Otherwise, rsync will behave very strangely.**

**NOTE: `rsync.canoeboot.org` is not directly accessible by the public, except
those whose IPs are whitelisted. For bandwidth reasons, the firewall running
on canoeboot.org blocks incoming rsync requests, except by specific IPs.**

**If you wish to run an rsync mirror, sync from one of the third party mirrors
above and set up your mirror. You can then contact Leah Rowe, to have your IP
addresses whitelisted for rsync usage - if the IP addresses match DNS A/AAAA
records for your rsync host, this can be used. A script runs in an hourly
crontab on canoeboot.org, that fetches the A/AAAA records of whitelisted
rsync mirrors, automatically adding rules permitting them to get through the
firewall.**

If you wish to regularly keep your rsync mirror updated, you can add it to a
crontab. This page tells you how to use crontab:
<https://man7.org/linux/man-pages/man5/crontab.5.html>

HTTP mirrors {#http}
------------

**The latest release is Canoeboot 20250107, under the `canoeboot` directory.**

WARNING: these mirrors are non-HTTPS which means that they are
unencrypted. Your traffic could be subject to interference by
adversaries. Make especially sure to check the GPG signatures, assuming
that you have the right key. Of course, you should do this anyway, even
if using HTTPS.

* <http://mirror.linux.ro/libreboot/canoeboot/> (linux.ro, Romania)
* <http://mirror.helium.in-berlin.de/libreboot/canoeboot/> (in-berlin.de, Germany)

FTP mirrors {#ftp}
-----------

**The latest release is Canoeboot 20250107, under the `canoeboot` directory.**

WARNING: FTP is also unencrypted, like HTTP. The same risks are present.

* <ftp://ftp.mirrorservice.org/sites/libreboot.org/release/canoeboot/> (University
of Kent, UK)
* <ftp://ftp.linux.ro/libreboot/canoeboot/> (linux.ro, Romania)
