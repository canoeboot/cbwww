---
title: Ліцензія шаблона
...

Цей веб-сайт написаний у Markdown і скомпільований у статичний HTML за допомогою
Pandoc. Використовується шаблон HTML, 
Авторське право (c) 2014--2017, Джон Макфарлейн

Зміни до нього захищені авторським правом Лією Роу 2021 року, випущені згідно з умовами
Creative Commons Zero license version 1.0 universal, яку ви можете знайти тут:
<https://creativecommons.org/publicdomain/zero/1.0/legalcode>

Ви можете знайти файл шаблону тут: [/template.include](/template.include) 

Файл `template.include` - це модифікована версія (змінена Лією Роу).
Оригінальний варіант можна знайти тут: [/template.original](/template.original)

Інші модифіковані шаблони можуть бути використані на певних сторінках. Перевірте це в
репозиторії Git для веб-сайта Canoeboot.

Оригінальний файл шаблону під назвою `template.original` від Джона Макфарлейна було
випущено за таких умов:

Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

Neither the name of John MacFarlane nor the names of other
contributors may be used to endorse or promote products derived
from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
OF THE POSSIBILITY OF SUCH DAMAGE.
