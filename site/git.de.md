---
title: Use Git and/or send patches to Canoeboot
x-toc-enable: true
...

Canoeboot Repositories
---------------------

Informationen darüber wer an Canoeboot arbeitet und wer das Projekt betreibt
sind unter [who.de.md](who.de.md) zu finden.

Das `canoeboot` Projekt hat hauptsächlich 3 Git Repositories:

* Build system: <https://codeberg.org/canoeboot/cbmk>
* Webseite (+Anleitungen): <https://codeberg.org/canoeboot/cbwww>
* Bilder (für die Webseite): <https://codeberg.org/canoeboot/cbwww-img>

Die Entwicklung von Canoeboot findet mithilfe der Versionskontrolle von
Git statt. Sieh in der [offiziellen Git Dokumentation](https://git-scm.com/doc)
nach, sofern Du nicht weisst wie man Git verwendet. 

Das `bucts` Repository wird auch vom Canoeboot Projekt gehostet, da das
Original Repository auf `stuge.se` nicht mehr verfügbar ist, seit wie dies
zuletzt geprüft haben. Das `bucts` Programm wurde von Peter Stuge geschrieben.
Du benötigst `bucts` sofern Du ein Canoeboot ROM intern auf ein Thinkpad X60 
oder T60 flashen möchtest, welches (derzeit) noch ein nicht-freies Lenovo 
BIOS verwendet. Anleitungen hierfür findest Du hier:\
[Canoeboot Installations Anleitungen](docs/install/)

Das `ich9utils` Repository wird erheblich vom `cbmk` build system verwendet.
Du kannst `ich9utils` allerdings auch separat herunterladen und verwenden.
Es erzeugt ICH9M descriptor+GbE Images für GM45 ThinkPads welche die ICH9M 
Southbridge verwenden. Es könnte auch für andere Systeme funktionieren,
welche dieselbe Platform bzw. denselben Chipsatz verwenden.
Dokumentation für `ich9utils` ist hier verfügbar:\
[ich9utils Dokumentation](docs/install/ich9utils.md)

### cbmk (canoeboot-make)

Dies ist das zentrale build system in Canoeboot. Man könnte auch sagen `cbmk` *ist*
Canoeboot! Das Git repository herunterladen:

	git clone https://codeberg.org/canoeboot/cbmk
  
Der oben dargestellte `git` Befehl, wird das Canoeboot build system `cbmk`
herunterladen.
Du kannst dann folgendermassen in das Verzeichnis wechseln:

	cd cbmk

Ändere dies nach deinen Vorstellungen oder erstelle einfach einen build. 
Für Anleitungen bzgl. `cbmk` build, siehe [build Anleitungen](docs/build/).

Informationen über das build system selbst und wie es funktioniert, sind
verfügbar unter dem [cbmk maintenance guide](docs/maintain/).

### cbwww and cbwww-img

Die *gesamte* Canoeboot Website sowie Dokumentation befindet sich in einem
Git Repository.
Du kannst es folgendermassen herunterladen:

	git clone https://codeberg.org/canoeboot/cbwww

Bilder befinden sich unter <https://av.canoeboot.org/> und sind verfügbar
in einem separaten Git Repository:

	git clone https://codeberg.org/canoeboot/cbwww-img

Du kannst alles nach deinen Vorstellungen ändern. Beachte die nachfolgenden
Informationen wie Du deine Änderungen zur Verfügung stellen kannst.

Die gesamte Website ist in Markdown geschrieben, insbesondere die Pandoc Version. 
Die statischen HTML Seiten werden mit [Untitled](https://untitled.vimuser.org/)
generiert. Leah Rowe, die Gründerin von Canoeboot, ist auch die Gründerin des Untitled static
site generator Projekts.

Wenn Du möchtest, kannst Du einen lokalen HTTP Server einrichten und eine
lokale Version der Website herstellen. Bitte bedenke, dass alle Bilder nach
wie vor mit den Bildern auf <https://av.canoeboot.org/> verknüpft werden,
daher werden jegliche Bilder die Du `cbwww-img` hinzugefügt hast nicht auf
deiner lokalen `cbwww` Seite dargestellt, sofern Du die Bilder (die Du
hinzugefügt hast) mit `av.canoeboot.org` verknüpfst. Es ist jedoch erforderlich, 
dass sich diese Bilder auf av.canoeboot.org befinden.

Sofern Du der Webseite Bilder hinzufügen möchtest, füge diese ebenso
dem `cbwww-img` Repository hinzu, indem Du diese dann jeweils mit diesem Link verknüpfst
<https://av.canoeboot.org/path/to/your/new/image/in/cbwww-img>.
Wenn dein Patch der Canoeboot Webseite hinzugefügt wird, werden erscheinen deine Bilder live.

If adding a photo, compress it for web distribution. Images should be about
800px wide, and usually under 100KiB in size:

First, scale your image down to approximately 800px width, using your favourite
image manipulation program. For example, with `imagemagick` you can do the
following (make sure the image isn't already smaller or equal than preferred).

        convert original.jpg -resize 600000@ -quality 70% web.jpg

You should always run `jpegoptim` on jpg images before submitting them.
It strips useless metadata and *losslessly* optimises them further by cleverly
rearranging the huffman tables used in them.

        jpegoptim -s --all-progressive web.jpg

If the image is a (line) drawing, vector graphics are preferable to bitmaps.
Therefore, if possible, save them as SVGs. Those are easy to modify,
and will surely make translators' work easier as well.

PNG images should be optimised with `zopfli` (this is lossless as well).
For example, this reduced the Canoeboot boot logo from around 11k to 3k:

        zopflipng -ym image.png image.png

Zu Entwicklungszwecken, könntest Du deine Bilder auch lokal verknüpfen, und
anschliesend die URLs anpassen sobald Du deine Patches für die Dokumentation/Webseite schickst.

Eine Anleitung wie Du eine lokale Version der Webseite herstellen kannst,
befinden sich auf der Untitled Webseite. Lade untitled
herunter, und erstelle in dem `untitled` Verzeichnis ein Verzeichnis mit
dem Namen `www/` dann wechsle in dieses Verzeichnis und klone das `cbwww`
Repository dorthin. Konfiguriere deinen lokalen HTTP Server entsprechend.

Nochmal, Anleitungen hierfür findest Du auf der Untitled Webseite.

### Name nicht erforderlich

Beiträge die Du hinzufügst, werden in einem für jeden zugänglichen Git
Repository öffentlich aufgezeichnet. Dies betrifft ebenso den Namen sowie
die email Adresse des Mitwirkenden.

Du musst bei Git keinen Autoren Namen bzw. keine email Addresse verwenden,
mithilfe derer Du identifizierbar bist. Du kannst `canoeboot Contributor`
verwenden und deine email Addresse könnte als contributor@canoeboot.org
spezifiert werden. Es ist Dir gestattet dies zu tun, sofern Du deine Privatsphäre
wahren möchtest. Wir glauben an Privatsphäre. Sofern Du anonym bleiben möchtest 
werden wir dies respektieren.

Natürlich kannst Du jeglichen Namen und/oder jegliche email Adresse verwenden
die Du möchtest.

Rechtlich gesprochen, jegliches Urheberrecht fällt automatisch unter die
Berner Übereinkunft zum Schutz von Werken der Literatur und Kunst. Es spielt
keine Rolle welchen Namen, oder ob Du tatsächlich überhaupt ein Urheberrecht
deklariert hast (aber wir setzen voraus das bestimmte Lizenzen für das
Urheberrecht verwndet werden - lies mehr darüber auf dieser Seite).

Sofern Du einen anderen Namen sowie eine andere email Adresse für deine 
Commits/Patches verwendest dann solltest Du anonym sein. Verwende 
[git log](https://git-scm.com/book/en/v2/Git-Basics-Viewing-the-Commit-History)
und [git show](https://git-scm.com/docs/git-show) um dies zu überprüfen
bevor Du einem öffentlichen Git Repository Änderungen hinzufügst.

### Lizenzen (für Mitwirkende)

Stelle sicher, dass deine Beiträge mit einer libre Lizenz frei lizensiert
sind. Canoeboot schreibt nicht mehr vor, welche Lizenzen akzeptiert werden,
und es existieren einige Lizenzen. Wir werden deinen Beitrag prüfen und
dir mitteilen sofern es ein Problem damit gibt (z.B. keine Lizenz).

Gib *immer* eine Lizenz an für deine Arbeit! Keine Lizenz anzugeben bedeutet
das deine Arbeit unter die Standard Urheberrechte fällt, was deine Arbeit 
proprietär macht und somit von denselben Einschränkungen betroffen ist.

Die MIT Lizenz ist ein guter Start, und sie ist die bevorzugte Lizenz
für sämtliche Arbeit an Canoeboot, aber wir sind nicht pingelig. Canoeboot 
hat in der Vergangenheit GNU Lizenzen so wie GPL verwendet; vieles davon
besteht nach wie vor und wird auch weiterhin bestehen.
Es ist deine Arbeit; sofern deine Arbeit auf der Arbeit eines anderen basiert,
ist es aufgrund der Lizenz-Kompatibilität ggfs. naheliegend diesselbe Lizenz zu
verwenden.

[Hier](https://opensource.org/licenses) findest Du übliche Beispiele für Lizenzen.

*Wenn* deine Arbeit auf bestehender Arbeit basiert, dann ist es wichtig
(für deinen Beitrag) das die Lizenz deiner Arbeit kompatibel ist mit der
Lizenz der Arbeit auf der sie beruht. Die MIT Lizenz ist hierfür gut geeignet,
weil sie mit vielen anderen Lizenen kompatibel ist, und Freiheit zulässt
(wie zum Beispiel die Freiheit einer SubLizenz) was bei vielen anderen
Lizenzen nicht der Fall ist:

<https://opensource.org/licenses/MIT>

### Patches senden

Erstelle einen Account unter <https://codeberg.org/> und navigiere (während 
Du eingeloggt bist) zu dem Repository das Du bearbeiten möchtest. Klicke 
auf *Fork* und Du wirst ein eigenes Canoeboot Repository in deinem Account 
erhalten. Erstelle einen Clone dieses Repository, füge alle gewünschten Änderungen hinzu
und führe anschliessend einen Push in dein Repository in deinem Account
auf Codeberg durch. Du kannst dies auch in einem neuen Branch erledigen,
sofern Du magst.

In deinem Codeberg Account kannst Du nun zum offiziellen Canoeboot
Repository navigieren und dort einen Pull Request erstellen. Die Art und 
Weise wie dies funktioniert ist vergleichbar mit anderen populären Web basierten
Git Plattformen die heutzutage verwendet werden. 

Du kannst dort deine Patches bereitstellen. Alternativ kannst Du dich in
den Canoeboot IRC Kanal einloggen und dort bekannt geben welche deiner Patches 
geprüft werden sollen, sofern Du ein eigenes Git repository mit den Patches
hast.

Sobald Du einen Pull Request erstellt hast, werden die Canoeboot Maintainer
per email informiert. Sofern Du nicht schnell genug eine Antwort erhälst,
kannst Du das Projekt ebenso mithilfe des `#canoeboot` Kanals auf Libera
Chat kontaktieren.

Ein weiterer Weg Patches zu senden ist Leah Rowe direkt eine email zu senden:
[info@minifree.org](mailto:info@minifree.org) ist Leah's Projekt email Addresse.

Um den Prozess der Quelltext Überprüfung transparent zu gestalten,
wird jedoch empfohlen künftig Codeberg zu verwenden.

Mirrors für cbmk.git
--------------------

Das `cbmk` Repository enthält Canoeboot's automatischess build system, welches
Canoeboot Veröffentlichungen  herstellt (inklusive kompilierter ROM Images).

Du kannst `git clone` für alle diese Links ausführen (die Links können auch
angeklickt werden, um Änderungen in deinem Web Browser anzusehen):

* <https://git.sr.ht/~canoeboot/cbmk>
* <https://git.disroot.org/canoeboot/cbmk>
* <https://gitea.treehouse.systems/canoeboot/cbmk>
* <https://0xacab.org/canoeboot/cbmk/>
* <https://framagit.org/canoeboot/canoeboot>
* <https://gitlab.com/canoeboot/cbmk>
* <https://notabug.org/canoeboot/cbmk>

Mirrors fur pico-serprog.git
----------------------------

* <https://notabug.org/libreboot/pico-serprog>

### cbwww.git Mirror

Das `cbwww` Repository enthält Markdown Dateien (Pandoc Variant), für die
Verwendung mit dem [Untitled Static Site Generator](https://untitled.vimuser.org/);
dies wird von Canoeboot verwendet um HTML Web Seiten bereitzustellen, *inklusive*
der Seite die Du gerade liest!

Du kannst `git clone` für diese Links ausführen und/oder die Links
anklicken um Änderungen in deinem Web Browser anzusehen). Siehe:

* <https://git.sr.ht/~canoeboot/cbwww>
* <https://git.disroot.org/canoeboot/cbwww>
* <https://gitea.treehouse.systems/canoeboot/cbwww>
* <https://0xacab.org/canoeboot/cbwww>
* <https://framagit.org/canoeboot/cbwww/>
* <https://gitlab.com/canoeboot/cbwww>
* <https://notabug.org/canoeboot/cbwww>

cbwww-img.git Mirror
----------------

* <https://git.sr.ht/~canoeboot/cbwww-img>
* <https://git.disroot.org/canoeboot/cbwww-img>
* <https://gitea.treehouse.systems/canoeboot/cbwww-img>
* <https://0xacab.org/canoeboot/cbwww-img>
* <https://framagit.org/canoeboot/cbwww-img/>
* <https://gitlab.com/canoeboot/cbwww-img>
* <https://notabug.org/canoeboot/cbwww-img>
