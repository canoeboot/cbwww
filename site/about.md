---
title: What is Canoeboot?
x-toc-enable: true
...

The main purpose of this article is to describe how the Canoeboot project
operates, why it exists and what it does. Who, what, why and when.

What is Canoeboot?
===================

Canoeboot is free/libre boot firmware based on Libreboot (which is in turn
based on coreboot), replacing proprietary BIOS/UEFI firmware on select x86/ARM
laptops, desktops and server mainboards. It provides an [automated build
system](docs/maintain/)
for [compiling coreboot ROM images](docs/build/), that are [easy to
install](docs/install/) for non-technical
users. The emphasis is placed upon ease of use, and optional [security
features](docs/gnulinux/grub_hardening.md).

Users take this automation for granted today, but Libreboot was the first such
project to implement this. It, like Canoeboot, is a *coreboot distro* in the
same way that *Trisquel* is a GNU+Linux distro. Similar projects now exist, today,
inspired by Libreboot's example. Coreboot is notoriously difficult to configure and install
for most non-technical users, but Libreboot and Canoeboot make it easier.

However, *Libreboot* no longer complies with GNU policy. In November 2022,
Libreboot adopted a more pragmatic policy of allowing any board from coreboot
to be supported, while reducing the number of binary blobs as much as possible.
Although this may satisfy most people, there exists a minority of people who
wish to still have a blob-free coreboot distro, like Libreboot once was.

Prior to November 2022, Libreboot complied fully with GNU policy in providing
an entirely blob-free coreboot distribution. The rest of this article will go
into a lot more detail, both on this and on the technical aspects, but the
gist of it is this:

Canoeboot is, in spirit and in practise, a continuation of the *old* Libreboot
project, prior to that policy change. It maintains sync with Libreboot as closely
as possible, while removing any and all non-free code, and disabling/removing
any code that could possibly handle it (for example modifying coreboot so as
to never add microcode updates or download blobs, even if told to by coreboot
configs, since the upstream coreboot project is otherwise engineered to handle
these if requested by the user).

Overview of operation
-------------------

More specifically, Canoeboot is a *fork* of Libreboot, maintained in parallel
as per each Libreboot release. Canoeboot adheres to the [GNU Free System
Distribution Guidelines](https://www.gnu.org/distros/free-system-distribution-guidelines.en.html),
and such adherence (to GNU FSDG) is the main purpose of Canoeboot. It consequently supports far less
hardware than Libreboot. *However*, this also means that Canoeboot is an
excellent choice for the purists out there who adhere to the GNU Free Software
ideology, and wish to use nothing but *Free Software*.

Canoeboot is essentially providing well-engineered releases showing what Libreboot
would be like if it *didn't* implement such a policy (in sharp contrast to the GNU
one that Canoeboot implements). Libreboot previously adhered to the GNU FSDG
policy, but adopted the *Binary Blob Reduction Policy* in November 2022, in an
effort to increase the number of mainboards that can be supported from coreboot.

Canoeboot was created because there are still a few people who want this sort
of thing, but there weren't any modern, or otherwise high quality implementations.
Thus, I decided to revive the old Libreboot project myself, forking from my very
own project (Libreboot) and calling the new fork Canoeboot. *I forked my own project.*

I'm writing in the first person. Who's this *I* you're reading about? It's me,
Leah Rowe; I am the founder and lead developer of Libreboot, *and also* the
founder and lead developer of Canoeboot! I maintain both projects, keeping them
in relative sync between releases, often performing same-day simultaneous
Canoeboot and Libreboot releases.

Who?
------

Canoeboot is maintained by the same founder, Leah Rowe, who is the founder and
lead developer of both the Libreboot project *and* the Canoeboot project.
Maintaining a project like Canoeboot is both challenging and fun; Canoeboot does
not permit any binary blobs from coreboot, which means that it can only support
a handful of mainboards from coreboot, and sometimes
several [mitigations](https://browse.libreboot.org/lbmk.git/plain/resources/coreboot/default/patches/0012-fix-speedstep-on-x200-t400-Revert-cpu-intel-model_10.patch?id=9938fa14b1bf54db37c0c18bdfec051cae41448e)
may be required to stabilise certain functionalities under these conditions.

Release schedule
--------------

The Canoeboot schedule is: whenever a Libreboot release is ready, produce a
new Canoeboot release based on it, when there are enough changes to warrant a
new release.

The intention, moving forward, is that Canoeboot will track *stable* releases
of Libreboot. Development is done mainly on Libreboot, and ported over to
Canoeboot periodically; any work that isn't suitable for Canoeboot (such as
scripts for handling binary blobs) are *not* ported over to Canoeboot.

How releases are engineered
-----------------

It's actually very simple. Here is the method by which Canoeboot releases are
created:

* Take an archive of Libreboot's git repositories (build system, website and
  images), at the version that the current Canoeboot release is based on.
* Take the *new* Libreboot release, that is the target for Canoeboot-ising.
* Diff the two revisions in bulk (as-is), by re-initialising Git history in
  the old Libreboot revision; then copy `.git/` to the directory containing the
  new revision.
* In the directory containing the new revision, commit all of the changes.
* In the directory containing the new revision, run this
  command: `git format-patch -n1`
* The resulting `.patch` file will then show all of the changes.

The resulting patch file is then opened up in *Vim*.

In the Canoeboot repository, these changes are then copied over. This is done
by scrolling through the patch file, deciding which changes are worthy. The
policy is to include *all* changes, except those that are not suitable under
FSDG.

*Then* the following is done, for coreboot and u-boot trees *per tree*:

* Take the old revision of a given tree (e.g. `coreboot/default`), and diff it
  with the entire source tree on the same tree (e.g. `coreboot/default`) but
  on the new revision; then from the diff, get the list of all files that have
  been *added* and all of the files that have been *modified* (ignore files that
  have been deleted; also keep track of files that have renamed). This can be
  done very automatically with Git.
* Based on that, a list of files for scanning is now available. Next,
  the `deblob-check` script is executed within that source tree, using that
  list as input. For example: `./deblob-check $(cat /path/to/list) > sus.list`
* The resulting `sus.list` file contains all results, and this new list of files
  can then be checked. This is checked *manually*, but usually doesn't take very
  long (it's never more than a couple hundred files, and it's easy to see within
  like 5 seconds whether it's a blob: 500 seconds if it's 100 files).
* Any false positives are ignored, while actual blobs are then added to
  the correct file, e.g. `config/coreboot/default/blobs.list`.
* Next, documentation is scanned. The same process is used (track new files,
  moved files and changed files), but there is no automation for this. Every
  changed/moved/added file must be checked manually. This is to check for any
  documentation that recommends or endorses any proprietary code. Whole files
  can be deleted in this way; a normal diff can be provided to clean up other
  files, under e.g. `config/coreboot/default/patches/` if necessary.
* There may also be cases where a given bit of code is *not a blob*, but still
  proprietary, e.g. source code provided with restrictions on usage,
  modification or distribution.

Libreboot often contains hundreds of changes per release, so it would be quite
inefficient to cherry-pick individual patches. Therefore, all development is
done in Libreboot exclusively, and Canoeboot is the by-product of that
development, updating every now and then.

The above steps are a general guide, but specific tweaks may also be required
in the build system, for a new release; minor edge cases here and there, that
are different for every release, but they are usually very minor.

The `deblob-check` script is from linux-libre, a GNU fork of Linux that is
de-blobbed, but the same script works on any source tree, except it will flag
all of the false positives on non-Linux source trees; it scans heuristically
for binary blobs.

This is how Canoeboot can provide releases so quickly, based upon each release
of Libreboot. Extensive testing is performed on ROM images compiled under the
Libreboot build system, so the Canoeboot images are also easy to verify, since
a Canoeboot release will always be closely based on a Libreboot release.

This is actually the benefit of Canoeboot, over all other FSDG-derived coreboot
distros, because the other projects do not have as good infrastructure or the
level of resources *or* technical knowledge that Libreboot has. Libreboot
provides high quality releases which are then filtered by order of the protocol
described above, to provide Canoeboot releases.
