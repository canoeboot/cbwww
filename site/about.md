---
title: What is Canoeboot?
x-toc-enable: true
...

The main purpose of this article is to describe how the Canoeboot project
operates, why it exists and what it does. Who, what, why and when.

Open source BIOS/UEFI firmware
------------------

Canoeboot is free/libre boot firmware based on [Libreboot](https://libreboot.org/) (which is in turn
based on coreboot), replacing proprietary BIOS/UEFI firmware on select x86/ARM
laptops, desktops and server motherboards. It provides an [automated build
system](docs/maintain/)
for [compiling coreboot ROM images](docs/build/), that are [easy to
install](docs/install/) for non-technical
users. The emphasis is placed upon ease of use, and optional [security
features](docs/linux/grub_hardening.md).

Users take this automation for granted today, but Libreboot was the first such
project to implement this. It, like Canoeboot, is
a *[coreboot distro](docs/maintain/)* in the
same way that *Debian* is a Linux distro. Similar projects now exist, today,
inspired by Libreboot's example. Coreboot is notoriously difficult to configure and install
for most non-technical users, but Libreboot and Canoeboot make it easier.

### How does Canoeboot compare to Libreboot?

More specifically, Canoeboot is a *fork* of Libreboot, maintained in parallel
as per each Libreboot release; Canoeboot maintains
a [zero-blob policy](news/policy.md), in contrast to Libreboot's
[Binary Blob Reduction Policy](https://libreboot.org/news/policy.html).
Canoeboot consequently supports far less hardware than Libreboot. Canoeboot
even *excludes CPU microcode updates*, regardless of the negative impact this
has on system stability. The purpose of Canoeboot is simply to provide free
boot firmware that is *fully* [Free Software](https://writefreesoftware.org/learn)
for the purists out there.

This means Canoeboot is technically inferior to Libreboot. Most people should
probably just use Libreboot. Canoeboot is essentially a well-engineered
proof of concept that shows what is possible when you take such a strict
approach. The term *inferior* is not meant as an insult, but it is a fact
that Canoeboot supports much less hardware than Libreboot, and
much *older* hardware; for example Libreboot can run on intel 8th gen
and newer, while Canoeboot is stuck on Core 2 Duo era hardware, at least
on Intel (Core 2 Duo machines from the year 2008). We do support some
nice AMD server boards in Canoeboot (e.g. ASUS KGPE-D16) but Libreboot can
go much further than Canoeboot, or any other project like it.

The approach taken by Canoeboot is the same one that Libreboot used to take,
from it's founding in 2013, right up until November 2022. On 17 November 2022,
Libreboot switched to the
[Binary Blob Reduction Policy](https://libreboot.org/news/policy.html),
which is still quite strict; it still prohibits binary blobs from being
included, including them only when absolutely required, because some boards
from coreboot still require certain code from the vendor (e.g. raminit
may not be implemented on some newer boards, so code from the vendor is used
in such cases, e.g. Intel MRC/FSP).

In practise, Libreboot *also* provides fully free firmware on a lot of
newer boards, depending on your point of view. For example, on Intel
platforms up to Haswell, free raminit is available and the only code needed
from the vendor is Intel ME, which is configured via `me_cleaner` on
Libreboot releases, disabling the ME after early bringup. See:
<https://github.com/corna/me_cleaner> - use of `me_cleaner` can be
considered *de-blobbing*, since it is removing almost all of the code
from the ME, but on most newer platforms, a small rump of ME code is still
required for power management as one example.

More information about precisely how *Libreboot* differs from Canoeboot,
and therefore how Canoeboot differs from Libreboot, can be gleaned by looking
at the following resources:

* [Libreboot Binary Blob Reduction Policy](https://libreboot.org/news/policy.html)
* [Canoeboot Binary Blob Extermination Policy](news/policy.md)
* [Libreboot Freedom Status](https://libreboot.org/freedom-status.html)

And check the hardware compatibility list of both projects:

* [Libreboot supported hardware](https://libreboot.org/docs/install/#which-systems-are-supported-by-libreboot)
* [Canoeboot supported hardware](docs/install/#which-systems-are-supported-by-canoeboot)

I'm writing in the first person. Who's this *I* you're reading about? It's me,
Leah Rowe; I am the founder and lead developer of Libreboot, *and also* the
founder and lead developer of Canoeboot! I maintain both projects, keeping them
in relative sync between releases, often performing same-day simultaneous
Canoeboot and Libreboot releases.

So why is Canoeboot even maintained, if it's inferior to Libreboot? Why
maintain two projects? The answer is simple: Canoeboot is a fun technical
challenge to see what can be done under such restrictive and dogmatic
policies. Maintaining it is also not that hard, because the design is
mostly kept in sync with Libreboot, on a per-commit basis, so the amount
of development overhead is actually quite minimal.

For example, both build systems both use the exact same design. See:

* [Libreboot build system design](https://libreboot.org/docs/maintain/)
* [Canoeboot build system design](docs/maintain/)

With few exceptions, Canoeboot generally keeps in sync with Libreboot
at all times, usually making releases on the exact same day. When a
Libreboot release comes out, Canoeboot usually also does a corresponding
release on the same day, or the day after.

### Who?

Canoeboot is maintained by the same founder, Leah Rowe, who is the founder and
lead developer of both the Libreboot project *and* the Canoeboot project.
Maintaining a project like Canoeboot is both challenging and fun; Canoeboot does
not permit any binary blobs from coreboot, which means that it can only support
a handful of motherboards from coreboot, and sometimes
several [mitigations](https://browse.libreboot.org/lbmk.git/plain/resources/coreboot/default/patches/0012-fix-speedstep-on-x200-t400-Revert-cpu-intel-model_10.patch?id=9938fa14b1bf54db37c0c18bdfec051cae41448e)
may be required to stabilise certain functionalities under these conditions.

### Release schedule

The Canoeboot schedule is: whenever a Libreboot release is ready, produce a
new Canoeboot release based on it, when there are enough changes to warrant a
new release.

The intention, moving forward, is that Canoeboot will track *stable* releases
of Libreboot. Development is done mainly on Libreboot, and ported over to
Canoeboot periodically; any work that isn't suitable for Canoeboot (such as
scripts for handling binary blobs) are *not* ported over to Canoeboot.

### How releases are engineered

It's actually very simple. Here is the method by which Canoeboot releases are
created:

* Take an archive of Libreboot's git repositories (build system, website and
  images), at the version that the current Canoeboot release is based on.
* Take the *new* Libreboot release, that is the target for Canoeboot-ising.
* Diff the two revisions in bulk (as-is), by re-initialising Git history in
  the old Libreboot revision; then copy `.git/` to the directory containing the
  new revision.
* In the directory containing the new revision, commit all of the changes.
* In the directory containing the new revision, run this
  command: `git format-patch -n1`
* The resulting `.patch` file will then show all of the changes.

The resulting patch file is then opened up in *Vim*.

In the Canoeboot repository, these changes are then copied over. This is done
by scrolling through the patch file, deciding which changes are worthy. The
policy is to include *all* changes, except those that are not suitable under
Canoeboot's [policy](news/policy.md).

*Then* the following is done, for coreboot and u-boot trees *per tree*:

* Take the old revision of a given tree (e.g. `coreboot/default`), and diff it
  with the entire source tree on the same tree (e.g. `coreboot/default`) but
  on the new revision; then from the diff, get the list of all files that have
  been *added* and all of the files that have been *modified* (ignore files that
  have been deleted; also keep track of files that have renamed). This can be
  done very automatically with Git.
* Based on that, a list of files for scanning is now available. Next,
  the `deblob-check` script is executed within that source tree, using that
  list as input. For example: `./deblob-check $(cat /path/to/list) > sus.list`
* The resulting `sus.list` file contains all results, and this new list of files
  can then be checked. This is checked *manually*, but usually doesn't take very
  long (it's never more than a couple hundred files, and it's easy to see within
  like 5 seconds whether it's a blob: 500 seconds if it's 100 files).
* Any false positives are ignored, while actual blobs are then added to
  the correct file, e.g. `config/coreboot/default/blobs.list`.
* Next, documentation is scanned. The same process is used (track new files,
  moved files and changed files), but there is no automation for this. Every
  changed/moved/added file must be checked manually. This is to check for any
  documentation that recommends or endorses any proprietary code. Whole files
  can be deleted in this way; a normal diff can be provided to clean up other
  files, under e.g. `config/coreboot/default/patches/` if necessary.
* There may also be cases where a given bit of code is *not a blob*, but still
  proprietary, e.g. source code provided with restrictions on usage,
  modification or distribution.

Libreboot often contains hundreds of changes per release, so it would be quite
inefficient to cherry-pick individual patches. Therefore, all development is
done in Libreboot exclusively, and Canoeboot is the by-product of that
development, updating every now and then.

The above steps are a general guide, but specific tweaks may also be required
in the build system, for a new release; minor edge cases here and there, that
are different for every release, but they are usually very minor.

Upstream sources are rigorously checked, comparing files that have changed
between releases. This takes time, but the process is largely automated.

This is how Canoeboot can provide releases so quickly, based upon each release
of Libreboot. Extensive testing is performed on ROM images compiled under the
Libreboot build system, so the Canoeboot images are also easy to verify, since
a Canoeboot release will always be closely based on a Libreboot release.

