---
title: What is Canoeboot?
x-toc-enable: true
...

The main purpose of this article is to describe how the Canoeboot project
operates, why it exists and what it does. Who, what, why and when.

You may also be interested in: [Canoeboot vs GNU Boot](gnuboot.md)

What is Canoeboot?
===================

Canoeboot is free/opensource boot firmware based on Libreboot (which is in turn
based on coreboot), replacing proprietary BIOS/UEFI firmware on select x86/ARM
laptops, desktops and server mainboards. It provides an [automated build
system](docs/maintain/)
for [compiling coreboot ROM images](docs/build/), that are [easy to
install](docs/install/) for non-technical
users. The emphasis is placed upon ease of use, and optional [security
features](docs/gnulinux/grub_hardening.md).

Users take this automation for granted today, but Libreboot was the first such
project to implement this. It, like Canoeboot, is a *coreboot distro* in the
same way that *Debian* is a Linux distro (that uses the GNU C Library and
coreutils, among other things). Similar projects now exist, today, inspired by
Libreboot's example. Coreboot is notoriously difficult to configure and install
for most non-technical users, but Libreboot and Canoeboot make it easier.

Overview of operation
-------------------

More specifically, Canoeboot is a *fork* of Libreboot, maintained in parallel
as per each Libreboot release. Canoeboot adheres to the [GNU Free System
Distribution Guidelines](https://www.gnu.org/distros/free-system-distribution-guidelines.en.html),
instead of Libreboot's more pragmatic [Binary Blob Reduction
Policy](https://libreboot.org/news/policy.html) - and such adherence (to GNU
FSDG) is the main purpose of Canoeboot. It consequently supports far less
hardware than Libreboot, providing a proof of concept showing what Libreboot
would be like if it *didn't* implement such a policy (in opposition to the GNU
one that Canoeboot implements). Libreboot previously adhered to the GNU FSDG
policy, but adopted the *Binary Blob Reduction Policy* in November 2022, in an
effort to increase the number of mainboards that can be supported from coreboot.

Thus, Canoeboot is a representation of the *old* Libreboot project. Coreboot
is nominally free software, but requires binary blobs for certain initialisation
on certain boards; only very few mainboards can boot without them. You can read
about how *Libreboot* handles this (in contrast to Canoeboot which bans all
binary blobs), by reading the Libreboot Freedom Status page:
<https://libreboot.org/freedom-status.html>

Canoeboot was created because there are still a few people who want this sort
of thing, but there weren't any modern, or otherwise high quality
implementations. Thus, I decided to revive the old Libreboot project myself,
forking from my very own project (Libreboot) and calling the new fork Canoeboot.
*I forked my own project.*

Who?
------

Canoeboot is maintained by the same founder, Leah Rowe, who is the founder and
lead developer of both the Libreboot project *and* the Canoeboot project.
Maintaining a project like Canoeboot is both challenging and fun; Canoeboot does
not permit any binary blobs from coreboot, which means that it can only support
a handful of mainboards from coreboot, and sometimes
several [mitigations](https://browse.libreboot.org/lbmk.git/plain/resources/coreboot/default/patches/0012-fix-speedstep-on-x200-t400-Revert-cpu-intel-model_10.patch?id=9938fa14b1bf54db37c0c18bdfec051cae41448e)
may be required to stabilise certain functionalities under these conditions.

The project called *[GNU Boot](https://libreboot.org/news/gnuboot.html)* launched
during June 2023, in opposition to Libreboot's new policy as of November 2022;
however, GNU Boot's base Libreboot revision was from around the time of the
Libreboot 20220710 release, so their base was roughly 1 year out of date
compared to Libreboot. Canoeboot was created to directly compete with GNU Boot,
which is lead by different people.

GNU Boot *originally* started as a hostile fork attempt; the FSF tried to steal
the name Libreboot by setting up a Libreboot project at another TLD (instead of
libreboot.org) and claiming to be the real thing, despite the fact that the
real Libreboot project was actively developed, and doing releases. [This
video](https://media.libreplanet.org/u/libreplanet/m/taking-control-over-the-means-of-production-free-software-boot/)
at LibrePlanet 2023, on 19 March 2023, announced the FSF's *Libreboot* project,
which was the *hostile* fork; this was anticipated, so the *real* Libreboot at
libreboot.org made a new release that same day, having prepared for it several
weeks beforehand. This resulted in the *[Libreboot 20230319
release](https://libreboot.org/news/libreboot20230319.html)*, and
started an aggressive counter-coup out-competing them. It succeeded, and the FSF
lost; they renamed their project to GNU Boot, starting from June 2023.

Canoeboot was originally designated as *GNU Boot* unofficially, launching in
the month of July 2023 as a proof of concept based
upon [Libreboot 20230625](https://libreboot.org/news/libreboot20230625.html) to
help the [real GNU Boot](https://libreboot.org/news/gnuboot.html) get up to date
versus Libreboot. GNU Boot's June 2023 revision was nearly 1 year behind on
Libreboot. The unofficial GNU Boot was renamed and re-branded to nonGeNUine Boot
on [21 July 2023](../news/nongenuineboot20230717.html#update-21-july-2023) when
the real GNU Boot sent a legal threat to the Libreboot project (citing trademark
infringement) - an innocent mistake, based on a misunderstanding. The purpose
of the original *unofficial* project was merely to provide a base for the real
GNU Boot project to just use however they wish, but they took it the wrong way.
The logic then was: if there is *going* to be another FSDG distro, continuing
on from the *old* Libreboot policy (based on GNU FSDG), it should at least be
high quality, and released to a high standard, so the unofficial project was
made to show them precisely how the Libreboot project is run, so as to provide
them with inspiration.

GNU FSDG policy is [heavily
misguided](https://libreboot.org/news/policy.html#problems-with-fsdg). Libreboot
is vastly superior to Canoeboot, but it can at least be developed to a high
standard (within the FSF and GNU dogma); as of 26 October 2023, GNU Boot's build
system and revisions were still largely based on Libreboot 20220710, with very
few meaningful changes; Canoeboot was released on that day (26 Oct 2023), based
on Libreboot 20231021 which is *13 months newer* than Libreboot 20220710; the
[nonGeNUine Boot 20230717 change log](news/nongenuineboot20230717.md) was in
reference to Libreboot 20220710, and the [Canoeboot 20231026 change
log](news/canoeboot20231026.md) is in reference to nonGeNUine Boot 20230717 -
if you combine both change logs, you then more or less have a list of all the
things *Canoeboot 20231026* can do that *GNU Boot* 0.1 RC didn't. It's a lot!

On this day, 26 October 2023, Canoeboot also supported *more boards* than GNU
Boot, namely: Dell Latitude E6400, gru\_bob chromebooks and gru\_kevin
chromebooks. The chromebooks are using ARM-based RK3399 SoCs with a heavily
modified version of U-Boot, developed for Libreboot. GNU Boot still didn't
have bootable U-Boot configurations on that day!

nonGeNUine Boot was renamed to Canoeboot, with the [Canoeboot 20231026
release](../news/canoeboot20231026.md) in October 2023, based
upon [Libreboot 20231021](https://libreboot.org/news/libreboot20231021.html).

Why does Canoeboot exist?
----

On the day of the first Canoeboot release, GNU Boot 0.1 RC was out but not much
different to the state it was in during July 2023, and they had not accepted the
help of the original nonGeNUine Boot project; their 0.1 RC release was essentially
Libreboot 20220710, but with a few build fixes on newer distros, some of which
were created by the unofficial GNU Boot first (e.g. fix KGPE-D16 build errors).
It was decided then that Canoeboot would be created, as an official project in
direct competition with GNU Boot, for fun - the GNU Boot project started in June
2023, in opposition to Libreboot's change of policy. Canoeboot started, with
the desire to provide FSDG-compliant releases based on Libreboot, exactly in
line with the GNU Free System Distribution Guidelines, as a proof of concept
to show what *Libreboot* would otherwise be.

Libreboot is on the forefront, bringing coreboot to ordinary people, and Canoeboot
follows the same pattern, providing high quality releases in parallel to it.

Canoeboot exists purely *for fun*, to see what is technically possible under GNU
policy. It is a fun technical challenge. Every effort is made to ensure perfect
compliance with GNU FSDG criteria.

Libreboot's Binary Blob Reduction Policy is more ideal, because it allows
more people to use coreboot, thus increasing software freedom overall for more
people, but there is no harm in Canoeboot so long as it does not stifle Libreboot
development; so Canoeboot is only updated after each Libreboot release,
cherry-picking what can be included under Canoeboot policy and done in an
extremely efficient manner.

Release schedule
--------------

The Canoeboot schedule is: whenever a Libreboot release is ready, produce a
new Canoeboot release based on it, when there are enough changes to warrant a
new release.

How releases are engineered
-----------------

It's actually very simple. Here is the method by which Canoeboot releases are
created:

* Take an archive of Libreboot's git repositories (build system, website and
  images), at the version that the current Canoeboot release is based on.
* Take the *new* Libreboot release, that is the target for Canoeboot-ising.
* Diff the two revisions in bulk (as-is), by re-initialising Git history in
  the old Libreboot revision; then copy `.git/` to the directory containing the
  new revision.
* In the directory containing the new revision, commit all of the changes.
* In the directory containing the new revision, run this
  command: `git format-patch -n1`
* The resulting `.patch` file will then show all of the changes.

The resulting patch file is then opened up in *Vim*.

In the Canoeboot repository, these changes are then copied over. This is done
by scrolling through the patch file, deciding which changes are worthy. The
policy is to include *all* changes, except those that are not suitable under
FSDG.

*Then* the following is done, for coreboot and u-boot trees *per tree*:

* Take the old revision of a given tree (e.g. `coreboot/default`), and diff it
  with the entire source tree on the same tree (e.g. `coreboot/default`) but
  on the new revision; then from the diff, get the list of all files that have
  been *added* and all of the files that have been *modified* (ignore files that
  have been deleted; also keep track of files that have renamed). This can be
  done very automatically with Git.
* Based on that, a list of files for scanning is now available. Next,
  the `deblob-check` script is executed within that source tree, using that
  list as input. For example: `./deblob-check $(cat /path/to/list) > sus.list`
* The resulting `sus.list` file contains all results, and this new list of files
  can then be checked. This is checked *manually*, but usually doesn't take very
  long (it's never more than a couple hundred files, and it's easy to see within
  like 5 seconds whether it's a blob: 500 seconds if it's 100 files).
* Any false positives are ignored, while actual blobs are then added to
  the correct file, e.g. `config/coreboot/default/blobs.list`.
* Next, documentation is scanned. The same process is used (track new files,
  moved files and changed files), but there is no automation for this. Every
  changed/moved/added file must be checked manually. This is to check for any
  documentation that recommends or endorses any proprietary code. Whole files
  can be deleted in this way; a normal diff can be provided to clean up other
  files, under e.g. `config/coreboot/default/patches/` if necessary.
* There may also be cases where a given bit of code is *not a blob*, but still
  proprietary, e.g. source code provided with restrictions on usage,
  modification or distribution.

Libreboot often contains hundreds of changes per release, so it would be quite
inefficient to cherry-pick individual patches. Therefore, all development is
done in Libreboot exclusively, and Canoeboot is the by-product of that
development, updating every now and then.

The above steps are a general guide, but specific tweaks may also be required
in the build system, for a new release; minor edge cases here and there, that
are different for every release, but they are usually very minor.

The `deblob-check` script is from linux-libre, a GNU fork of Linux that is
de-blobbed, but the same script works on any source tree, except it will flag
all of the false positives on non-Linux source trees; it scans heuristically
for binary blobs.

This is how Canoeboot can provide releases so quickly, based upon each release
of Libreboot. Extensive testing is performed on ROM images compiled under the
Libreboot build system, so the Canoeboot images are also easy to verify, since
a Canoeboot release will always be closely based on a Libreboot release.

This is actually the benefit of Canoeboot, over all other FSDG-derived coreboot
distros, because the other projects do not have as good infrastructure or the
level of resources *or* technical knowledge that Libreboot has. Libreboot
provides high quality releases which are then filtered by order of the protocol
described above, to provide Canoeboot releases.

osboot project
--------------

Prior to 16 November 2022, another project existed called the *osboot project*,
which was also created by Leah. Osboot started as a fork of Libreboot, starting
in early December 2020, with the exact policies and goals of Libreboot as of 16
November 2022. On 16 November 2022, osboot was *shut down* and all code
differences merged back into *Libreboot*, thus creating the osboot/libreboot
merger of 16 November 2022. The osboot project was thus the blueprint for
modern Libreboot.

Prior to the merger, during those almost-2-years, osboot and Libreboot were kept
in sync, but it was much harder; Libreboot was the main project, but under the
old policy, whereas osboot was *adding* new boards and new logic to the build
system. Then later, osboot became main; however, Libreboot was still the
bigger project. At the time *osboot* started, Libreboot had not seen any
releases in five years; osboot forked the old Libreboot build system from 2016
because of a failed re-write of it since 2017; osboot was used in early 2021
to *revive* the Libreboot project, scrapping the re-write and modernising the
classic design that later became [lbmk](https://libreboot.org/docs/maintain/)
(the Canoeboot version is [cbmk](docs/maintain/) and the osboot version was
called *osbmk*).

Removing code is easier than adding it each time, when syncing one project
based on another, but doing it this way was impossible then, so osboot and
Libreboot were maintained in parallel on a *per-patch* basis; the same logic
would be implemented per-patch between the two projects, but this started
becoming much harder to manage over time.

So if osboot was to become the de facto main project, I decided that it should
be osboot that has the most recognition. Therefore, I merged osboot into
Libreboot. I *originally* intended to then maintain an `fsdg` branch within
Libreboot, which would have been equivalent to today's Canoeboot in terms of
policy. However, this too would have become impractical.

So the situation before was: FSDG-only Libreboot, and no osboot. Then it was
Osboot and Libreboot. Then libreboot-only again, but under the Binary Blob
Reduction Policy. Now, today, it is libreboot under the Binary Blob Reduction
Policy and Canoeboot under Libreboot's old Binary Blob *Extermination* Policy
adhering to GNU FSDG. You can read the actual old Libreboot policy (the FSDG
one) in this link:
<https://web.archive.org/web/20221107235850/https://libreboot.org/news/policy.html>

Conclusion
==========

tl;dr - it's basically [this](https://xkcd.com/419/).
