---
title: Kontakt der Canoeboot projekt
x-toc-enable: true
...

User support
------------

IRC oder Reddit werden bevorzugt, sofern Du eine Support Anfrage hast (IRC empfohlen).
Für Informationen bzgl. IRC and Reddit siehe unten.

Entwicklungs Diskussion
---------------------

Eine Mailing Liste ist für die Zukunft in Planung. Bis dahin, siehe unter
[der Git Seite](git.md) für Informationen wie Du dich an der Entwicklung beteiligen kannst.

Hier finden sich ebenso Anleitungen zum Senden von Patches (via Pull-Requests).

IRC Chatraum
-------------

IRC ist hauptsächlich der Weg um Kontakt Canoeboot Projekt aufzunehmen. `#canoeboot` auf Libera
IRC.

Webchat:
<https://web.libera.chat/#canoeboot>

Libera ist eines der grössten IRC Netzwerke, welches für Libre Software Projekte verwendet wird.
Mehr Infos gibt es hier: <https://libera.chat/>

Wenn Du dich mit deinem bevorzugten IRC Klienten verbinden möchtest (z.B. weechat or irssi),
anbei die Verbindungsdetails:

* Server: `irc.libera.chat`
* Channel: `#canoeboot`
* Port (TLS): `6697`
* Port (non-TLS): `6667`

Wir empfehlen, dass Du Port `6697` mit aktivierter TLS Verschlüsselung verwendest.  

Es wird empfohlen SASL für die Authentifizierung zu verwenden. Diese Seiten auf der Libera
Website erläutern wie dies funktioniert:

* WeeChat SASL Anleitung: <https://libera.chat/guides/weechat>
* Irssi SASL Anleitung: <https://libera.chat/guides/irssi>
* HexChat SASL Anleitung: <https://libera.chat/guides/hexchat>

Grundsätzlich solltest Du die Dokumentation der von Dir verwendeten IRC Software konsultieren.

Soziale Medien
-----------------

Canoeboot existiert offiziell an vielen Orten.

### Mastodon

Gründerin und Haupt-Entwicklerin, Leah Rowe, ist auf Mastodon:

* <https://mas.to/@libreleah>

Leah kann zudem unter dieser eMail kontaktiert werden:
[leah@libreboot.org](mailto:leah@libreboot.org)

### Reddit

Hauptsächlich verwendet als Support Kanal und für Veröffentlichung von Neuigkeiten: 
<https://www.reddit.com/r/canoeboot/>
