---
title: Завантаження
x-toc-enable: true
...

Нові випуски оголошуються в [основній секції новин](news/).

Якщо ви більше зацікавлені в розробці canoeboot, пройдіть на
[сторінку розробки canoeboot](../git.md), яка також включає посилання на
репозиторії Git. Сторінка на [/docs/maintain/](docs/maintain/) описує те, як
Canoeboot складається разом, і як підтримувати його. Якщо ви бажаєте зібрати
Canoeboot із джерельного кода, [прочитайте цю сторінку](docs/build/).

Код підпису GPG
---------------

**Останнім випуском є Canoeboot 20250107, в директорії `canoeboot`.**

### NEW KEY

Full key fingerprint: `8BB1 F7D2 8CF7 696D BF4F  7192 5C65 4067 D383 B1FF

Use this to verify the most recent releases.

### OLD KEY

Повний відбиток ключа: `98CC DDF8 E560 47F4 75C0  44BD D0C6 2464 FA8B 4856`

This is the same key used to sign [Libreboot releases](https://libreboot.org/download.html), which is also used to
sign Canoeboot releases since it's the same person maintaining both projects.

Завантажте ключ тут: [lbkey.asc](lbkey.asc)

Випуски Canoeboot підписані з використанням GPG.

	sha512sum -c sha512sum.txt
	gpg --verify sha512sum.txt.sig

Репозиторій Git
===============

Посилання на архіви регулярних випусків зазначені на цій сторінці.

Однак, для абсолютно найновішої версії Canoeboot,
існує репозиторії Git, з якого можна завантажити. Ідіть сюди:

[Як завантажити Canoeboot через Git](git.md)

Download Canoeboot from mirrors
===============================

Canoeboot releases are hosted on the same Rsync server as Libreboot, and
mirrors pick this up; look in the `canoeboot` directory on Libreboot mirrors.
For your convenience, these are linked below (on the mirror lists).

Дзеркала HTTPS {#https}
-------------

**Останнім випуском є Canoeboot 20250107, в директорії `canoeboot`.**

Дані дзеркала є рекомендованими, оскільки використовують TLS (https://) шифрування.

Ви можете завантажити Canoeboot через дані дзеркала:

* <https://www.mirrorservice.org/sites/libreboot.org/release/canoeboot/> (Кентський
університет, Великобританія)
* <https://mirrors.mit.edu/libreboot/canoeboot/> (Університет МТІ, США)
* <https://mirror.math.princeton.edu/pub/libreboot/canoeboot/> (Прінстонський
університет, США)
* <https://mirror.shapovalov.website/libreboot/canoeboot/> (shapovalov.website, Україна)
* <https://mirror.koddos.net/libreboot/canoeboot/> (koddos.net, Нідерланди)
* <https://mirror-hk.koddos.net/libreboot/canoeboot/> (koddos.net, Гонконг)
* <https://mirror.cyberbits.eu/libreboot/canoeboot/> (cyberbits.eu, Франція)

Дзеркала RSYNC {#rsync}
-------------

Наступні дзеркала rsync доступні публічно:

* <rsync://rsync.mirrorservice.org/libreboot.org/release/canoeboot/> (Кентський університет,
Великобританія)
* <rsync://mirror.math.princeton.edu/pub/libreboot/canoeboot/> (Прінстонський університет, США)
* <rsync://rsync.shapovalov.website/libreboot/canoeboot/> (shapovalov.website, Україна)
* <rsync://ftp.linux.ro/libreboot/canoeboot/> (linux.ro, Румунія)
* <rsync://mirror.koddos.net/libreboot/canoeboot/> (koddos.net, Нідерланди)
* <rsync://mirror-hk.koddos.net/libreboot/canoeboot/> (koddos.net, Гонконг)

Ви підтримуєте роботу дзеркала? Зв'яжіться з проектом canoeboot, і посилання буде
додано до цієї сторінки!

Ви можете зробити своє дзеркало rsync доступним через свій веб-сервер, а також налаштувати
ваше *власне* дзеркало бути доступним через rsync. Є багато онлайн-ресурсів,
які показують вам те, як налаштувати сервер rsync.

Як створити ваше власне дзеркало rsync:

Корисно для відзеркалювання повного набору архівів випусків Canoeboot. Ви можете розмістити
команду rsync в crontab та витягувать файли в директорію на
вашому веб-сервері.

Якщо ви збираєтесь відзеркалювати повний набір, рекомендовано, щоб вами було виділено
хоча би 25 ГБ. Rsync Canoeboot наразі приблизно 12 ГБ, таким чином виділення 25 ГБ
забезпечить вам багато місця на майбутнє. Мінімально, ви маєте переконатись, що
хоча би 15-20 ГБ простору доступно, для вашого дзеркала Canoeboot.

*Настійно рекомендується, щоб ви використовували дзеркало canoeboot.org*, якщо бажаєте
розміщувати офіційне дзеркало. В іншому випадку, якщо ви просто бажаєте створити своє власне
локальне дзеркало, вам варто використовувати одне з інших дзеркал, яке синхронізується з
canoeboot.org.

**NOTE: the rsync commands below will only download canoeboot. Remove
the `canoeboot/` part at the end of each path, if you also want to download
all of Libreboot; Libreboot and Canoeboot share the same Rsync server.**

Перед створенням дзеркала, зробіть директорію на вашому веб-сервері. Для 
прикладу:

	mkdir /var/www/html/libreboot/canoeboot/

Тепер ви можете виконувати rsync, для прикладу:

	rsync -avz --delete-after rsync://rsync.canoeboot.org/mirrormirror/ /var/www/html/libreboot/canoeboot/

Ви могли би розмістить це в щогодинний crontab. Для прикладу:

	crontab -e

Потім в crontab, додайте цей рядок і збережіться/вийдіть (щогодинний crontab):

	0 * * * * rsync -avz --delete-after rsync://rsync.canoeboot.org/mirrormirror/ /var/www/html/libreboot/canoeboot/

**Це надзвичайно важливо, щоб мати в кінці косу лінію (/) в кінці кожного шляху,
в вищезазначеній команді rsync. В інакшому випадку, rsync буде поводитись дуже дивно.**

**ПОМІТКА: `rsync.canoeboot.org` не є напряму доступним для громадськості, окрім
тих, чиї IP у білому списку. Через пропускну здатність, Брандмауер, який працює
на canoeboot.org, блокує вхідні запити rsync, окрім окремих IP.**

**Якщо ви бажаєте запустити дзеркало rsync, синхронізуйте з одного з дзеркал третіх сторін
вище і встановіть своє дзеркало. Ви можете потім зв'язатись з Лією Роу, щоб мати ваші адреси
IP внесеним в білий список для використання rsync - якщо адреси IP відповідають DNS A/AAAA
записам для вашого хоста rsync, це може бути використано. Сценарій виконується в щогодинному
crontab на canoeboot.org, який отримує A/AAAA записи внесених в білий список дзеркал
rsync, автоматично додаючи правила, які дозволяють їм проходити через
брандмауер.**

Якщо ви бажаєте регулярно тримати свої дзеркала rsync оновленими, ви можете додати це до
crontab. Ця сторінка розповідає вам, як використовувати crontab:
<https://man7.org/linux/man-pages/man5/crontab.5.html>

Дзеркала HTTP {#http}
------------

**Останнім випуском є Canoeboot 20250107, під директорією `canoeboot`.**

УВАГА: ці дзеркала є не-HTTPS, що означає, що вони
незашифровані. Ваш трафік може бути об'єктом втручання
противників. Особливо ретельно переконайтесь, щоб перевірити підписи GPG, передбачаючи, що
ви маєте правильний ключ. Звісно, вам варто зробити це в будь-якому випадку, навіть
при використанні HTTPS.

* <http://mirror.linux.ro/libreboot/canoeboot/> (linux.ro, Румунія)
* <http://mirror.helium.in-berlin.de/libreboot/canoeboot/> (in-berlin.de, Німеччина)

Дзеркала FTP {#ftp}
-----------

**Останнім випуском є Canoeboot 20250107, під директорією `canoeboot`.**

УВАГА: FTP є також незашифрованим, подібно HTTP. Ті ж самі ризики присутні.

* <ftp://ftp.mirrorservice.org/sites/libreboot.org/release/canoeboot/> (Кентський
університет, Великобританія)
* <ftp://ftp.linux.ro/libreboot/canoeboot/> (linux.ro, Румунія)
