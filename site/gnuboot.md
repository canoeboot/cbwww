---
title: Canoeboot vs GNU Boot
x-toc-enable: true
...

If you want to understand the issue Canoeboot had with GNU Boot, please
read the [about page](about.md), the [Libreboot Binary Blob Reduction
Policy](https://libreboot.org/news/policy.html) and the [GNU Boot article
on libreboot.org](https://libreboot.org/news/gnuboot.html). Basically, the FSF
decided to attempt a *hostile fork* of Libreboot, which they announced on 19
March 2023 at their 2023 *LibrePlanet* conference. You can read about *that*
and more of the history between GNU/FSF and Libreboot, by also reading
the [10-year anniversary history page on
libreboot.org](https://libreboot.org/news/10.html) - Canoeboot started in self
defense, to provide a project that is as technically superior to GNU Boot as
possible, while adhering to GNU Free System Distribution Guidelines as policy,
which is the same policy that GNU Boot uses, in contrast to
Libreboot's [Binary Blob Reduction Policy](https://libreboot.org/news/policy.html).

Canoeboot is *aggressively* developed, rebasing upon each new release of
Libreboot and, thus, maintaining absolute sync with Libreboot. It does this by
providing all of the same boards and features, minus those boards/features or
behaviours that are not in line with GNU policy. As a result, Canoeboot has
weaker hardware support, but it provides a solution to those hardcore diehards
who absolutely must have everything adhere to GNU.

Canoeboot is technically superior
----------------------------------

tl;dr as of 26 October 2023, Canoeboot code is about 1 year ahead of GNU Boot
in terms of development, and about 2 years ahead in terms of documentation.
Read on, if you want details. GNU Boot is also known as *gnuboot*. The purpose
of *Canoeboot* is to demonstrate the inferiority of GNU policy, by providing the
highest quality releases possible, based on Libreboot, keeping sync with
Libreboot while showing what boards/features would have to be removed from
Libreboot, if it were to comply with GNU Boot. In other words, Canoeboot is the
logical conclusion of what is *possible* under GNU policy.

Libreboot's [Binary Blob Reduction
Policy](https://libreboot.org/news/policy.html) provides an alternative, where
users have boot firmware that is as much free software as possible, while not
being dogmatic about it. You can learn more by reading that policy.

Canoeboot and GNU Boot are *both* forks of Libreboot, designed to comply with
the GNU Free System Distribution Guidelines. This page is maintained, to show
the differences between these two projects.

This current version of the page pertains to *Canoeboot 20231026*
versus *GNU Boot 0.1 RC*. You can find GNU Boot ("gnuboot") on the GNU Savannah
website.

You can also read the [Canoeboot 20231101 changelog](news/canoeboot20231101.md);
that release only came out 7 days after the 20231026 release, so the rest of
this page is more or less accurate, when combined with the facts on
the 20231101 announcement.

GNU Boot 0.1 RC based on Libreboot 20220710
===========================================

This fact is very important; nonGeNUine Boot's 20230717 changelog is relative
to Libreboot 20220710, and Canoeboot 20231026's changelog is relative to
nonGeNUine Boot 20230717's changelog.

Therefore, this page will analyse differences in both projects, at these two
points. First, let's analyse GNU Boot, with the tag reset to `0.1-rc1`, which
corresponds to commit ID `a64d284fd798d843133c9d7274bba17bd7837174`. Since GNU
Boot also contains the Libreboot history that it forked from, it contains the
Libreboot 20220710 release tag, so we can do this:

	git log --graph --pretty=format:'%Cred%h%Creset %s %Creset' --abbrev-commit 20220710..0.1-rc1

Within the GNU Boot git repository, this would yield the following response:

```
* a64d284 .gitignore: order alphabetically 
* 0df4fe5 GRUB: config from HDD/SSD: Add support for gnuboot_grub.cfg 
* ce13d22 GRUB: Use GNU Boot logo 
* 74b678c GRUB: Say the name GNUBoot in the grub menu 
* eeddd2b build/dependencies: debian: adding python-is-python3 to build seabios properly 
* 58b8e09 coreboot/fam15h: don't build ada toolchain for generic platforms 
* f7c0fec coreboot/fam15h: update code base, deblob, unset CONFIG_STM (see bug #64535) 
* de9297f coreboot/fam15h: fix crossgcc acpica build on newer hostcc 
* c38348d coreboot/fam15h: fix for gcc/gnat building 
* 0d77d99 coreboot/fam15h: fixing binutils not building properly 
* b773079 coreboot/default, coreboot/fam15h: use GNU mirror for acpica 
* bf17993 Continue Libreboot under the GNU project
```

And that's all. The fam15h-related fixed are actually merged from
the `fsdg20230625` branch of Libreboot, made during July 2023. See:
<https://browse.libreboot.org/lbmk.git/log/?h=fsdg20230625>

The other patches are also merged (cherry-picked) from Libreboot! The above
commit log is all that GNU Boot did, for their 0.1 RC1 release.

Therefore, to know the differences between Canoeboot and GNU Boot, I will copy
all items first from the nonGeNUine Boot 20230717 change log, and then the
Canoeboot 20231026 change log, but I will skip those entries that define
features which GNU Boot already has.

On this day, GNU Boot's current commit ID (in the `main` branch)
is `54c6ae497d49c233b654c171978baa77b90ffe17` from 12 October 2023. Most of
the changes since 0.1 RC1 up to that commit are just documentation changes,
and even still, only cherry-picking minor patches here and there that were
already done in Libreboot, in some cases years ago. It's worth noting that
the GNU Boot documentation is based on Libreboot documentation from *late 2021*
or at most, very early 2022.

I don't need to compare documentation, and it would take too long. Their
documentation is *2 years out of date*, what more is there to say?

Now, feature comparisons in the build systems:

Canoeboot 20231026 features that GNU Boot lacks
===============================================

Board support
-------------

Canoeboot has *these* boards fully supported, that GNU Boot currently lacks
support for:

* [Dell Latitude E6400](docs/hardware/e6400.md)
* [ASUS Chromebook Flip C101 (gru-bob)](docs/install/chromebooks.md)
* [Samsung Chromebook Plus (v1) (gru-kevin)](docs/install/chromebooks.md)

Git revisions in Canoeboot 20231026:
------------------------------------

* Coreboot (default): commit ID `d862695f5f432b5c78dada5f16c293a4c3f9fce6`, 12 October 2023
* Coreboot (fam15h\_udimm): commit ID `1c13f8d85c7306213cd525308ee8973e5663a3f8`, 16 June 2021
* GRUB: commit ID `e58b870ff926415e23fc386af41ff81b2f588763`, 3 October 2023
* SeaBIOS: commit ID `1e1da7a963007d03a4e0e9a9e0ff17990bb1608d`, 24 August 2023
* U-Boot: commit ID `4459ed60cb1e0562bc5b40405e2b4b9bbf766d57`, 2 October 2023

Git revisions in GNU Boot 0.1 RC1:
---------------------------------

* Coreboot (default): commit ID `b2e8bd83647f664260120fdfc7d07cba694dd89e`, 17 November 2021
* Coreboot (fam15h\_udimm): commit ID `1c13f8d85c7306213cd525308ee8973e5663a3f8`, 16 June 2021
* GRUB: commit ID `f7564844f82b57078d601befadc438b5bc1fa01b`, 25 October 2021
* SeaBIOS: commit ID `1281e340ad1d90c0cc8e8d902bb34f1871eb48cf`, 24 September 2021
* U-Boot: **does not exist** (no ARM/U-Boot support in Libreboot 20220710)
  NOTE: GNU Boot *does* support downloading, deblobbing and re-compressing
  U-Boot archives, and in fact does a better job of *that* than Canoeboot in
  some ways, but it does not yet actually build U-Boot, and it does not boot
  U-Boot in any way, on any actual mainboards.

As you can see, Canoeboot's revisions are a lot newer.

GRUB LUKS2 support with argon2
------------------------------

Canoeboot 20231026 contains a heavily patched version of GRUB, which contains
argon2 support. This allows full decryption of LUKS2 volumes, without having
to switch to different key derivation (PBKDF2) and without needing to use
LUKS1. You can simply use the default LUKS2 setup provided by any distro, and
it will work. More information is available about GRUB cryptomount, in
the [GNU+Linux guide](docs/gnulinux/) - search on that page for LUKS2 or argon2.

GNU Boot completely *lacks* this feature, as of 26 October 2023. It can only
support LUKS2 if the key derivation is downgraded to PBKDF2 (insecure, or to
LUKS1 (also insecure).

For all intentions, the average user cannot have a fully encrypted system on
GNU Boot. They must leave `/boot` unencrypted on GNU+Linux distros.

With Canoeboot, you can have encrypted `/boot` very easily. This is a boon for
security, because it reduces the chance of someone tampering with your data
successfully, and combined with [other steps](docs/gnulinux/grub_hardening.md),
can be used to reduce the risk of evil maid attacks (by making it infeasible).

GRUB detached LUKS headers
--------------------------

GRUB 2.12 also supports detached LUKS headers, whereas GRUB 2.06 does not.
GNU Boot *currently* uses GRUB 2.06 as its payload. Canoeboot and Libreboot
both use GRUB 2.12 as a payload.

Serprog support
---------------

Canoeboot can build firmware images for RP2040 and STM32 microcontrollers,
using `pico-serprog` and `stm32-vserprog` respectively. This can be used to
set up an SPI flasher of high quality, but these parts are low-cost.

GNU Boot does not support this feature, as of 26 October 2023.

Simplified command structure
----------------------------

There are *9* shell scripts in Canoeboot 20231026, versus about 50 in GNU
Boot 0.1 RC1, because GNU Boot uses the pre-audit design; Libreboot used to
have lots of very simple scripts, but ended up with a lot of code repetition.

The new lbmk design generalises all of the logic, doing away with the very
hacky logic that existed in the old build system design.

The interface in Canoeboot's build system is much easier to use. For example,
the commands are shorter, and easier to remember. See:
[cbmk maintenance manual](docs/maintain/) tells you everything about the
Canoeboot build system.

GNU Boot doesn't even *have* a maintenance manual, in their version. Their
documentation exists in the same repository as code, but their
version of `docs/maintain/` does not actually contain any instructions,
at least as of commit ID a64d284fd798d843133c9d7274bba17bd7837174 on 17
August 2023.

Better documentation
--------------------

Canoeboot has much better documentation, but this is obvious if you've been
paying attention. As already stated: GNU Boot's documentation is horribly
out of date, even relative to the version of Libreboot that they're using!
(which itself is also horribly out of date)

Canoeboot's build system is smaller
-----------------------------------

Smaller doesn't mean worse; in fact, Canoeboot's build system is more
efficient. It's about 1250 sloc (source lines of code), when counting shell
scripts in the core of the build system. Libreboot, Canoeboot and GNU Boot
build systems all use the same design, written in shell scripts.

1250 sloc in Canoeboot, versus 2111 in gnuboot; gnuboot however lacks many
of the features and improvements that you're about to see below. The
Canoeboot build system does several times as many things, in half
the amount of code! The code is generally just more reliable, less error
prone, and easier to work with, in Canoeboot. GNU Boot uses a very old version
of the Libreboot build system design, from long before I started any massive
audits. There have been *three* Libreboot build system audits in 2023, as
of 26 October 2023.

Three audits, and Canoeboot has inherited the improvements of all of them.
GNU Boot's design is based on the pre-audit lbmk codebase.

Build system / performance improvements in Canoeboot:

* Much stricter, more robust error handling; too many changes to list here, so
  check the git log. Also, errors that *are not errors* are no longer treated as
  such; nonGeNUine Boot 20230717's build system was actually too strict, sometimes.
* Most logic has been unified in single scripts that perform once type of task
  each, instead of multiple scripts performing the same type of talk; for
  example, defconfig-based projects now handled with the same scripts, and
  preparing trees for them is done the same. These unifications have been done
  carefully and incrementally, with great thought so as to prevent *spaghetti*.
  The code is clean, and small.
* GitHub is no longer used on main Git repository links, instead only as backup
* Backup repositories now defined, for all main repos under `config/git/`
* Single-tree projects are no longer needlessly re-downloaded when they already
  have been downloaded.
* GRUB LUKS2 support now available, with argon2 key derivation; previously, only
  PBKDF2 worked so most LUKS2 setups were unbootable in Canoeboot. This is fixed.
* Vastly reduced number of modules in GRUB, keeping only what is required.
* Use `--mtime` and option options in GNU Tar (if it is actually GNU Tar), when
  creating Tar archives. This results in partially reproducible source archives,
  and consistent hashes were seen in testing, but not between distros.
* Always re-inialitise `.git` within lbmk, for the build system itself, if
  Git history was removed as in releases. This work around some build systems
  like coreboot that use Git extensively, and are error-prone without it.
* More robust makefile handling in source trees; if one doesn't exist, error
  out but also check other makefile name combinations, and only error out if
  the command was to actually build.
* ROMs build script: support the "all" argument, even when getopt options are
  used e.g. `-k`
* Disabled the pager in `grub.cfg`, because it causes trouble in some
  non-interactive setups where the user sees an errant message on the screen
  and has to press enter. This fixes boot interruptions in some cases, allowing
  normal use of the machine. The pager was initially enabled many years ago,
  to make use of cat a bit easier in the GRUB shell, but the user can just
  enable the pager themselves if they really want to.
* U-Boot can now be compiled standalone, without using the ROMs build script,
  because crossgcc handling is provided for U-Boot now in addition to coreboot.
* All helper scripts are now under `include/`, and main scripts in `script/`,
  called by the main `build` script
* Generally purge unused variables in shell scripts
* Simplified initialisation of variables in shell scripts, using the `setvars`
  function defined under `include/err.sh`
* Support patch subdirectories, when applying patches. This is done recursively,
  making it possible to split up patch files into smaller sets inside sub
  directories, per each source tree (or target of each source tree, where a
  project is multi-tree within lbmk)
* SPDX license headers now used, almost universally, in all parts of cbmk.
* Files such as those under `config/git` are now
  concatenated, traversing recursively through the target directory; files first,
  then directories in order, and for each directory, follow the same pattern
  until all files are concatenated. This same logic is also used for patches.
  This now enables use of subdirectories, in some config/patch directories.
* General code cleanup on `util/nvmutil`
* Git histories are more thoroughly deleted, in third party source trees during
  release time.
* Symlinks in release archives are no longer hard copies; the symlinks are
  re-created by the release script, because it clones the current lbmk work
  directory via Git (local git clone), rather than just using `cp` to copy links.
* Properly output to stderr, on printf commands in scripts where it is either
  a warning prior to calling `err`, or just something that belongs on the error
  output (instead of standard output).
* Don't use the `-B` option in make commands.
* SECURITY: Use sha512sum (not sha1sum) when verifying certain downloads. This
  reduces the chance for collisions, during checksum verification.
* Set GRUB timout to 5s by default, but allow override and set to 10s or 15s
  on some mainboards.
* Support both curl and wget, where files are downloaded outside of Git; defer
  to Wget when Curl fails, and try each program three times before failing. This
  results in more resilient downloading, on wobbly internet connections.
* Don't clone Git repositories into `/tmp`, because it might be a tmpfs with
  little memory available; clone into `tmp/gitclone` instead, within lbmk,
  and `mv` it to avoid unnecessary additional writes (`mv` is much more efficient
  than `cp`, for this purpose).
* Removed unused `target.cfg` handling in vendor scripts, because they use
  the concatenated config format instead (they always have).
* Coreboot builds: automatically run make-oldconfig, to mitigate use of raw
  coreboot config where a revision was updated but the config was untouched.
  This may still result in a confirmation dialog, and it's still recommended
  that the configs be updated per revision (or switch them to defconfigs).
* Vastly simplified directory structure; `resources/scripts/` is now `script/`,
  and `resources/` was renamed to `config/`; ifd and gbe files were also moved
  to `config/ifd/`. Commands are now 1-argument instead of 2, for example
  the `./build boot roms` command is now `./build roms`.
* memtest86plus: only build it on 64-bit hosts, for now (32-bit building is
  broken on a lot of distros nowadays, and lbmk doesn't properly handle cross
  compilation except on coreboot or U-Boot)
* (courtesy of Riku Viitanen) don't use cat on loops that handle lines of text.
  Instead, use the `read` command that is built into `sh`, reading each line.
  This is more efficient, and provides more robust handling on lines with
  spaces in them.
* *ALL* projects now have submodules downloaded at build time, not just multi
  tree projects such as coreboot - and a few projects under `config/git` have
  had certain `depend` items removed, if a given project already defines it
  under `.gitmodules` (within its repository).
* Improved cbutils handling; it's now even less likely to needlessly re-build
  if it was already built.
* The release build script no longer archives what was already built, but
  instead builds from scratch, creating an archive from source downloads
  first before building the ROM archives. This saves time because it enables
  a single build test per release, whereas at was previously necessary to test
  the Git repository and then the release archive. Testing both is still desired,
  but this behaviour also means that whatever is built at release time is
  guaranteed to be the same as what the user would build (from archives).
* Improved handling of `target.cfg` files in multi-tree projects coreboot,
  SeaBIOS and U-Boot. Unified to all such projects, under one script, and
  with improved error handling.
* GRUB payload: all ROM images now contain the same ELF, with all keymaps
  inserted. This speeds up the build process, and enables easier configuration
  when changing the keyboard layout because less re-flashing is needed.
* Simplified IFD handling on ICH9M platforms (e.g. X200/T400 thinkpads); the
  ich9gen utility wasn't needed anymore so ich9utils has been removed, and now
  the IFD/GbE files are included pre-assembled (generated by ich9gen). Ich9gen
  can still be used, or you can re-generate with coreboot's bincfg; the ifdtool
  util can be used to edit IFD and nvmutil (part of Canoeboot) can change MAC
  addresses. The ich9utils code was always redundant for the last few years,
  especially since 2022 when nvmutil was first written.
* Running as root is now forbidden, for most commands; lbmk will exit with
  non-zero status if you try. The `./build dependencies x` commands still work
  as root (they're the only commands available as root).
* Enabled memtest86plus on more boards, where it wasn't previously enabled.
* Only enable SeaBIOS as first payload on desktops, but still enable GRUB as
  second payload where GRUB is known to work (on each given host). The text
  mode and coreboot framebuffer modes are provided in each case, where feasible.
* The `list` command has been mostly unified, making it easier to tell (from
  lbmk) what commands are available, without having to manually poke around
  under `script/`.
* The `-T0` flag is now used, universally, on xz commands. This makes `xz` run
  on multiple threads, greatly speeding up the creation of large tar archives.
* Universally use `-j` in make commands, for multi-threading, but it relies
  on `nproc` to get thread count, so this only works if you have `nproc` (you
  probably don't, if you run BSD; BSD porting is still on TODO for Canoeboot)
* File names as arguments now universally have quotes wrapped around them, and
  similar auditing has been done to all variables used as arguments everywhere
  in lbmk. There were cases where multiple arguments were wrongly quoted then
  treated as a single argument, and vice versa. This is now fixed.
* Re-wrote `.gitcheck`; now, a global git name/email config is always required.
  The only behaviour (setting local config, and unsetting) was quite error-prone
  under fault conditions, where cleanup may not have been provided, or when
  execution was interrupted, resulting sometimes in accidentally committing
  to `lbmk.git` as author named `lbmkplaceholder`.
* The new BSD-like coding style is now used on *all* shell scripts in lbmk. A
  few scripts still used the old lbmk coding style, as of audit 2.
* Scripts no longer directly exit with non-zero status, under fault conditions;
  instead, `x_` or `err` is used to provide such behaviour. This results in all
  exits from lbmk being consolidated to `err`, under fault conditions. - zero
  exits are also consolidated, going only through the main script, which has its
  own exit function called `lbmk_exit` that provides `TMPDIR` cleanup.
* BSD-style error handling implemented, with an `err` function (and functions
  that use it) inside `include/err.sh`; there is also `x_` which can be used
  to run a command and exit automatically with non-zero status, useful because
  it provides more verbose output than if you just relied on `set -e`, and it
  still works when a script *does not* use `set -e` - however, it is not used
  on all functions, because it works by executing `$@` directly, which can break
  depending on arguments. Therefore, some scripts just default to `|| err` for
  providing breakage in scripts.
* Memtest *6.2* now used (instead of *5.x* releases). This is essentially a
  re-write, and it works on the coreboot framebuffer, whereas previous revisions
  only worked on text mode setups.
* NO MAKEFILE. The Makefile in lbmk has been removed. It was never meaningfully
  used because all it did was run lbmk commands, without implementing any logic
  itself. A Makefile may be added again in the future, but with a view to
  installing *just the build system* onto the host system, to then build ROM
  images under any number of directories. Lbmk's design is strictly no-Makefile,
  but it uses Makefiles provided by third party source trees when building them.
* Safer GRUB configuration file handling between GRUB memdisk and coreboot CBFS;
  it is no longer possible to boot without a GRUB config, because the one in
  GRUB memdisk is provided as a failsafe, overridden by *inserting* one in CBFS,
  but there is no config in CBFS by default anymore.
* The build system *warns* users about `elf/` vs `bin/`, when it comes to
  flashing coreboot ROM images; it tells them to use `bin/` because those
  images do contain payloads, whereas the ones under `elf/` do not.
* VASTLY more efficient build process; all coreboot ROMs without payload are
  now cached under `elf/`, as are payloads, then they are joined separately by
  the usual ROMs build script, and these cached ROMs contain many changes in
  them that were previously handled by `moverom` in the main ROM build script.
  Under the new design, repetitive steps are avoided; payloads are inserted into
  a copy of the cached ROMs under `TMPDIR`, *before* being copied for keymaps
  and small files; this eliminates delays caused by slow compression (LZMA is
  always used, when inserting payloads). After crossgcc and the payloads are
  compiled, the ROM with coreboot builds in under a minute, whereas it would
  have previously taken several minutes on most Canoeboot-supported hardware.
* VASTLY reduced GRUB payload size; modules that aren't needed have been removed
  resulting in much smaller GRUB payloads, that also boot faster.
* ALL defconfig creation, updating and modification are handled by the same
  script that *also* handles compiling, as mentioned in the bullet-point below.
* ALL main source trees are now compiled, downloaded, configured and cleaned
  using the same script. The *download* (Git) logic is a separate file
  under `include/` and its functions are called by the main build script, which
  provides a stub for this.
* Scripts are no longer executed directly, ever, except the main script. All
  scripts are otherwise executed from `script/`, inheriting the `TMPDIR`
  variable set (and exported) by lbmk.
* Generally improved user feedback in scripts, especially the vendor scripts.
* Coreboot, U-Boot and SeaBIOS are now downloaded, configured and compiled using
  the exact same script. Although these codebases differ wildly, their build
  systems use the same design, and they are compatible from a user-interface
  perspective.
* Vastly improved `/tmp` handling; a universal `TMPDIR` is set (environmental
  variable) and exported to all child processes running lbmk scripts. On exit,
  the main tmp directory is purged, cleaning all tmp directories under it.
* General simplification of coding style on all shell scripts.
* Fixed some variable initialisations in the coreboot ROM image build script
* Don't enable u-boot on QEMU x86 images (due to buggy builds, untested) 
* Fixed coreboot-version file inserted into coreboot trees, when compiled
  on Canoeboot release archives.
* Very general auditing has been done, finding and fixing bugs.
* Reduced the number of scripts significantly. There were about 50 scripts in
  the nonGeNUine Boot 20230717 build system. There are closer to *20* in today's
  Canoeboot 20231026 revision.
* Many scripts that were separate are now unified. For example: the scripts
  handling defconfigs files on SeaBIOS, u-Boot and coreboot have now been
  merged into a single script, performing the same work *better* in less code.
* Ditto many other scripts; repeated logic unified, logic generalised. The
  logic for *downloading* coreboot and u-boot was unified into one script,
  basing off of the coreboot one, and then expanding to also cover SeaBIOS.
  Most building (e.g. handling of Makefiles) is now done in a single script.
* Far superior error handling; in many scripts, the `-e` option in `sh` was
  heavily relied upon to catch errors, but now errors are handled much more
  verbosely. *Many* fault conditions previously did not make lbmk *exit* at all,
  let alone with non-zero status, and zero status was sometimes being returned
  under some edge cases that were tested. Error handling is more robust now.
* `util/ich9utils` (containing `ich9gen`) was *removed*, thus eliminating about
  3000 source lines (of C code) from lbmk. The `nvmutil` program, also provided
  by and originating from the Canoeboot project, can already change GbE MAC
  addresses. Coreboot's bincfg can generate ich9m descriptors, and ifdtool can
  manipulate them; so the features provided by ich9utils were superfluous, since
  they are available in other projects that we ship. We now ship pre-built
  ifd/gbe configs on these machines, which can be modified or re-assembled
  manually if you want to. This eliminates a moving part from Canoeboot, and
  speeds up the build a little bit.
* ROM images (of coreboot) build *much faster*: no-payload coreboot ROMs are
  cached on disk, as are payloads, where previously only the latter was cached.
  These cached images have as much inserted into them as possible, to eliminate
  redundant steps in the build process. The `elf` directory contains these, and
  the existing `bin` directory still holds the full ROM images (containing
  payloads) when compiled.
* GRUB payload: vastly reduced the size of the payload, by eliminating GRUB
  modules that were not needed. About 100KB of compressed space saved in flash!
* GRUB payload: [argon2 key derivation supported](argon2.md) - this means LUKS2
  decryption is now possible in GRUB. This work was performed by Nicholas
  Johnson, rebasing from Axel's AUR patch for GRUB 2.06 (Canoeboot currently
  uses GRUB 2.12).
* The *new* coding style is now used on many more scripts, including
  the `build/boot/roms_helper` script - the new style is much cleaner,
  mandating that logic be top-down, with a `main()` function defined; it's
  basically inspired by the OpenBSD coding style for C programs, adapted to
  shell scripts.
* All GRUB keymaps now included; a single `grub.elf` is now used on all ROM
  images. The `grub.cfg` goes in GRUB memdisk now, but can be overridden by
  inserting a `grub.cfg` in CBFS; many behaviours are also controlled this way,
  for example to change keymaps and other behaviours. This results in *much*
  faster builds, because a different GRUB payload doesn't have to be added to
  each new ROM image; such takes time, due to time-expensive LZMA compression.
  This, plus the optimised set of GRUB modules, also makes GRUB itself load
  much faster. All of the fat has been trimmed, though still quite a lot more
  than a Crumb.
* A lot of scripts have been removed entirely, and their logic not replaced;
  in many cases, Canoeboot's build system contained logic that had gone unused
  for many years.
* More reliable configs now used on desktop mainboards: SeaBIOS-only for start,
  but GRUB still available where feasible (in the SeaBIOS menu). This makes it
  more fool proof for a user who might use integrated graphics and then switch
  to a graphics card; the very same images will work.
* TMPDIR environmental variable now set, and exported from main parent process
  when running lbmk; child processes inherit it, and a single tmp dir is used.
  This is then automatically cleaned, upon exit from lbmk; previously, lbmk did
  not cleanly handle `/tmp` at all, but now it's pretty reliable.
* A [HUGE build system audit](https://libreboot.org/news/audit.html) inherited
  from Libreboot, is part of the Canoeboot tree; the entire build system was
  re-written in a much cleaner coding style, with much stricter error handling
  and clear separation of logic. A *lot* of bugs were fixed. A *LOT* of bugs.
  Build system auditing has been the *main* focus, in these past 12 months.
* `cros`: Disable coreboot-related BL31 features. This fixes poweroff on gru
  chromebooks. Patch courtesy of Alper Nebi Yasak.
* `u-boot`: Increase EFI variable buffer size. This fixes an error where
  Debian's signed shim allocates too many EFI variables to fit in the space
  provided, breaking the boot process in Debian. Patch courtesy Alper Nebi Yasak
* Coreboot build system: don't warn about no-payload configuration. nonGeNUine Boot
  compiles ROM images *without* using coreboot's payload support, instead it
  builds most payloads by itself and inserts them (via cbfstool) afterwards.
  This is more flexible, allowing greater configuration; even U-Boot is
  handled this way, though U-Boot at least still uses coreboot's crossgcc
  toolchain collection to compile it. Patch courtesy Nicholas Chin.
* `util/spkmodem-recv`: New utility, forked from GNU's implementation, then
  re-written to use OpenBSD style(9) programming style instead of the
  originally used GNU programming style, and it is uses
  OpenBSD `pledge()` when compiled on OpenBSD. Generally much cleaner coding
  style, with better error handling than the original GNU version (it is forked
  from coreboot, who forked it from GNU GRUB, with few changes made). This
  is a receiving client for spkmodem, which is a method coreboot provides to
  get a serial console via pulses on the PC speaker.
* download/coreboot: Run `extra.sh` directly from given coreboot tree. Unused
  by any boards, but could allow expanding upon patching capabilities in lbmk
  for specific mainboards, e.g. apply coreboot gerrit patches in a specific
  order that is not easy to otherwise guarantee in more generalised logic of
  the nonGeNUine Boot build system.
* `util/e6400-flash-unlock`: New utility, that disables flashing protections
  on Dell's own BIOS firmware, for Dell Latitude E6400. This enables nonGeNUine Boot
  installation *without* disassembling the machine (external flashing equipment
  is *not required*). Courtesy Nicholas Chin.
* Build dependencies scripts updated for more modern distros. As of this day's
  release, nonGeNUine Boot compiles perfectly in bleeding edge distros e.g. Arch
  Linux, whereas the previous 20220710 required using old distros e.g.
  Debian 10.
* `cbutils`: New concept, which implements: build coreboot utilities like
  cbfstool and include the binaries in a directory inside lbmk, to be re-used.
  Previously, they would be compiled in-place within the coreboot build system,
  often re-compiled needlessly, and the checks for whether a given util are
  needed were very ad-hoc: now these checks are much more robust.
  Very centralised approach, per coreboot tree, rather than selectively
  compiling specific coreboot utilities, and makes the build system logic in
  nonGeNUine Boot much cleaner.
* GRUB config: 30s timeout by default, which is friendlier on some desktops
  that have delayed keyboard input in GRUB.
* ICH9M/GM45 laptops: 256MB VRAM by default, instead of 352MB. This fixes
  certain performance issues, for some people, as 352MB can be very unstable.
* U-Boot patches: for `gru_bob` and `gru_kevin` chromebooks, U-Boot is used
  instead of Google's own *depthcharge* bootloader. It has been heavily
  modified to avoid certain initialisation that is replaced by coreboot, in
  such a way that U-Boot is mainly used as a bootloader providing UEFI for
  compliant GNU+Linux distros and BSDs. Courtesy Alper Nebi Yasak.
* lbmk: The entire nonGeNUine Boot build system has, for the most part, been made
  portable; a lot of scripts now work perfectly, on POSIX-only implementations
  of `sh` (though, many dependencies still use GNU extensions, such as GNU
  Make, so this portability is not directly useful yet, but a stepping stone.
  nonGeNUine Boot eventually wants to be buildable on non-GNU, non-GNU/Linux systems,
  e.g. BSD systems)
* nvmutil: Lots of improvements to code quality, features, error handling. This
  utility was originally its own project, started by Leah Rowe, and later
  imported into the nonGeNUine Boot build system.
* build/boot/roms: Support cross-compiling coreboot toolchains for ARM platforms,
  in addition to regular x86 that was already supported. This is used for
  compiling U-boot as a payload, on mainboards.
* U-boot integration: at first, it was just downloading U-Boot. Board integration
  for ARM platforms (from coreboot) came later, e.g. ASUS Chromebook Flip C101
  as mentioned above. The logic for this is forked largely from the handling
  of coreboot, because the interface for dealing with their build systems is
  largely similar, and they are largely similar projects. Courtesy Denis Carikli
  and Alper Nebi Yasak.
* New utility: `nvmutil` - can randomise the MAC address on Intel GbE NICs, for
  systems that use an Intel Flash Descriptor
* General build system fixes: better (and stricter) error handling
* Fixed race condition when building SeaBIOS in some setups. 
* GRUB configs: only scan ATA, AHCI or both, depending on config per board.
  This mitigates performance issues in GRUB on certain mainboards, when
  scanning for `grub.cfg` files on the HDD/SSD.
* GRUB configs: speed optimisations by avoiding slow device enumeration in
  GRUB.

Summary
=======

So, in conclusion: this page is not trying to tell you why you should use
Canoeboot; rather, it's just telling you that someone worse exists. Canoeboot
and GNU Boot are *both* inherently flawed in their designs; *both* projects are
completely *inferior* to the Libreboot project, for all the reasons laid out in
the [Binary Blob Reduction Policy](https://libreboot.org/news/policy.html) - the
Canoeboot project is provided, specifically, to prove the merits of that policy,
by showing what Libreboot would have been by now if it continued adhering to
GNU policy, instead of changing to its current Blob Reduction Policy.

Libreboot provides all of the same blob-free configurations as Canoeboot, when
possible on any given mainboard, and that is the preference, but the FSF/GNU
dogma states that the user must never run any proprietary software. This dogma
is wrong; a more correct approach is to say that proprietary software is bad,
because it restricts the user's freedom to study and modify the software; it
removes their power to say no when the developer wants to change the program in
a bad way, it leaves them at the mercy of that developer - the point is this:

Free software is good, and we should be promoting as much of it as possible.
This means that a hardline no-blob approach like the policy implemented by
Canoeboot (or GNU Boot for that matter), is entirely misguided, because it
will only alienate those who just want *some* free software. Most people like
the idea of software freedom but cannot go all the way yet; we should encourage
people to make the right choices, but otherwise just help them in whatever way
we can, meeting people where they're at right now.

And that is why Libreboot exists in the way that it does. Canoeboot serves as an
example of what would happen in the best-case scenario if Libreboot never changed
its policy. The best possible release of Canoeboot will still be missing a ton
of boards and features from Libreboot. Indeed, the Canoeboot 20231026 and
nonGeNUine Boot 20230717 both illustrate that quite nicely.

GNU Boot 0.1 RC3 and Canoeboot 20240102
---------------------------------------

Dubbed *Canoeboot 20240102*, though it's not actually a release. For context,
first read: <https://libreboot.org/news/audit4.html>

Canoeboot imported these changes, in
revision `102ce12cea023783729d6a2cd1576afc95bf7540` on 2 January 2024. This
revision is referred to unofficially as *Canoeboot 20240102*.

These changes add the following noteworthy improvements, that benefit Canoeboot
users:

* GRUB 2.12 stable release, instead of GRUB 2.12-rc1. The stable release came
  out on 20 December 2023.
* Generic cmake and autoconf handling now, in the build scripts. This means that
  many more projects can be added more easily to the build system (utilities
  and such)
* Higher default drive level of 12mA, instead of 4mA, when building serprog
  images for RP2040-based MCUs
* Generic fixes in the build system, better error checking; failed Git patching
  now actually exits, whereas in previous releases (and also in GNU Boot's
  build system, because it inherits Libreboot which had this bug for years),
  in many older releases, `git am` failing does not cause the build system to
  exit. The build continues, erroneously. Libreboot fixed this in audit 4, and
  Canoeboot has imported this.
* Crossgcc tarballs no longer bundled in releases, making them smaller. The
  build system still downloads these at build time, but this means that now
  you will only download what you need, when you build Canoeboot for a board.
* Single-tree projects are now configurable with `target.cfg` files. This could
  enable use of crossgcc, if you set `xarch`, and make/cmake/autoconf arguments
  are now all configurable in `target.cfg` - previously, this file was only
  possible for multi-tree projects.
* `grub.cfg`: the default config scanning now also searches for syslinux configs,
  in addition to extlinux, though it still looks for GRUB configs first. GRUB
  can parse syslinux/extlinux configuration files. Canoeboot's GRUB is also now
  configured to search for configs on the *ESP* (EFI System Partition), both
  on USB media and on the installed system. With these changes, Canoeboot now
  boots distros much more reliably, especially UEFI-based ones, even though
  UEFI is not implemented in Caneoboot on x86 yet.

These and other/subsequent changes will be merged with the lists above, when
the next Canoeboot release comes out. The main lists of changes above are
for the current binary release of Canoeboot, versus GNU Boot.

GNU Boot 0.1 RC3
----------------

The article above compares Canoeboot 20231107 and GNU Boot 0.1 RC1. It should
be noted that 0.1 RC3 is now available, but it doesn't really add any major
changes, and most of the changes are documentation changes. Here is a brief list
of changes that would be beneficial to users:

* Removed a few binary blobs that GNU Boot overlooked, which were included in
  the GNU Boot (these were also overlooked in Canoeboot releases, and have
  been handled in Canoeboot, thanks to reports sent by Denis Carikli of GNUBoot)
* The `CONFIG_USE_BLOBS` setting was disabled on all coreboot configs - it
  is an extra setting that reduces the chance of the coreboot build system
  accidentally downloading blobs.
* Uses the coreboot build system to generate TOP SWAP bootblocks on i945,
  rather than using dd. This means that the 2nd bootblock is present in CBFS,
  reducing the chance that it will be overwritten at build time.
* They've started integrating the Guix package manager into their build system,
  for example they now use Guix (not to be confused with Guix System, which
  ironically cannot build GNU Boot due to lack of GCC-Gnat) to build GRUB.
  This results in higher maintenance burden and slows down development of
  GNU Boot - I considered using Nix in lbmk, but I need to be able to change
  everything quickly. Extra complexity like that is overkill. The benefit
  though is that users can more easily build the software, without thinking
  about it as much.

GNU Boot 0.1 RC has not altered the coreboot code on any machines, nor the
GRUB code, and in fact it seems to be vanilla GRUB 2.06. Most of the changes
by 0.1 RC3 have been further integration of the Untitled Static Site Generator
maintained by Leah Rowe, adapting Untitled (which builds the Libreboot,
Canoeboot and GNU Boot websites) so that it can be used with the GNU Savannah
infrastructure more easily (as of 2 January 2024, the GNU Boot project is
considering adapting the website for use with Haunt instead of Untitled).

GNU Boot's code complexity increased, considerably. Counting build scripts,
the GNU Boot build system is **3216 lines of code**, as of 0.1 RC3 - whereas,
the Canoeboot *20240102* build system is **1127 lines of code**. That's about
3x smaller, but don't let size fool you. Canoeboot's [design](docs/maintain/)
is highly efficient. It does a lot more than GNU Boot currently does. You'll
note that this figure of 1127 is *lower* than the one given for Canoeboot
version 20231107, above. That is because Canoeboot became *more* efficient
since then!

Despite the considerable reduction in code size, by comparison, the Canoeboot
build system is *much* more powerful. It *does* a lot more, patching and
building a lot more projects, including U-Boot, and it handles cross compilation
too, for ARM - it also integrates Serprog firmware projects, for STM32
and RP2040 devices, and has much better support for autoconf/cmake-based projects.

The Canoeboot build system is so vastly efficient due to its design. GNU Boot
is based upon and expanded from Libreboot 20220710, which used a much more
complicated build system design. The *Canoeboot* build system inherits changes
from Libreboot Build System Audit [1](https://libreboot.org/news/audit.html),
[2](https://libreboot.org/news/audit2.html),
[3](https://libreboot.org/news/audit3.html) and
[4](https://libreboot.org/news/audit4.html), the purpose of which was (and is)
to reduce build system complexity, improving efficiency and reliability, while
adding many more features. Essentially, Canoeboot generalises a lot more logic,
handling codebases (download, patch, configure, compile) more generically with
a single script, whereas GNU Boot has separate scripts for each project, and
thus *duplicates* a lot of logic. While the latter design of GNU Boot is more
flexible in some ways, it does result in much higher complexity.

It should be noted that GNUBoot's code complexity *increased* a lot, relative
to Libreboot 20220710 which it forked, while not actually adding any new
functionality (none that will be beneficial to most users). The 20220710
build system was *2117* lines of code, versus GNU Boot's 3216,
Canoeboot's 1127 and Libreboot's (on 2 January 2024) 1562!

That's all. The information above, and more, will be properly merged into this
page when the next release of Canoeboot comes out, or when/if GNU Boot makes
considerable technical improvements to their project.

As of 2 January 2024, GNU Boot is *still* about 1 year behind on code and
about 2 years behind on documentation, when comparing to the technical
progress of Libreboot; the same numbers also apply to Canoeboot vs GNU Boot.
