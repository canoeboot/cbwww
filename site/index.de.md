---
title: Canoeboot projekt
x-toc-enable: true
...

Das *Canoeboot* Projekt bietet
eine [freie](https://writefreesoftware.org/learn) *Boot
Firmware* welche auf [bestimmten Intel/AMD x86 und ARM Geräten](docs/hardware/)
die Hardware initialisiert (z.b. Speicher-Controller, CPU, Peripherie),
und dann einen Bootloader für dein Betriebssystem startet. [GNU+Linux](docs/gnulinux/)
sowie [BSD](docs/bsd/) werden gut unterstützt. Es ersetzt proprietäre BIOS/UEFI
Firmware. Hilfe ist verfügbar
via [\#canoeboot](https://web.libera.chat/#canoeboot)
und [Libera](https://libera.chat/) IRC.

<img tabindex=1 class="r" src="https://av.canoeboot.org/t60logo.jpg" /><span class="f"><img src="https://av.canoeboot.org/t60logo.jpg" /></span>

**NEUESTE VERSION: Die neueste Version von Canoeboot ist 20231107, veröffentlicht
am 7. November 2023.
Siehe auch: [Canoeboot 20231107 release announcement](news/canoeboot20231107.md).**

Canoeboot was *originally* named [nonGeNUine Boot](news/nongenuineboot20230717.html),
provided as a proof of concept for the [GNU Boot](https://libreboot.org/news/gnuboot.html)
or *gnuboot* project to use a more modern Libreboot base, but
they went in their own direction instead. Canoeboot development was continued,
and it maintains sync with the Libreboot project, as a parallel development
effort. See: [How are Canoeboot releases engineered?](about.md#how-releases-are-engineered)

Canoeboot adheres to the *GNU Free System Distribution Guidelines* as policy,
whereas Libreboot adheres to its own [Binary Blob Reduction
Policy](https://libreboot.org/news/policy.html). Canoeboot and Libreboot
are *both* maintained by the same person, Leah Rowe, sharing code back and forth.

Warum solltest Du *Canoeboot* verwenden?
----------------------------

Canoeboot gibt dir [Freiheit](https://writefreesoftware.org/learn) welche
Du mit den meisten Boot Firmwares nicht hast, und zusätzlich schnellere Boot
Geschwindigkeiten sowie [höhere Sicherheit](docs/gnulinux/grub_hardening.md).
Es ist extrem leistungsfähig und für viele Einsatzzwecke [konfigurierbar](docs/maintain/).

Du hast Rechte. Das Recht auf Privatsphäre, Gedankenfreiheit, Meinungsäußerungsfreiheit,
und Informationsfreiheit. In diesem Zusammenhang, Canoeboot gibt dir diese Rechte.
Deine Freiheit ist wichtig.
[Das Recht auf Reparatur](https://yewtu.be/watch?v=Npd_xDuNi9k) ist wichtig.
Viele Menschen verwenden proprietäre (non-libre)
Boot Firmware, sogar wenn Sie ein [Libre OS](https://www.openbsd.org/) verwenden.
Proprietäre Firmware [enthält](faq.html#intel) häufig [Hintertüren](faq.html#amd),
und kann fehlerhaft sein. Das Canoeboot Projekt wurde im Oktober 2023 gegründet, 
mit dem Ziel, Coreboot Firmware auch für technisch unerfahrene Nutzer verfügbar 
zu machen.

Das Canoeboot Projekt verwendet [Coreboot](https://www.coreboot.org/) für
[die Initialiserung der Hardware](https://doc.coreboot.org/getting_started/architecture.html).
Die Coreboot Installation ist für unerfahrene Benutzer überaus schwierig; sie
übernimmt lediglich die Basis Initialisierung und springt dann zu einem separaten
[payload](https://doc.coreboot.org/payloads.html) Programm (z.B.
[GRUB](https://www.gnu.org/software/grub/),
[Tianocore](https://www.tianocore.org/)), welche zusätzlich konfiguriert werden muss.
*Canoeboot löst dieses Problem*; es ist eine *Coreboot Distribution* mit
einem [automatisierten Build System](docs/build/) welches vollständige *ROM images* 
für eine robustere Installation erstellt. 
Dokumentation ist verfügbar.

Canoeboot ist kein Coreboot Fork
-----------------------------------

<img tabindex=1 class="l" style="max-width:25%;" src="https://av.canoeboot.org/dip8/adapter.jpg" /><span class="f"><img src="https://av.canoeboot.org/dip8/adapter.jpg" /></span>

Tatsächlich versucht Canoeboot so nah am regulären Coreboot zu bleiben wie möglich,
für jedes Board, aber mit vielen automatisch durch das Canoeboot Build System zur 
Verfügung gestellten verschiedenen Konfigurationstypen. 

Ebenso wie *Alpine Linux* eine *Linux Distribution* ist, ist Canoeboot eine
*Coreboot Distribution*. Sofern Du ein ROM Image von Grund auf herstellen möchtest,
musst Du zunächst Konfigurationen auf Experten Level durchführen,
und zwar für Coreboot, GRUB sowie sämtliche Software die Du sonst noch verwenden 
möchtest um das ROM Image vorzubereiten. Mithilfe von *Canoeboot* kannst Du 
sprichwörtlich von Git oder einem anderen Quell-Archiv herunterladen, anschliessend 
`make` ausführen, und es wird komplette ROM Images herstellen, ohne das Benutzer 
Eingaben oder Eingreifen von Nöten sind. Die Konfiguration wurde bereits im 
Vorfeld erledigt.

Sofern Du das reguläre Coreboot herstellen wollen würdest, ohne hierfür das automatisierte
Canoeboot Build System zu verwenden, würde dies deutlich mehr Eingreifen und ein 
sehr tiefgreifendes technisches Verständnis voraussetzen um eine funktionsfähige 
Konfiguration herzustellen. 

Reguläre Binär Veröffentlichungen bieten diese ROM Images vor-kompiliert,
und Du kannst dies einfach installieren ohne spezielle technische 
Kenntnisse oder Fertigkeiten abgesehen von der Fähigkeit einer 
[vereinfachten Anleitung, geschrieben für technisch unerfahrene Benutzer](docs/install/) zu folgen.
