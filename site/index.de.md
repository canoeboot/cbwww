---
title: Freie und Open Source BIOS/UEFI Firmware
x-toc-enable: true
...

Canoeboot ist ein [Coreboot-Distribution](docs/maintain/) (coreboot distro),
so wie Debian eine Linux-Distribution ist. Das *Canoeboot* Projekt bietet
eine [freie](https://writefreesoftware.org/learn) *Boot
Firmware* welche auf [bestimmten Intel/AMD x86 und ARM Geräten](docs/install/#which-systems-are-supported-by-canoeboot)
die Hardware initialisiert (z.b. Speicher-Controller, CPU, Peripherie),
und dann einen Bootloader für dein Betriebssystem startet. [Linux](docs/linux/)
sowie [BSD](docs/bsd/) werden gut unterstützt. Es ersetzt proprietäre BIOS/UEFI
Firmware. Hilfe ist verfügbar
via [\#canoeboot](https://web.libera.chat/#canoeboot)
und [Libera](https://libera.chat/) IRC.

<img tabindex=1 class="r" src="https://av.canoeboot.org/t60logo.jpg" /><span class="f"><img src="https://av.canoeboot.org/t60logo.jpg" /></span>

Canoeboot provides GNU boot loader "[GRUB](docs/linux/)" and SeaBIOS payloads
on x86/x86\_64
Intel/AMD motherboards, and a [U-Boot UEFI payload](docs/uboot/) *for coreboot*
on ARM64(Aarch64) motherboards.
An [x86/x86\_64 U-Boot UEFI payload](docs/uboot/uboot-x86.md) is also available
on some boards. The x86, x86\_64 and arm64 U-Boot payloads provide a lightweight
UEFI boot implementation, which can boot many Linux distros and BSD systems.
The SeaBIOS and GRUB payloads also boot Linux/BSD systems. Which one you use
depends on your preferences. Canoeboot's [design](docs/maintain/) incorporates
all of these boot methods in a single image, so you can choose which one you use
at boot time, and more payloads (e.g. Linux kexec payload) are planned for
future releases.

**NEUESTE VERSION: Die neueste Version von Canoeboot ist 20250107, veröffentlicht
am 7 January 2025.
Siehe auch: [Canoeboot 20250107 release announcement](news/canoeboot20250107.md).**

Warum solltest Du *Canoeboot* verwenden?
----------------------------

Canoeboot gibt dir [Freiheit](https://writefreesoftware.org/learn) welche
Du mit den meisten Boot Firmwares nicht hast, und zusätzlich schnellere Boot
Geschwindigkeiten sowie [höhere Sicherheit](docs/linux/grub_hardening.md).
Es ist extrem leistungsfähig und für viele Einsatzzwecke [konfigurierbar](docs/maintain/).

Du hast Rechte. Das Recht auf Privatsphäre, Gedankenfreiheit, Meinungsäußerungsfreiheit,
und Informationsfreiheit. In diesem Zusammenhang, Canoeboot gibt dir diese Rechte.
Deine Freiheit ist wichtig.
[Das Recht auf Reparatur](https://en.wikipedia.org/wiki/Right_to_repair) ist wichtig.
Viele Menschen verwenden proprietäre (non-libre)
Boot Firmware, sogar wenn Sie ein Libre OS verwenden.
Proprietäre Firmware [enthält](faq.html#intel) häufig [Hintertüren](faq.html#amd),
und kann fehlerhaft sein. Das Canoeboot Projekt wurde im Oktober 2023 gegründet, 
mit dem Ziel, Coreboot Firmware auch für technisch unerfahrene Nutzer verfügbar 
zu machen.

Das Canoeboot Projekt verwendet [Coreboot](https://www.coreboot.org/) für
[die Initialiserung der Hardware](https://doc.coreboot.org/getting_started/architecture.html).
Die Coreboot Installation ist für unerfahrene Benutzer überaus schwierig; sie
übernimmt lediglich die Basis Initialisierung und springt dann zu einem separaten
[payload](https://doc.coreboot.org/payloads.html) Programm (z.B.
[GRUB](https://www.gnu.org/software/grub/),
[Tianocore](https://www.tianocore.org/)), welche zusätzlich konfiguriert werden muss.
*Canoeboot löst dieses Problem*; es ist eine *Coreboot Distribution* mit
einem [automatisierten Build System](docs/build/) welches vollständige *ROM images* 
für eine robustere Installation erstellt. 
Dokumentation ist verfügbar.

Canoeboot ist kein Coreboot Fork
-----------------------------------

<img tabindex=1 class="l" style="max-width:25%;" src="https://av.vimuser.org/uboot-canoe.png" /><span class="f"><img src="https://av.vimuser.org/uboot-canoe.png" /></span>

Tatsächlich versucht Canoeboot so nah am regulären Coreboot zu bleiben wie möglich,
für jedes Board, aber mit vielen automatisch durch das Canoeboot Build System zur 
Verfügung gestellten verschiedenen Konfigurationstypen. 

Ebenso wie *Debian* eine *Linux Distribution* ist, ist Canoeboot eine
*Coreboot Distribution*. Sofern Du ein ROM Image von Grund auf herstellen möchtest,
musst Du zunächst Konfigurationen auf Experten Level durchführen,
und zwar für Coreboot, GRUB sowie sämtliche Software die Du sonst noch verwenden 
möchtest um das ROM Image vorzubereiten. Mithilfe von *Canoeboot* kannst Du 
sprichwörtlich von Git oder einem anderen Quell-Archiv herunterladen, anschliessend 
`make` ausführen, und es wird komplette ROM Images herstellen, ohne das Benutzer 
Eingaben oder Eingreifen von Nöten sind. Die Konfiguration wurde bereits im 
Vorfeld erledigt.

Sofern Du das reguläre Coreboot herstellen wollen würdest, ohne hierfür das automatisierte
Canoeboot Build System zu verwenden, würde dies deutlich mehr Eingreifen und ein 
sehr tiefgreifendes technisches Verständnis voraussetzen um eine funktionsfähige 
Konfiguration herzustellen. 

Reguläre Binär Veröffentlichungen bieten diese ROM Images vor-kompiliert,
und Du kannst dies einfach installieren ohne spezielle technische 
Kenntnisse oder Fertigkeiten abgesehen von der Fähigkeit einer 
[vereinfachten Anleitung, geschrieben für technisch unerfahrene Benutzer](docs/install/) zu folgen.
