---
title: Free and Open Source BIOS/UEFI boot firmware
x-toc-enable: true
...

*Canoeboot* is a *[coreboot distribution](docs/maintain)* (coreboot distro),
in the same way that Debian is a *Linux distribution*; Canoeboot is also
a [Libreboot](https://libreboot.org) fork. Canoeboot provides
[free, open source](https://writefreesoftware.org/) (*libre*) boot
firmware based on coreboot, replacing proprietary BIOS/UEFI firmware
on [specific Intel/AMD x86 and ARM based motherboards](docs/install/#which-systems-are-supported-by-canoeboot),
including laptop and desktop computers. It initialises the hardware (e.g. memory
controller, CPU, peripherals) and starts a bootloader for your operating
system. [Linux](docs/linux/) and [BSD](docs/bsd/) are well-supported. Help is
available via [\#canoeboot](https://web.libera.chat/#canoeboot)
on [Libera](https://libera.chat/) IRC.

<img tabindex=1 class="r" alt="Lenovo Thinkpad T60 with Canoeboot" src="https://av.canoeboot.org/t60logo.jpg" /><span class="f"><img alt="Lenovo ThinkPad T60 with Canoeboot" src="https://av.canoeboot.org/t60logo.jpg" /></span>

Canoeboot provides GNU boot loader "[GRUB](docs/linux/)" and SeaBIOS payloads
on x86/x86\_64
Intel/AMD motherboards, and a [U-Boot UEFI payload](docs/uboot/) *for coreboot*
on ARM64(Aarch64) motherboards.
An [x86/x86\_64 U-Boot UEFI payload](docs/uboot/uboot-x86.md) is also available
on some boards. The x86, x86\_64 and arm64 U-Boot payloads provide a lightweight
UEFI boot implementation. Canoeboot's [design](docs/maintain/) incorporates all
of these payloads in a single image, and you choose one at boot time.

Canoeboot is a *special fork* of [Libreboot](https://libreboot.org/), maintained
in parallel to it by the same developer (Leah Rowe), who maintains both projects.
Canoeboot [removes all binary blobs](news/policy.md) from coreboot, unlike
Libreboot which has a more
pragmatic [Binary Blob Reduction Policy](https://libreboot.org/news/policy.html).
Canoeboot is provided for [Free Software](https://writefreesoftware.org/learn)
purists, who *only* want Free Software, while Libreboot supports *much newer*
hardware. [Canoeboot even removes CPU microcode
updates](news/policy.html#more-detailed-insight-about-microcode),
regardless of the negative impact this has on system stability.

**NEW RELEASE: The latest release is Canoeboot 20250107, released
on 7 January 2025.
See: [Canoeboot 20250107 release announcement](news/canoeboot20250107.md).**

*We* believe the freedom to [study, share, modify and use
software](https://writefreesoftware.org/), without any
restriction, is one of the fundamental human rights that everyone must have.
In this context, *software freedom* matters. Your freedom matters. Education
matters.
[Right to repair](https://en.wikipedia.org/wiki/Right_to_repair) matters; Canoeboot lets
you continue to use your hardware, with continued firmware updates. All of this
is *why* Canoeboot exists.

Overview of Canoeboot design
----------------------------

<img tabindex=1 class="l" alt="U-Boot x86 coreboot payload" style="max-width:25%;" src="https://av.vimuser.org/x200-uboot.jpg" /><span class="f"><img alt="U-Boot x86 coreboot payload" src="https://av.vimuser.org/x200-uboot.jpg" /></span>

Canoeboot provides [coreboot](https://coreboot.org/) for [machine
initialisation](https://doc.coreboot.org/getting_started/architecture.html),
which then jumps to a [payload](https://doc.coreboot.org/payloads.html) in
the boot flash; coreboot works with many payloads, which boot your operating
system e.g. Linux/BSD.

Canoeboot makes coreboot easy to use for
non-technical users, by providing a [fully automated build
system](docs/maintain/), [automated build process](docs/build/) and
[user-friendly installation instructions](docs/install/), in addition to
regular [binary releases](download.md) that provide pre-compiled ROM images for
installation on supported hardware. Without automation such as that provided by
Canoeboot, coreboot would be inaccessible for most users; you can also
still [reconfigure](docs/maintain/) Canoeboot however you wish.

Why use Canoeboot?
------------------

<img tabindex=1 class="r" src="https://av.vimuser.org/uboot-canoe.png" /><span class="f"><img src="https://av.vimuser.org/uboot-canoe.png" /></span>

If you're already inclined towards free software, maybe already a coreboot user,
Canoeboot makes it easier to either get started or otherwise maintain coreboot
on your machine, via build automation. It provides regular tested releases,
pre-assembled, often with certain patches on top of coreboot (and other code)
to ensure stability. By comparison, coreboot uses a rolling-release model, with
a snapshot of the codebase every few months; it is very much developer-oriented,
whereas Canoeboot is specifically crafted for end users. In other words, the
purpose of Canoeboot is to *Just Work*. Direct configuration and installation
of coreboot is also possible, but Canoeboot makes it *much* easier.

Canoeboot gives you [Free Software](https://writefreesoftware.org/) that
you otherwise can't get with most other boot firmware, plus faster boot speeds
and [better security](docs/linux/grub_hardening.md). It's extremely powerful
and [configurable](docs/maintain/) for many use cases. If you're unhappy with
the restrictions (not to mention, security issues) imposed on you by proprietary
BIOS vendors, then Canoeboot is one possible choice for you. Since it inherits
coreboot, it doesn't have any known backdoors in the code, nor does it contain
legacy cruft from the 1980s. Canoeboot provides a sleek, fast boot experience
for Linux/BSD systems, based on coreboot which is regularly audited and improved.

Canoeboot is more reliable than proprietary firmware. Many people use proprietary
(non-libre) boot firmware, even if they use [a libre OS](https://www.openbsd.org/).
Proprietary firmware often [contains](faq.html#intel) [backdoors](faq.html#amd),
and can be buggy. The Canoeboot project was founded in October 2023, with the
express purpose of making coreboot firmware accessible for non-technical users;
the Libreboot project, upon which Canoeboot is based, was founded for the same
reason, in December 2013.

Canoeboot is a community-oriented project, with a focus on helping users escape
proprietary boot firmware; we ourselves want to live in a world where all software
is [free](https://writefreesoftware.org/), and so, Canoeboot is an effort to
help get closer to that world. Unlike the big vendors, we don't try to stifle
you in any way, nor do we see you as a security threat; we regard the ability
to use, study, modify and redistribute software freely to be a human right that
everyone must have. Extended to computers, these are products that you purchased,
and so you should have the freedom to change them in any way you like. When you
see Intel talk about their *Boot Guard* (which prevents coreboot by only letting
firmware signed by them be executed) or other vendors imposing similar
restrictions, and you hear them talk about "security", they are only talking
about *their* security, not yours. In the Canoeboot project, it is reversed; we
see Intel Boot Guard and similar such technologies as an attack on your freedom
over your own property (your computer), and so, we make it our mission to help
you [wrest](https://trmm.net/TOCTOU/) back such control.

Canoeboot is not a fork of coreboot
-----------------------------------

<img tabindex=1 class="r" style="max-width:15%;" alt="Dell Latitude E6400 with Canoeboot" src="https://av.canoeboot.org/e6400/e6400-seabios.jpg" /><span class="f"><img alt="Dell Latitude E6400 with Canoeboot" src="https://av.canoeboot.org/e6400/e6400-seabios.jpg" /></span>

In fact, Canoeboot tries to stay as close to *stock* coreboot as possible,
for each board, but with many different types of configuration provided
automatically by the Canoeboot build system.

In the same way that *Alpine Linux* is a *Linux distribution*, Canoeboot is
a *coreboot distribution*. If you want to build a ROM image from scratch, you
otherwise have to perform expert-level configuration of coreboot, GRUB and
whatever other software you need, to prepare the ROM image. With *Canoeboot*,
you can literally download from Git or a source archive, and run a simple script,
and it will build entire ROM images. An automated build system, named `cbmk`
(CanoeBoot MaKe), builds these ROM images automatically, without any user input
or intervention required. Configuration has already been performed in advance.

If you were to build regular coreboot, without using Canoeboot's automated
build system, it would require a lot more intervention and decent technical
knowledge to produce a working configuration.

Regular binary releases of Canoeboot provide these
ROM images pre-compiled, and you can simply install them, with no special
knowledge or skill except the ability to
follow [simplified instructions, written for non-technical
users](docs/install/).

### How to help Canoeboot

The [Libreboot tasks page](https://libreboot.org/tasks/) lists tasks that
could (will) be worked on. It will be updated over time as more tasks are
added/completed. If you want to help, you could pick one of these tasks and
work on it.

<img tabindex=1 class="l" style="max-width:25%;" src="https://av.canoeboot.org/e6400/e6400xfr-seabios.jpg" /><span class="f"><img src="https://av.canoeboot.org/e6400/e6400xfr-seabios.jpg" /></span>

Canoeboot *tracks Libreboot*, on a per-commit basis. This is done by Leah Rowe,
who also maintains Libreboot, but Canoeboot only merges those changes that are
conducive to a zero-blob setup; for example, a change pertaining to boards that
require Intel ME firmware would not be imported into Canoeboot. Libreboot
changes are cherry-picked, so that Canoeboot remains in relative sync at all
times. This may seem extreme, but the way it's done actually minimizes
development overhead completely, and you can read about that process on
the [about](about.md) page.

Unless you want to work on a feature that is truly specific to Canoeboot, that
Libreboot genuinely doesn't have, then you should contribute directly to
Canoeboot. Otherwise, it is our preference that you submit to *Libreboot* first;
however, if you're ideologically uncomfortable with Libreboot, you may submit
to Canoeboot and your changes will (if conducive to Libreboot development) also
be cherry-picked into Libreboot; the usual development workflow is the other
way around, as described above.

The *single* biggest way you can help is to *add* new motherboards in Libreboot,
by submitting a config. Anything coreboot supports can be integrated in
Libreboot, with ROM images provided in releases. If the board
is [suitable](news/policy.md) for Canoeboot, it will then also be merged
in Canoeboot. See:

* [Apply to become a Libreboot board maintainer/tester](https://libreboot.org/docs/maintain/testing.html)
* [Libreboot porting guide for new motherboards](https://libreboot.org/docs/maintain/porting.html)
* [Libreboot build system documentation](https://libreboot.org/docs/maintain/)
* [Canoeboot build system documentation](docs/maintain/)

After that, there is build system maintenance (see above), and *documentation*
which we take seriously. Documentation is critical, in any project.

*User support* is also critical. Stick around on IRC, and if you're competent
to help someone with their issue (or wily enough to learn with them), that is
a great service to the project. A lot of people also ask for user support
on the `r/canoeboot` subreddit.

You can check bugs listed on
the [Libreboot bug tracker](https://codeberg.org/libreboot/lbmk/issues);
please also check the [Canoeboot bug
tracker](https://codeberg.org/canoeboot/cbmk/issues).

If you spot a bug and have a fix, [here are instructions for how to send
Canoeboot patches](git.md), and you can also report it. Also, this entire website is
written in Markdown and hosted in a [separate
repository](https://codeberg.org/canoeboot/cbwww) where you can send patches.

The above links, in the previous paragraph, are to Canoeboot resources. Again,
please submit to Libreboot first, if that seems more appropriate, and your
changes will be cherry-picked into Canoeboot. Please look
at [Libreboot's Git page](https://libreboot.org/git.html)
and [Libreboot's lbwww repository](https://codeberg.org/libreboot/lbwww) where
you can send patches.

Any and all development discussion and user support are all done on the IRC
channel. More information is on the [contact page](contact.md).

### Translations needed, for canoeboot.org

If you want to help with translations, you can translate pages, update existing
translations and submit your translated versions. For instructions, please
read:

[How to submit translations for libreboot.org](https://libreboot.org/news/translations.html)

You can also adapt these instructions for the Canoeboot website. Where a page
is identical on both projects, it may make sense to submit a translation there,
otherwise please also send to Canoeboot simultaneously if that seems wise;
otherwise, simply sending translations to Libreboot will be appreciated, and
your translation will be adapted for canoeboot.org.

Even if someone is already working on translations in a given language, we can
always use multiple people. The more the merrier!
