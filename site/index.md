---
title: Canoeboot project
x-toc-enable: true
...

The *Canoeboot* project provides
[free, open source](https://writefreesoftware.org/learn) (*libre*) boot
firmware based on coreboot, replacing proprietary BIOS/UEFI firmware
on [specific Intel/AMD x86 and ARM based motherboards](docs/hardware/),
including laptop and desktop computers. It initialises the hardware (e.g. memory
controller, CPU, peripherals) and starts a bootloader for your operating
system. [GNU+Linux](docs/gnulinux/) and [BSD](docs/bsd/) are well-supported. Help is
available via [\#canoeboot](https://web.libera.chat/#canoeboot)
on [Libera](https://libera.chat/) IRC.

<img tabindex=1 class="r" src="https://av.canoeboot.org/t60logo.jpg" /><span class="f"><img src="https://av.canoeboot.org/t60logo.jpg" /></span>

**NEW RELEASE: The latest release is Canoeboot 20231107, released
on 7 November 2023.
See: [Canoeboot 20231107 release announcement](news/canoeboot20231107.md).**

Canoeboot was *originally* named [nonGeNUine Boot](news/nongenuineboot20230717.html),
provided as a proof of concept for the [GNU Boot](https://libreboot.org/news/gnuboot.html)
or *gnuboot* project to use a more modern Libreboot base, but
they went in their own direction instead. Canoeboot development was continued,
and it maintains sync with the Libreboot project, as a parallel development
effort. See: [How are Canoeboot releases engineered?](about.md#how-releases-are-engineered)

Canoeboot adheres to the *GNU Free System Distribution Guidelines* as policy,
whereas Libreboot adheres to its own [Binary Blob Reduction
Policy](https://libreboot.org/news/policy.html). Canoeboot and Libreboot
are *both* maintained by the same person, Leah Rowe, sharing code back and forth.

Why should you use *Canoeboot*?
----------------------------

Canoeboot gives you [freedoms](https://writefreesoftware.org/learn) that
you otherwise can't get with most other boot firmware, plus faster boot speeds
and [better security](docs/gnulinux/grub_hardening.md). It's extremely powerful
and [configurable](docs/maintain/) for many use cases. Canoeboot is a *special
fork* of Libreboot, maintained in parallel to it by the same developer (Leah
Rowe); Canoeboot complies with the GNU Free System Distribution Guidelines,
whereas Libreboot adopts a more pragmatic [Binary Blob Reduction
Policy](https://libreboot.org/news/policy.html). Consequently, *Canoeboot* only
supports a very limited subset of hardware from coreboot that is known to boot
without binary blobs. Many other boards in coreboot require binary blobs for
things like memory controller initialisation. Canoeboot *removes* binary blobs
from coreboot and U-Boot, which are then provided "de-blobbed" in releases.

*We* believe the freedom to [study, share, modify and use
software](https://writefreesoftware.org/), without any
restriction, is one of the fundamental human rights that everyone must have.
In this context, *software freedom* matters. Your freedom matters. Education
matters.
[Right to repair](https://yewtu.be/watch?v=Npd_xDuNi9k) matters.
Many people use proprietary (non-libre)
boot firmware, even if they use [a libre OS](https://www.openbsd.org/).
Proprietary firmware often [contains](faq.html#intel) [backdoors](faq.html#amd),
and can be buggy. The Canoeboot project was founded in October 2023, with the
express purpose of making coreboot firmware accessible for non-technical users.

The Canoeboot project uses [coreboot](https://www.coreboot.org/) for [hardware
initialisation](https://doc.coreboot.org/getting_started/architecture.html).
Coreboot is notoriously difficult to install for most non-technical users; it
handles only basic initialization and jumps to a separate
[payload](https://doc.coreboot.org/payloads.html) program (e.g.
[GRUB](https://www.gnu.org/software/grub/),
[Tianocore](https://www.tianocore.org/)), which must also be configured.
*Canoeboot solves this problem*; it is a *coreboot distribution* with
an [automated build system](docs/build/) that builds complete *ROM images*, for
more robust installation. Documentation is provided.

Canoeboot is not a fork of coreboot
-----------------------------------

<img tabindex=1 class="l" style="max-width:25%;" src="https://av.canoeboot.org/dip8/adapter.jpg" /><span class="f"><img src="https://av.canoeboot.org/dip8/adapter.jpg" /></span>

In fact, Canoeboot tries to stay as close to *stock* coreboot as possible,
for each board, but with many different types of configuration provided
automatically by the Canoeboot build system.

In the same way that *Alpine Linux* is a *Linux distribution*, Canoeboot is
a *coreboot distribution*. If you want to build a ROM image from scratch, you
otherwise have to perform expert-level configuration of coreboot, GRUB and
whatever other software you need, to prepare the ROM image. With *Canoeboot*,
you can literally download from Git or a source archive, and run `make`, and it
will build entire ROM images. An automated build system, named `cbmk`
(CanoeBoot MaKe), builds these ROM images automatically, without any user input
or intervention required. Configuration has already been performed in advance.

If you were to build regular coreboot, without using Canoeboot's automated
build system, it would require a lot more intervention and decent technical
knowledge to produce a working configuration.

Regular binary releases of Canoeboot provide these
ROM images pre-compiled, and you can simply install them, with no special
knowledge or skill except the ability to
follow [simplified instructions, written for non-technical
users](docs/install/).
