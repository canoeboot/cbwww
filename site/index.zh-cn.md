---
title: 自由且开源 BIOS/UEFI 固件
x-toc-enable: true
...

Canoeboot 是 [Libreboot](https://libreboot.org/) 的分支。
*Canoeboot* 项目基于 coreboot 提供了[自由且开源](https://writefreesoftware.org/zh-cn/learn/)的引导固件，替代了特定基于 Intel/AMD x86 及 ARM 的主板（包括笔记本和桌面计算机）上的专有 BIOS/UEFI 固件。它首先对硬件（如内存控制器、CPU、外设）进行初始化，然后为操作系统启动 bootloader。本项目对 [Linux](docs/linux/) 和 [BSD](docs/bsd/) 支持良好。寻求帮助，可以前往 [Libera](https://libera.chat/) IRC 上的 [\#canoeboot](https://web.libera.chat/#canoeboot) 频道。

<img tabindex=1 class="r" src="https://av.canoeboot.org/t60logo.jpg" /><span class="f"><img src="https://av.canoeboot.org/t60logo.jpg" /></span>

**新版发布: 最新版本 Canoeboot 20250107 已在 2025 年 1 月 7 日发布。详见: [Canoeboot 20250107 发布公告](news/canoeboot20250107.md).**

Canoeboot provides GNU boot loader "[GRUB](docs/linux/)" and SeaBIOS payloads
on x86/x86\_64
Intel/AMD motherboards, and a [U-Boot UEFI payload](docs/uboot/) *for coreboot*
on ARM64(Aarch64) motherboards.
An [x86/x86\_64 U-Boot UEFI payload](docs/uboot/uboot-x86.md) is also available
on some boards. The x86, x86\_64 and arm64 U-Boot payloads provide a lightweight
UEFI boot implementation, which can boot many Linux distros and BSD systems.
The SeaBIOS and GRUB payloads also boot Linux/BSD systems. Which one you use
depends on your preferences. Canoeboot's [design](docs/maintain/) incorporates
all of these boot methods in a single image, so you can choose which one you use
at boot time, and more payloads (e.g. Linux kexec payload) are planned for
future releases.

为什么要使用 *Canoeboot*?
----------------------------

Canoeboot 赋予了你[自由](https://writefreesoftware.org/learn)，而这等自由，是你用其他引导固件得不到的。同时，它的启动速度更加快，[安全性也更加高](docs/linux/grub_hardening.md)。在各种情况下使用，它都十分强大，具有高度[可配置性](docs/maintain/)。

*我们*相信，不受限制地[研究、分享、修改及使用软件](https://writefreesoftware.org/)的自由，是每个人都必须享有的基本人权的一部分。这时，*软件自由*至关重要。你的自由至关重要。教育至关重要。[修理权](https://en.wikipedia.org/wiki/Right_to_repair)至关重要。尽管许多人在用自由的操作系统，但他们用的引导固件却是专有（非自由）的。专有固件常常[包含](faq.html#intel)了[后门](faq.html#amd)，并且也可能出问题。2023 年 10 月，我们建立了 Canoeboot 项目，目的是让不懂技术的用户能使用 coreboot 固件。

Canoeboot 项目使用 [coreboot](https://www.coreboot.org/) 来[初始化硬件](https://doc.coreboot.org/getting_started/architecture.html)。对大部分不懂技术的用户来说，coreboot 是出了名地难安装；它只处理了基础的初始化，然后跳转进入单独的 [payload](https://doc.coreboot.org/payloads.html) 程序（例如 [GRUB](https://www.gnu.org/software/grub/)、[Tianocore](https://www.tianocore.org/)），而后者也需要进行配置。*Canoeboot 解决了这样的问题*；他是一个 *coreboot 发行版*，配有[自动构建系统](docs/build/)，能够构建完整的 *ROM 镜像*，从而让安装更加稳定。另有文档可参考。

Canoeboot 不是 coreboot 的分支
-----------------------------------

<img tabindex=1 class="l" style="max-width:25%;" src="https://av.vimuser.org/uboot-canoe.png" /><span class="f"><img src="https://av.vimuser.org/uboot-canoe.png" /></span>

事实上，Canoeboot 对每一块主板，都尽可能保持与*标准*的 coreboot 接近，但 Canoeboot 构建系统也自动提供了许多不同类型的配置。

Canoeboot 是一个 *coreboot 发行版*，就好比 *Debian* 是一个 *Linux 发行版*。如果你想要从零开始构建 ROM 镜像，那你需要对 coreboot、GRUB 以及其他所需软件进行专业级别的配置，才能准备好 ROM 镜像。有了 *Canoeboot*，你只需要下载 Git 仓库或者源代码归档，然后运行 `make`，接着就能构建整个 ROM 镜像。一套自动构建系统，名为 `cbmk`（Canoeboot Make），将自动构建 ROM 镜像，而无需任何用户输入或干预。配置已经提前完成。

如果你要构建常规的 coreboot，而不使用 Canoeboot 的自动构建系统，那么需要有很多的干预以及相当的技术知识，才能写出一份能工作的配置。

Canoeboot 的常规二进制版本，提供了这些预编译的 ROM 镜像。你可以轻松安装它们，而无需特别的知识和技能，只要能遵循[写给非技术用户的简单指南](docs/install/)。
