---
title: Project contributors
x-toc-enable: true
...

Leah Rowe made this website for fun, based on Libreboot.

It is only a proof of concept. You should otherwise use Libreboot:

<https://libreboot.org/>
