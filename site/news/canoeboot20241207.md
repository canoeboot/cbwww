% Canoeboot 20241207 released! U-Boot UEFI on x86
% Leah Rowe in Canoe Leah Mode™
% 7 December 2024

Canoeboot is a *special fork* of [Libreboot](https://libreboot.org/), providing
a de-blobbed configuration on *fewer motherboards*; Libreboot supports more
hardware, and much newer hardware. More information can be found on Canoeboot's
[about](../about.html) page and by reading Libreboot's [Binary Blob Reduction Policy](https://libreboot.org/news/policy.html).

This release, Canoeboot 20241207, is based on and in sync with the recent
stable release of [Libreboot](https://libreboot.org/), namely
[Libreboot 20241206](https://libreboot.org/news/libreboot20241206.html).
Therefore, this can be considered a stable release of Canoeboot.

This page lists all changes since Canoeboot 20241102.

Open source BIOS/UEFI firmware
------------------------------

<img tabindex=1 class="r" src="https://av.vimuser.org/x200-uboot.jpg" /><span class="f"><img src="https://av.vimuser.org/x200-uboot.jpg" /></span>

Canoeboot is a free/libre BIOS/UEFI replacement on x86 and ARM, providing
boot firmware that initialises the hardware in your computer, to then load an
operating system (e.g. Linux). It is specifically
a *[coreboot distribution](../docs/maintain/)*,
like how Debian is a Linux distribution. It provides an automated
build system to produce coreboot ROM images with a variety of payloads such as
GRUB or SeaBIOS, with regular well-tested releases to make coreboot as easy
to use as possible for non-technical users. From a project management perspective,
this works in *exactly* the same way as a Linux distro, providing a source-based
package manager (called cbmk) which patches sources and compiles coreboot images.
It makes use of [coreboot](https://www.coreboot.org/) for hardware initialisation,
and then a payload such as [SeaBIOS](https://www.seabios.org/SeaBIOS)
or [GRUB](https://www.gnu.org/software/grub/) to boot your operating
system; on ARM(chromebooks), we provide *U-Boot* (as a coreboot payload).

### U-Boot UEFI payload on x86\_64

For Canoeboot 20241207, today's release, U-Boot is *also* provided as an
optional coreboot payload on x86 machines. This provides a sensible UEFI
implementation, useful for booting Linux and BSD systems more easily. More
information available on the [U-Boot x86 page](../docs/uboot/uboot-x86.md).

This means that you can have a UEFI boot environment, even on machines where
the original vendor firmware never supported it. For example, the ThinkPad X200
in the photo is running U-Boot, and booting a distro via U-Boot's UEFI
implementation; that machine could not originally do UEFI.

Since this is based on a stable release, not much has changed; the focus has
been on bug fixes. However, the U-Boot x86 payload is a notable new feature.

Summarised list of changes
-------------------------

<img tabindex=1 class="r" src="https://av.vimuser.org/uboot-canoe.png" /><span class="f"><img src="https://av.vimuser.org/uboot-canoe.png" /></span>

Changes are in order per category, from newest to oldest:

### Feature changes

* U-Boot support, for both x86 and x86\_64, provided as a coreboot payload.
  This complements the existing ARM64 U-Boot support present in Canoeboot.
  Several changes have been made, for example Canoeboot has added an auto-boot
  timeout for the first selected entry on the bootflow menu (upstream does
  not currently support this feature, at the time of this Canoeboot release).

### Configuration changes

* Enabled the serial console by default, on these machines: Thinkpad X60, T60.
* U-Boot x86 enabled on almost every x86 board, chainloaded from SeaBIOS.
  The `seauboot` images boot directly into U-Boot first, from SeaBIOS, but
  U-Boot can be bypassed by using the ESC menu in SeaBIOS.

### Bug fixes

* `script/trees`: reset `PATH` per target, to avoid it being wrong in the next
  target, because we manipulate this when enabling cross compilation (by using
  the coreboot cross compiler).
* Disabled using Microcode updates in U-Boot builds on x86, because we remove
  the microcode from U-Boot so enabling it would yield a build error.
* `rom.sh`: Removed unnecessary shebang; this is not a main script, but only
  meant to be included from other scripts.
* Added `openssl-devel-engine` to Fedora dependencies
* Unset `displaymode` on normal `initmode` in the `rom.sh` script, otherwise
  images that use `normal` mode would have `normal_normal` in the image name;
  now it just has `normal` in the image name.
* U-Boot: merged an x86 bug fix from Simon Glass, well, two fixes: one silently
  disables the serial console if a serial device is not present, to prevent a
  hanging bug, and a second fix avoids clearing the display when initialising
  with VESA, because VESA ROMs already do this (we do not currently use VESA
  setups in Canoeboot, so this is a preventative bugfix).
* `script/trees`: unset `CROSS_COMPILE` per target, before setting it per
  target, so that the next target does not use the same configuration as the
  previous target, in cases where the next target does not enable a cross
  compiler. This was necessary to prevent build errors with the x86 U-Boot
  payload.
* Added `arm-none-eabi-gcc-cs-c++` to Fedora dependencies.

### Revision updates

* Updated U-Boot to v2024.10, on both ARM64 and x86 boards.
* Bumped coreboot/next to the latest coreboot revision, from several days
  prior to this Canoeboot release. This is currently used for the Dell
  OptiPlex 780 ports.

Git log
-------

This git log covers all changes in this audit, relative to Canoeboot 20241102.

```
* 4088cc12eb remove the purple patch on arm64 u-boot 
* 4a50e5a3e9 u-boot: Use bootflow menu by default for ARM64 boards 
* 81a29cef73 i made u-boot turquoise 
* 421e73c04c Add bootflow/branding patches to arm64 U-Boot too 
* 773cd8b384 Add canoeboot branding/version to U-Boot bootflow 
* ce27bb0ea0 Add auto-boot timeout for U-Boot's bootflow menu 
* 2ccc9215d8 8-sec auto-boot timeout for U-Boot's bootflow menu 
* ea9e4765f4 add /dump/ to .gitignore 
* 2d40305a35 u-boot: Enable USB with a preboot command 
* 0cea72c1cc u-boot: Update ARM64 boards to v2024.10 
* 6069668153 trees: reset PATH per-target 
* f6a2af67d5 Disable including Microcode in U-Boot builds 
* 106904ed81 rom.sh: Add U-Boot before SeaBIOS and GRUB (x86) 
* 19bc40962a rom.sh: Remove unnecessary shebang 
* 9a22d10e47 config/deps/fedora41: Add openssl-devel-engine to dependency list 
* 38dd860431 rom.sh: unset displaymode on normal initmode 
* b179ef63ed rom.sh: Don't build U-Boot on normal initmode 
* c885a63dd5 rom.sh: Don't build txtmode U-Boot images 
* dea09b8bf5 rom.sh: Support SeaUBoot for 64-bit x86 U-Boot 
* eea06c9659 U-Boot x86: Avoid clearing the VESA display 
* 5dd6a4e978 enable the serial console on thinkpad x60 
* 112b761926 enable the serial console on thinkpad t60 
* 8ba8cf3e60 Only boot 32-bit u-boot from grub, 64 from seabios 
* 6ff2a65a7c make the u-boot grub menuentry more useful 
* 0a90386ddb Re-enable U-Boot x86 on real motherboards 
* f3d68fade3 u-boot x86 serial/ns16550: disable UART as needed 
* 8333930599 Disable U-Boot x86 except on Qemu 
* d6cf658624 fix U-Boot hotkey mention in grub.cfg 
* 5e27c14b05 Update x86 U-Boot to v2024.10 (was v2024.07) 
* 186e3c0a23 grub.cfg: mark U-Boot as experimental in the menu 
* 414ff6095c trees: unset CROSS_COMPILE per target 
* 739207021d Enable x86 U-Boot payload on every x86 board 
* ac5c87681f Add U-Boot x86_64 payload 
* a70a9813d7 add arm-none-eabi-gcc-cs-c++ to fedora dependency 
* 5a641b071d Bump coreboot/next in line with lbmk 
* 2d2cdd2368 data/coreboot: add missing variable 
* 966fc8c23f Experimental U-Boot payload (32-bit dtb, U-Boot) 
```

This is 37 changes since Canoeboot 20241102.
