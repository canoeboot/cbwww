% Canoeboot 20250107 released!
% Leah Rowe in Canoe Leah Mode™
% 7 January 2025

Canoeboot is a *special fork* of [Libreboot](https://libreboot.org/), providing
a de-blobbed configuration on *fewer motherboards*; Libreboot supports more
hardware, and much newer hardware. More information can be found on Canoeboot's
[about](../about.html) page and by reading Libreboot's [Binary Blob Reduction Policy](https://libreboot.org/news/policy.html).

This release, Canoeboot 20250107, is based on and in sync with the recent
stable release of [Libreboot](https://libreboot.org/), namely
[Libreboot 20241206 revision 8](https://libreboot.org/news/libreboot20241206rev8.html) from 6
January 2025. Therefore, this can be considered a stable release of Canoeboot.

This page lists all changes since Canoeboot 20241207.

Open source BIOS/UEFI firmware
------------------------

<img tabindex=1 class="r" src="https://av.vimuser.org/x200-uboot.jpg" /><span class="f"><img src="https://av.vimuser.org/x200-uboot.jpg" /></span>

Canoeboot is a free/libre BIOS/UEFI replacement on x86 and ARM, providing
boot firmware that initialises the hardware in your computer, to then load an
operating system (e.g. Linux). It is specifically
a *[coreboot distribution](../docs/maintain/)*,
like how Debian is a Linux distribution. It provides an automated
build system to produce coreboot ROM images with a variety of payloads such as
GRUB or SeaBIOS, with regular well-tested releases to make coreboot as easy
to use as possible for non-technical users. From a project management perspective,
this works in *exactly* the same way as a Linux distro, providing a source-based
package manager (called cbmk) which patches sources and compiles coreboot images.
It makes use of [coreboot](https://www.coreboot.org/) for hardware initialisation,
and then a payload such as [SeaBIOS](https://www.seabios.org/SeaBIOS)
or [GRUB](https://www.gnu.org/software/grub/) to boot your operating
system; on ARM(chromebooks), we provide *U-Boot* (as a coreboot payload).
Experimental x86 U-Boot support is also available.

Summarised list of changes
--------------------

### Board support

RP2530 devices now supported in `pico-serprog`, e.g. Raspberry Pi Pico 2.

We previously only supported the RP2040 devices, e.g. original Pi Pico. We
now support both RP2040 and RP2530. Images are provided in the same tarball.

The documentation has been updated, to reflect this improvement. Canoeboot also
now imports `picotool` which is now used to compile the UF2 binaries for
installation, when building them from `pico-sdk`.

### Revision updates

In descending order from latest changes to earliest changes:

* Bumped SeaBIOS to revision 1602647f1 (7 November 2024). This brings in a
  single change from upstream, that prevents the boot menu from being hidden
  when `boot-menu-wait` is negative.
* Bumped GRUB to revision 6811f6f09 (26 November 2024). This brings in several
  important fixes from upstream, most notably TPM2 key support, though we
  don't yet actually enable it in the code as it's unused on all machines.
  Minor fixes have been observed, e.g. graphical glitches fixed under certain
  edge cases.
* Bumped flashprog to revision eb2c041 (14 Nov 2024). This brings in several
  important fixes from upstream.

### Feature changes

In descending order from latest changes to earliest changes:


* `./mk inject` function added, based on the Libreboot one, but the Canoeboot
  one only sets MAC addresses; it does not download/inject vendor files. It
  uses the same `setmac` command syntax. It's documented now on
  the [nvmutil](../docs/install/nvmutil.md) page.
* `util/nvmutil`: Show the same byte order, in the same grouping and
  display format, as `hexdump -C`, so that the output is more intuitive when
  the user runs e.g. `./nvm gbe.bin dump`
* `lib.sh`: Support `reinstall` on Fedora dnf. Use it by running
  the `./mk dependencies fedora re` command.
* `lib.sh`: Support `--reinstall` on Debian apt-get, when running
  the `./mk dependencies debian` command. You can
  use: `./mk dependencies debian --reinstall`
* `rom.sh`: Support setting `payload_grubsea="y"` on a board's `target.cfg`
  file when compiling, so that GRUB is the primary payload. This is not enabled
  on any boards, nor will it be, on release images, but the user can do it
  themselves when compiling from source.

### Configuration changes

In descending order from the latest changes to the earliest changes:

None of note; some other entries in this release log could be considered
configuration changes.

### Bug fixes

Several fixes were made to prevent build errors on the latest Debian Sid
and Arch, as of 6 January 2025. Fedora 41 was also tested, fixing
various issues.

The following bug fixes have been merged (in descending order from the latest
changes to the earliest changes):

* `lib.sh`: Python versions are detected using Python's own built-in tuple,
  instead of relying on the `--version` argument, because the tuple is more
  consistent across various distros, whereas some distros might modify the
  output of `--version` to include their own branding, which could have messed
  up the way our build system was using `awk`.
* `lib.sh`: Properly set up `python` in the `PATH` directorier, by creating
  a temporary link to the correct Python version, where detected. Python 3
  is the only python on most distros now, but we still occasionally encounter
  setups where the user has `python` as v2.x and has `python3` in PATH for
  Python v3.x; in this latter scenario, the build system corrects for it so
  that `python` is temporarily v3.x while images are being compiled. Several
  parts of the build system do use Python quite heavily, and it is *always*
  Python 3.
* `submodule/grub`: Use codeberg as the primary mirror, instead of the GNU
  mirror, because the GNU mirror is often slow. This is done specifically
  for the `gnulib` GRUB submodule.
* `util/nvmutil`: Describe what the program does, in help output. The lack
  of such info is considered a bug, because lack of documentation is a bug.
* `util/nvmutil`: Proper `PREFIX` and `DESTDIR` handling in the Makefile.
  The default prefix is now `/usr/local`, as is correct and righteous; it
  was previously `/usr`. All variables are now properly honoured,
  namely: `DESTDIR`, `PREFIX`, `INSTALL`, `CC` and `CFLAGS` - the user can
  now set custom variables, when compiling.
* Fixed several errors produced by `shellcheck` on the build system.
* Added `libuuid-devel` to Fedora dependencies.
* flashprog: Disable `-Werror` on builds, to prevent over-zealous compilers
  from yielding errors on mere warnings. This is a preventative bug fix,
  because no errors had yet been observed.
* Debian dependencies: Replaced `liblz4-tool` with `liblz4-dev`, for
  Debian Trixie/Sid. The same config still works for Debian Bookworm.
* `u-boot`: Imported a fix from upstream, that prevents a build error when
  compiling with Swig 4.3.0, because the calling convention changed on one
  of the functions that U-Boot's build system uses. Specifically,
  the `SWIG_AppendOutput` is used now. The way this fix works means that there
  are no behaviour changes, when compiling on an older machine with,
  say, Swig 4.1.0 as used in Debian Bookworm, which is what Canoeboot currently
  uses on this day, when compiling releases. We still want the build system
  to work on newer distros.
* Use `command -v` instead of `which`, where the latter was previously used
  in a few places on the build system. The `command -v` command is standard,
  whereas `which` isn't. This is to
  mitigate [which hunts](https://lwn.net/Articles/874049/).
* Added double quotes on several variables in the build system, to prevent
  globbing.
* Removed auto-confirm options on distro dependencies scripts. This is to
  prevent buggy distros from messing up the user's packages, because now it
  will show a confirmation dialog when the package manager sees a conflict;
  previously, the build system just said yes to everything, which reduced the
  amount of user interaction. The idea is to annoy the user, rather than
  break their machine, and this is only a mild annoyance.
* `trees`: Hack the `PATH` environmental variable to link GCC and GNAT, matching
  them so that both versions are the same. This mitigates an issue on Debian
  Trixie/Sid as of January 2025, whereby `gcc` is `gcc-14` while `gnat`
  is `gnat-13` and also installs `gcc-13`; if this ids what the user has, GCC
  is set to GCC 13 in `PATH`. The user can install `gnat-14` if they wish,
  and the GNAT version is auto-matched in `PATH` instead, matching GCC. This
  is only done when compiling the coreboot crossgcc toolchain, because GNAT
  is required there and must match the GCC version.
* `dependencies/debian`: Changed `python3-distutils`
  to `python3-distutils-extra`, so that dependencies can be installed on
  Debian Trixie/Sid; the same config still works on Debian Bookworm.
* Added SPDX headers to various configuration files.
* `git.sh`: Re-initialise the `livepull` variable per-project and per-tree,
  so that previous runs of it do not unpredictably affect the next, when
  cloning upstream Git repositories.
* `lib.sh`: Safer exit when running `./mk dependencies` (don't rely on the
  exit statuses of chained commands being zero).
* GRUB: Background images are loaded again from CBFS, instead of memdisk,
  because otherwise GRUB prefers the memdisk one and adding one in CBFS
  didn't change the background image, when users attempted this.

### General code cleanup

In descending order from the latest changes to the earliest changes:

* `util/nvmutil`: Make the GbE checksum of `0xBABA` a define, for clarity.
* `lib.sh`: cleaned up a few if statements
* `util/nvmutil`: Tidied up several `pledge` calls on OpenBSD (code quality
  improvement).

Git log
-------

This log is relative to Canoeboot 20241207:

```
* 514f61d6ba pico-sdk: Remove old, unnecessary patch 
* 465b18eff3 remove errant symlink 
* ec7e8d3a8f Bump coreboot/next to 2f1e4e5e85, 31 December 2024 
* 8829539531 rom.sh: don't run mkpicotool on dry builds 
* 62d655b8dd pico-sdk: Import picotool as a dependency 
* adf1a2e1a4 lib.sh: Much safer python version check 
* 1b1dae36d2 set up python in PATH, ensuring that it is python3 
* ac6b7c9e3a add libx86 to arch dependencies 
* 24aa70869e add less to arch dependencies 
* d731b07aa7 lib.sh: Set python after dependencies 
* d57303e080 update my copyright years on modified scripts 
* bf5979f0b2 lib.sh: Fix unescaped quotes in chkvars() 
* 9baf6a72a7 Revert "fix more unescaped quotes in eval" 
* c1dd3da298 fix more unescaped quotes in eval 
* caa18bdcb3 fix ./mk dependencies build issue 
* 43ab374ec7 rom.sh: Remove errant GRUB modules check 
* 2916aee7ab submodule/grub: use codeberg for 1st gnulib mirror 
* 8e30ea27ef util/nvmutil: Update AUTHORS and COPYING files 
* 376b1301f5 util/nvmutil: Describe nvmutil in help output 
* da32fe1928 util/nvmutil: Remove the correct binary on uninstall 
* 54809f06bf util/spkmodem-recv: More correct Makefile 
* ac6f3884ad util/nvmutil: Honour the INSTALL variable 
* e993212146 util/nvmutil: Don't clean when doing uninstall 
* 21c87a6fbf util/nvmutil: Proper DESTDIR/PREFIX handling 
* 5ed1114443 util/nvmutil: Set CC and CFLAGS only if unset 
* 1e494f4530 util/nvmutil: Capitalise BABA 
* ba71891687 util/nvmutil: Add uninstall to Makefile 
* 1310c9a58c util/nvmutil: Add distclean to Makefile 
* faa78f53c2 util/nvmutil: Make the GbE checksum a define 
* 227c396ca2 util/nvmutil: nicer hexdump display 
* 07e8b9f35e util/nvmutil: show the correct hexdump order 
* 76d87782a8 lib.sh mktarball: cleaner if statement 
* 3d1fea0977 fix lbmk shellcheck errors 
* 4862f6a348 lib.sh and rom.sh: update my header 
* 8bdfd31498 rom.sh: handle tarballs in mkcoreboottar 
* 13194ec94a util/nvmutil: Obey the 79-character per line limit 
* ce0fdf183d util/nvmutil: Tidy up copyright header 
* f3feac0728 util/nvmutil: Fix another straggler 
* 145ff612c1 util/nvmutil: Tidy up pledge calls 
* 4a83659885 compile rmodtool alongside cbfstool 
* a22f272dfe fedora41/dependencies: add libuuid-devel 
* de26e984df add uuid-devel to fedora41 dependencies 
* 4ce13dab7a support ./mk dependencies fedora reinstall 
* 9d60470a97 fix missing semicolon in grub nvme patch 
* 2d860d6f66 bump seabios to rev 1602647f1 (7 November 2024) 
* 5d82a911b9 Bump GRUB revision to 6811f6f09 (26 November 2024) 
* b75d32be72 flashprog: Disable -Werror 
* 22206cadeb bump flashprog to revision eb2c041 (14 Nov 2024) 
* 1ebdb20d97 replace liblz4-tool with lz4 and liblz4-dev 
* 012f530656 lib.sh dependencies: support --reinstall argument 
* 33206cae60 move xbmkpath to XBMK_CACHE/ 
* 4a4f7abf0e Revert "Remove legacy update/vendor commands" 
* 71bccad345 Fix U-Boot build issue with Swig 4.3.0 
* 2608bea771 use command -v instead of which 
* 3dc3a557db trees: remove unnecessary subshell 
* dbf8bef5ff trees: only symlink host gcc/gnat to build xgcc 
* cd93e1b615 trees: correction on check_gnu_path 
* 57fafb0b73 trees: match gcc/gnat versions both ways 
* f167d4aeb9 Merge path.sh into script/trees 
* 444154c002 path.sh: Further cleanup 
* c25ea91783 path.sh: More thorough gcc/gnat version check 
* 12221b2828 path.sh: minor cleanup 
* 02b1a45c6b path.sh: remove unnecessary shebang 
* 5a5a7c37f5 Fix globbing issue in cbmk 
* 69c5f8911d remove auto-confirm on distro dependencies 
* 7ee6ec0ce9 Mitigate Debian Trixie/Sid GCC/GNAT version mismatch 
* f1d9ecc468 rom.sh: Name pico directory serprog_pico 
* bb6c31aa16 add 2024 to Riku's copyright header on rom.sh 
* 3d738af507 pico-sdk: update to 2.1.0 
* fa6ed8816e pico-serprog: enable building for multiple pico chips 
* 31a0ea3537 add spdx headers to various config files 
* dd6f914186 git.sh: don't initialise livepull globally 
* 417accd9e0 lib.sh: Support copying files locally 
* 7b8bda9977 lib.sh: Safer exit from ./mk dependencies 
* 63eb43ec39 dependencies/void: add missing escape character 
* daefe4f895 rom.sh: support grub-first setups 
* 73920cb0a1 rom.sh: insert grub background in cbfs not memdisk 
* 5ebeb65cbf add tarballs and signatures to gitignore 
* 5053354644 fix another very stupid mistake 
* df509aac63 fix the stupidest bug ever 
```

This is precisely 80 changes, since Canoeboot 20241207.
