% Canoeboot 20240510 released!
% Leah Rowe in Canoe Leah Mode™
% 10 May 2024

Canoeboot is a *special fork* of [Libreboot](https://libreboot.org/), providing
a de-blobbed configuration on *fewer motherboards*; Libreboot supports more
hardware, and much newer hardware. More information can be found on Canoeboot's
[about](../about.html) page and by reading Libreboot's [Binary Blob Reduction Policy](https://libreboot.org/news/policy.html).

**Do not use the Canoeboot 20240510 release, because it had problems with it.
Please use the [Canoeboot 20240612 release](canoeboot20240612.md) instead.**

Free software BIOS/UEFI
----------------------

Canoeboot is a [free/libre](https://writefreesoftware.org/) BIOS/UEFI replacement
on x86 and ARM, providing
boot firmware that initialises the hardware in your computer, to then load an
operating system (e.g. Linux). It is specifically
a *[coreboot distribution](../docs/maintain/)*,
in the same way that Debian is a Linux distribution. It provides an automated
build system to produce coreboot ROM images with a variety of payloads such as
GRUB or SeaBIOS, with regular well-tested releases to make coreboot as easy
to use as possible for non-technical users. From a project management perspective,
this works in *exactly* the same way as a Linux distro, providing the same type
of infrastructure, but for your boot firmware instead of your operating system.
It makes use of [coreboot](https://www.coreboot.org/) for hardware initialisation,
and then a payload such as [SeaBIOS](https://www.seabios.org/SeaBIOS)
or [GRUB](https://www.gnu.org/software/grub/) to boot your operating
system; on ARM(chromebooks), we provide *U-Boot* (as a coreboot payload).

Canoeboot provides many additional benefits such as fast boot speeds, greater
security and greater customisation, but the *primary* benefit
is [software freedom](https://writefreesoftware.org/learn). With use of GRUB
in the flash, you can make use of many advanced features such as the ability
to [boot from an encrypted /boot partition](../docs/linux/)
and [verify kernel GPG signature at boot time](../docs/linux/grub_hardening.md).

If you're fed up of the control that proprietary UEFI vendors have over you,
then Canoeboot is *for you*. Although many would agree that it is a major step
forward for most users, it's actually a very old idea. Old is often better. It used to
be that computers were much more open for learning, and tinkering. Canoeboot
implements this old idea in spirit and in practise, helping you wrest back control.

Unlike the hardware vendors, Canoeboot does not see *you* as a *security threat*;
we regard the ability to use, study, modify and redistribute software freely to
be a human right that everyone *must* have, and the same is true of hardware.
Your computer is *your property* to use as you wish. Free Software protects you,
by ensuring that you always have control of the machine.

*This* new release, Canoeboot 20240510, released today 10 May 2024, is
a new *stable* release of Canoeboot. The *previous* release was
Canoeboot 20240504, just *6 days ago*. This is a minor release, with minimal
changes; if you already installed Canoeboot 20240504, you probably don't need
to update, but the *SeaBIOS* revision was updated, and has some fixes.

Changes in this release
----------------------

*Extensive* changes have been made to the documentation and website!

Very large and sweeping changes.

ALSO:

### Build system changes

The following improvements have been made, most of them not affecting the
final build (the actual code that goes in-flash):

* bump seabios (payload) to the latest upstream revision as of today
* main `build` script: exit (with non-zero status) if not running it from the
  main cbmk work directory (the design of cbmk makes this mandatory)
* merge `script/build/serprog` into `script/build/roms` - now you
  run e.g. `./build roms serprog rp2040` instead of `./build serprog rp2040`
* merge `include/err.sh` into `include/option.sh`
* build/roms: more code cleanup in general
* build/roms: split up `main()` into smaller functions
* build/roms: allow status search by mismatch (e.g. list all targets that
  are *not* stable, instead of only being able to list ones that are)

Regarding SeaBIOS, the following upstream changes have been merged into
this Canoeboot release:

```
    * e5f2e4c6 pciinit: don't misalign large BARs
    * 731c88d5 stdvgaio: Only read/write one color palette entry at a time
    * c5a361c0 stdvga: Add stdvga_set_vertical_size() helper function
    * 22c91412 stdvga: Rename stdvga_get_vde() to stdvga_get_vertical_size()
    * 549463db stdvga: Rename stdvga_set_scan_lines() to stdvga_set_character_height()
    * c67914ac stdvga: Rename stdvga_set_text_block_specifier() to stdvga_set_font_location()
    * aa94925d stdvga: Rework stdvga palette index paging interface functions
    * 8de51a5a stdvga: Rename stdvga_toggle_intensity() to stdvga_set_palette_blinking()
    * 96c7781f stdvga: Add comments to interface functions in stdvga.c
    * 2996819f stdvga: Rename CGA palette functions
    * 91368088 stdvgamodes: Improve naming of dac palette tables
    * 70f43981 stdvgamodes: No need to store pelmask in vga_modes[]
    * 1588fd14 vgasrc: Rename vgahw_get_linesize() to vgahw_minimum_linelength()
    * d73e18bb vgasrc: Use curmode_g instead of vmode_g when mode is the current video mode
    * 192e23b7 vbe: implement function 09h (get/set palette data)
    * 3722c21d vgasrc: round up save/restore size
    * 5d87ff25 vbe: Add VBE 2.0+ OemData field to struct vbe_info
    * 163fd9f0 fix smbios blob length overflow
    * 82faf1d5 Add LBA 64bit support for reads beyond 2TB.
    * 3f082f38 Add AHCI Power ON + ICC_ACTIVE into port setup code
    * 3ae88886 esp-scsi: terminate DMA transfer when ESP data transfer completes
    * a6ed6b70 limit address space used for pci devices.
```

And that's all. If you installed Canoeboot 20240504, you probably don't need
to update to today's release. If you *didn't* install 20240504, then you may
aswell update to today's release (Canoeboot 20240510).

Hardware supported in this release
--------------------------

This release supports the following hardware:

### Servers (AMD, x86)

-   [ASUS KFSN4-DRE motherboard](../docs/hardware/kfsn4-dre.md)
-   [ASUS KGPE-D16 motherboard](../docs/hardware/kgpe-d16.md)

### Desktops (AMD, Intel, x86)

-   [Acer G43T-AM3](../docs/hardware/acer_g43t-am3.md)
-   [Apple iMac 5,2](../docs/hardware/imac52.md)
-   [ASUS KCMA-D8 motherboard](../docs/hardware/kcma-d8.md)
-   [Gigabyte GA-G41M-ES2L motherboard](../docs/hardware/ga-g41m-es2l.md)
-   [Intel D510MO and D410PT motherboards](../docs/hardware/d510mo.md)
-   [Intel D945GCLF](../docs/hardware/d945gclf.md)

### Laptops (Intel, x86)

-   [Apple MacBook1,1 and MacBook2,1](../docs/hardware/macbook21.md)
-   [Dell Latitude E6400, E6400 XFR and E6400 ATG, with Intel
    GPU](../docs/hardware/e6400.md)
-   [Lenovo ThinkPad R400](../docs/hardware/r400.md)
-   [Lenovo ThinkPad R500](../docs/hardware/r500.md)
-   [Lenovo ThinkPad T400 / T400S](../docs/hardware/t400.md)
-   [Lenovo ThinkPad T500](../docs/hardware/t500.md)
-   [Lenovo ThinkPad W500](../docs/hardware/t500.md)
-   [Lenovo ThinkPad X200 / X200S / X200 Tablet](../docs/hardware/x200.md)
-   Lenovo ThinkPad X301
-   Lenovo ThinkPad X60 / X60S / X60 Tablet

### Laptops (ARM, with U-Boot payload)

-   [ASUS Chromebook Flip C101 (gru-bob)](../docs/install/chromebooks.md)
-   [Samsung Chromebook Plus (v1) (gru-kevin)](../docs/install/chromebooks.md)

Again, very minor release. I wasn't fully happy with the last one, specifically
the last Canoeboot release, so I decided to another quick one.

That is all.

Errata
-----

See: <https://codeberg.org/libreboot/lbmk/issues/216>

This bug has been *fixed* in lbmk.git, and the fix will be included in
the next release, but it wasn't caught in the 20240504 release. The same
fix has been applied to Canoeboot's build system, cbmk.

It is almost certainly guaranteed that *no* Canoeboot users were ever
affected by this, but extreme measures have been taken to ensure that it
is *entirely* guaranteed from now on. Read on to know more:

The bug is quite serious, and it was previously decided that documentation
should be written warning about it (in docs/install/). The bug was *only*
triggered on Intel Sandybridge hardware (e.g. ThinkPad X220) and was never
reported on other boards, but there's no way to fully know; what is known
is that the offending patch that caused the bug has been *removed*; namely,
xHCI GRUB patches, which are now only provided on Haswell and Broadwell
hardware (where the bug has not occured) in *Libreboot*; in Canoeboot, the
GRUB tree with xHCI support is provided, but not currently used on any
motherboards in Canoeboot. **Therefore, we know that the bug will no longer occur.**

The next release will exclude xHCI support on machines that don't need it, which
is *every machine that Canoeboot supports* (as of Canoeboot 20240504/20240510),
and a mitigation is in place that makes SeaBIOS the primary payload, to prevent
effective bricks in the future; the bug was in GRUB, but if SeaBIOS is the
first payload then the machine remains bootable even if a similar bug occurs.

It is now the default behaviour, in the next release, that certain images
contain a bootorder file in CBFS, making SeaBIOS try GRUB first, but you can
still press ESC to access the SeaBIOS boot menu if you want to directly boot
an OS from that. This, and the other change mentioned above, will guarantee
stability. GRUB is *no longer* the primary payload, on any motherboard.

However, it was later decided to put this release in the `testing`
directory instead; it was initially designated as a stable release.

All ROM images for the 20240504/20240510 releases have been *removed* from
rsync, but the source tarball remains in place.

For now, you are advised to use the November 2023 release, or build from
cbmk.git at revision `4f6fbfde81f5176e5892d1c00627f8f680fd3780` (which is known
to be reliable and is the current revision as of this time of writing) - or,
alternatively, you are advised to use the next release after 20240510.

A new [audit](audit1.md) has been conducted, marked complete as of 9 June 2024,
fixing this and many issues; a new *true* stable release will be made available
some time in June 2024.
