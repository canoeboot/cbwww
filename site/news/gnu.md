% Should Canoeboot become GNU Canoeboot?
% Leah Rowe in GNU Leah Mode™
% 12 May 2024

**UPDATE ON 12 JUNE 2024: Please do not use Canoeboot 20240504 or 20240510
because there were problems with these releases. Use
the [Canoeboot 20240612 release](canoeboot20240612.md) instead, which contains
a series of bug fixes that correct the issues from May 2024 releases. More
information is available in the Canoeboot 20240612 announcement.**

And now, without further ado:

Original article
================

Should it? That is the question I emailed to GNU's evaluation team today.

In it, I make a strong case in support of membership, and I encourage members
of the public to also voice their opinions about this topic. Therefore, I have
publicised this link in several places:

What is Canoeboot?
==================

The *Canoeboot* project provides
[free](https://writefreesoftware.org/learn) (*libre*) boot
firmware based on coreboot, replacing proprietary BIOS/UEFI firmware
on [specific Intel/AMD x86 and ARM based motherboards](docs/hardware/),
including laptop and desktop computers. It initialises the hardware (e.g. memory
controller, CPU, peripherals) and starts a bootloader for your operating
system. [GNU+Linux](../docs/gnulinux/) and [BSD](../docs/bsd/) are well-supported. Help is
available via [\#canoeboot](https://web.libera.chat/#canoeboot)
on [Libera](https://libera.chat/) IRC.

Canoeboot is a *special fork* of LIbreboot, maintained in parallel to it.
Canoeboot adheres to the GNU Free System Distribution Guidelines, ensuring
that everything in the flash is *free software*. Canoeboot *only* supports a
very limited subset of hardware from coreboot (which it uses for initialisation)
due to this policy, but it's provided for the purists who only want free
software.
The right to study, share, modify and use software freely without restrictions
is a fundamental right that everyone *must* have.

The benefit of Canoeboot is that it provides a completely
automated build process and installation procedure, with well-tested builds
released on a regular basis that the user can simply install, with minimal
fuss. You can think of it as a *coreboot distro*. Coreboot provides snapshot
source code archives every few months, but that's it. Canoeboot gives
you the ROM images pre-compiled, with source code, and with payloads
already pre-configured. In other words: Canoeboot makes coreboot
extremely easy to use.

More info about Canoeboot's design can be found in the [build system
documentation](../docs/maintain/).

Thoughts welcome!
=================

I asked this question in various places, where you can comment:

Thread on FSF LibrePlanet mailing list:
<https://lists.gnu.org/archive/html/libreplanet-discuss/2024-05/msg00001.html>

Thread on my Mastodon account:
<https://mas.to/@libreleah/112428793049670713>

Thread on Trisquel forums (FSF endorsed GNU+Linux distro, and de-facto stomping
ground for many debates within the FSF community):

<https://trisquel.info/en/forum/should-gnu-boot-become-gnu-canoeboot>

And reddit: <https://www.reddit.com/r/linux/comments/1cqe924/should_canoeboot_become_gnu_canoeboot/>

Email sent to GNU Eval
======================

Here is the email that I sent (replies to it will **not** be published, only
my original email below will be):

```
Message-ID: <864f2dd0-3af8-4c01-8d9b-d4236b43b335@minifree.org>
Date: Sun, 12 May 2024 15:50:42 +0100
MIME-Version: 1.0
User-Agent: Mozilla Thunderbird
Content-Language: en-US
From: Leah Rowe <info@minifree.org>
Autocrypt: addr=info@minifree.org; keydata=
 xsFNBGWN15sBEADECGPEe37tdU3xe7OshKU19xVOPuJRMveCO5DHfv/lsZMXLWXwMMpbG+2x
 SMQZcdZc0HCUq6TQE9fU0rA3kcFz0miMOuB2WJbYy9guvg9pAjLa0LUyb2T/HPDDy0ifYtqr
 OzwETwWRiWQcTHjJ0knwNReaEterpPki1MbK79EwSuQBIgq9lQ611qLn5SmE7sBRB5kze7q3
 KdTvY/CTfvOpVizgRF8kqqG4r4XkI0dTyrvC3i3Eub3F3YPWNjN06rECG6wO+TPzRo7em+0C
 dPYDgtqq4Srf050KNZsVt10Plty5VpJm2GfoXFh6SZBO1zSbBpTGU+7vBsR731ye2ouQdcIs
 06Qi4wHmJ71liqJwxZ0ju2F5edC7jDzdk4jAIaCiSiU+iGg28RsxoUdLkJl5Q4yW507Gr0ps
 HIBJBAWJo1i75qKVhrmN25xjJLv0MjgaR7RgT1T7uuX1KPuo8NbHbRlkIv7987NeJzgbUzzp
 ka2MJjTEd1ova9kPyICVmKCfBnT+bO3vfJAuQXRlf3qjXSLsxCD7Jmu07if0jXFIvjy/nC4H
 0QPlwd/sVS7Svfn4rEGEnulrtBvVdOr6I+LmDedbSsSlYNlqagdyGsdKZfWSGxhjfz4oAkVy
 +y39s1qAnM5191m+u72dmnQPtxI8lEH/G+j+hK8NxDvV5ri3owARAQABzR1MZWFoIFJvd2Ug
 PGluZm9AbWluaWZyZWUub3JnPsLBlAQTAQoAPhYhBIux99KM92ltv09xklxlQGfTg7H/BQJl
 jdebAhsDBQkJZgGABQsJCAcCBhUKCQgLAgQWAgMBAh4BAheAAAoJEFxlQGfTg7H/PKoQAIB8
 z2Rg+R0417YRTBXvbVG5kPpKOO3DWUaQCJx6uypBUpgw2giKDsDz59c2vNaADs7Zh5xQ+2bz
 B+jkCjVSuzguApT3gxTnICvLeM72d5ZEF6Q8/YC6s9IiIHssCujbxtNN5yDU/Kn9Qd3gb+Bn
 9t/ZYT+L/SGLU3Zerq57lSt8ixU7JOvAolgqRzaPwTFvi1GPZbE5Gynj9riTxZc3KLFROWYN
 iiO33T+X17TepTUaubkoA3DgWcQ6tw2dsJ8MT0DtZH8KlU/ufq0NBfFIw79uCQ+m/GbiW9Sm
 KtppayLUJyJndlf5fQ/NaNJw9RS+yF5ellGKWWAwh+Drv0PITzJ1dLeWOF5degtyn9+HaMSM
 RIs5G6m69UxZmLJ9TQF1Lt1u0l1EH7yHIAf69MX4nlcDIx6moIJgo2UJ9u2D7odDUKlPG8X4
 lbc5cvGx9l/Hy6WF8dOYUiU1avU65uhggaNXGXC6JbDiChCeuWTnzKXqlvae8rU9jSpDBSQF
 lOOgvi3NifDLM7xFByDtFabnJHniNO1B6kS0V0nv6sJlRuB584kQUQ+PaRUj8QvXjsLvYhxY
 Tw2G7jzQkXEOBZq3jenVzAg5ejOB7mTNoZOYOWoFY7XDRUGOmGDvH2qQGVNhedhSBJilAiu6
 K0NyjkaFAGufvgXDjLImPav/kWdbTDvNzsFNBGWN15sBEADj4Dhn29Z0LVU+B1Wc8U0wdV7N
 bDIjbhEvJ0Yc+FTNorQq7avY9jTkGtcnKPUti6cJxuQOZPxaIzP1mMK9BlsGbB8bTJ7oqpsI
 XuvGKOOZQMb7i+qEIfJw6ZAXSwuKq4xBciJU52WdX8OaSWJ6KZX74yrE1SXW+7yj3ENZNWuT
 N2kAPF595XYpZwTAaJRy/ojfORu8WFXvo5osZJI1TlrJeDFecBntPkCgvI1VPTF3fUU3lGZc
 8rVascaGaJ6tBd5mTx/RrRyJrrJMEd1dca0MHV4WIDbNeINpbEhS2SbqOj/9q9z8tfmleqXV
 jb+gKjSDpD8Nsxv9+2gq29tevMLqZ5R3NA4jj4rOnyIuq+YFtkZuBuEc04UX3ApixdAQ0jIN
 hIBfT8YcMC+wwTvl4jG4Rhzrc4jOX8igOe9wkstbl7JxGJU28x4htskAyM2CMxPaUrorm64c
 U2S0UoJGrvLfjItGud8dyM+RgtaO5wTFNO2A0dnq2/thIIgoEaiQfKMq2ilISsnf6x8as0FV
 3c7mGO8pDJlRwjoNg/89CMhxVbgO+5JQ+JHLfoMA++R+QaYy1ZLY51h5Ax6OtVWpZh67EuVD
 Lx/5EFiyM25UTpqvH7t0ae2oLbAwQtIv1fRLhHP4aVSsP793of92/1lpybbpRD7/7ruVQ64d
 +/N49db6mQARAQABwsF8BBgBCgAmFiEEi7H30oz3aW2/T3GSXGVAZ9ODsf8FAmWN15sCGwwF
 CQlmAYAACgkQXGVAZ9ODsf8EqQ/8CjAQTza1pvd/GmKYHtldSA1odPB7AQkB+j4oyu5gDqtM
 /fFRv3uGVYLcyZwC3XF69KL+NpcUYG22RQWokt+z3OiVYfP5LyKjXe2cPMS2cmyXBHEcCP8Q
 SffNQNoC2VYzaVXH4P6cBVmkDG3yPtQ2cH1Al6jhMS4Fa0TCe6kgA7qROSoapdZuwbxEE7Fe
 aYZIXQUBLpe2C6SexljD65DLhbDE5p4N56VbncO6IAqr9JQSlEXBgvgXGhXqfOmZkmCjXJU7
 Sgy8sIyF4b1uEOkcWxUUfvfrO1yrcWyxvTlZdPCJy802B7UVFPWTbKwFoyIq5Lr8wk2npzAm
 Dy/hKZlIV72Vk0eIcrTCfpbj0GehrIIYcpW7Z2IUtETNkyuQs+OScIdrP3PItajNwktQJrhz
 +UGbF9MuT+CHuruhw5KzymkzcnEzCNaYvY4eG8IXP6VpX1GCs9eUvCWEnjP0OkmhQniWvS3v
 AB7DfZSm0NAoDX0ZvNzzIRbiM5uhYqDUSIsmOwUiJLjveLysmIRaAc/K/Euml0obCUJfanD7
 KSs7SZYp24ZTDNsvsWbsk1y8QvAGkZBfuykfRCMxsmGRyN2hS6H/ftttCUrj8Qn1/hJDBegE
 UJgYDItHpE7KJeBHMFNUB2sTHPbzx+a5WPYxeYJevAeTwAg0/nRobXRzER7BLIU=
To: gnueval@gnu.org
Cc: ksiewicz@fsf.org, zoe@fsf.org, Mike Gerwitz <mtg@gnu.org>,
 simon@josefsson.org, christian@grothoff.org, gnu-advisory@gnu.org,
 rms@gnu.org, bob@proulx.com
Subject: Should Canoeboot become GNU Canoeboot?
Content-Type: multipart/signed; micalg=pgp-sha256;
 protocol="application/pgp-signature";
 boundary="------------QXCa2wGHkDkyD669GgjjqNUT"

This is an OpenPGP/MIME signed message (RFC 4880 and 3156)
--------------QXCa2wGHkDkyD669GgjjqNUT
Content-Type: multipart/mixed; boundary="------------K2qUk5UHVXzKPCuKPN8riXWW";
 protected-headers="v1"
From: Leah Rowe <info@minifree.org>
To: gnueval@gnu.org
Cc: ksiewicz@fsf.org, zoe@fsf.org, Mike Gerwitz <mtg@gnu.org>,
 simon@josefsson.org, christian@grothoff.org, gnu-advisory@gnu.org,
 rms@gnu.org, bob@proulx.com
Message-ID: <864f2dd0-3af8-4c01-8d9b-d4236b43b335@minifree.org>
Subject: Should Canoeboot become GNU Canoeboot?

--------------K2qUk5UHVXzKPCuKPN8riXWW
Content-Type: multipart/mixed; boundary="------------qXtx1QnbZel60GYrRxArVU6C"

--------------qXtx1QnbZel60GYrRxArVU6C
Content-Type: multipart/alternative;
 boundary="------------0qkEOS0KaVvTlm4TzFss1Nsy"

--------------0qkEOS0KaVvTlm4TzFss1Nsy
Content-Type: text/plain; charset=UTF-8; format=flowed
Content-Transfer-Encoding: base64

SGkNCg0KVGhpcyBpcyBzZW50IHRvIEdOVSBFdmFsIHRlYW0sIGJ1dCBDQydkIHRvIG90aGVy
cy4gTXkgbWFpbiByZWNpcGllbnQgaXMgdGhlIEdOVSBFdmFsIHRlYW0uDQoNCkkgd2FudCBH
TlUgQ2Fub2Vib290LiBUaGlzIGlzIG15IG9mZmljaWFsIGNvbnRhY3Qgd2l0aCBHTlUsIGFz
IHRoZSBsZWFkIGRldmVsb3BlciBhbmQgZm91bmRlciBvZiB0aGUgQ2Fub2Vib290IHByb2pl
Y3QsIGEgZnVsbHkgZnJlZSBjb3JlYm9vdCBkaXN0cm8gYmFzZWQgb24gTGlicmVib290Lg0K
DQpDYW5vZWJvb3QgaGFzIHJlY2VudGx5IHJlbW92ZWQgYWxsIG9wcG9zaXRpb24gdG8gdGhl
IEZTRiBhbmQgZGVjaWRlZCB0byBzdGF1bmNobHkgcHJvbW90ZSBpdCBpbnN0ZWFkLCBpbiBh
ZGRpdGlvbiB0byBGU0RHLiBUaGlzIGlzIHBhcnQgb2YgYSBnZW5lcmFsIGRlc2lyZSBJJ3Zl
IGhhZCBzaW5jZSB0aGUgc3RhcnQgb2YgdGhlIHllYXIsIHRvIHNlZWsgcmVjb25jaWxsaWF0
aW9uIHdpdGggdGhlIEZTRiBhbmQgR05VIHByb2plY3QsIGFmdGVyIHRoZSBkcmFtYSB0aGF0
IGVuc3VlZCBmaXJzdCB3aXRoIGxpYnJlYm9vdC5vcmcgdnMgbGlicmVib290LmF0LCBhbmQg
dGhlbiBsaWJyZWJvb3Qub3JnIHZzIEdOVSBCb290Lg0KDQpUaGlzIGNoYW5nZSBpcyBwZXJt
YW5lbnQsIHdoZXRoZXIgR05VIGFjY2VwdHMgbXkgcHJvcG9zYWwgdG9kYXk7IGV2ZW4gaWYg
Q2Fub2Vib290IGRvZXMgbm90IGJlY29tZSBHTlUgQ2Fub2Vib290LCBpdCB3aWxsIGNvbnRp
bnVlIHRvIG9wZXJhdGUgYXMgaXQgZG9lcyBub3cuIEkgcmVjZW50bHkgZGlkIGEgcmVsZWFz
ZSB3aGljaCBpcyBzdGF1bmNobHkgcHJvLUZTRi4gSSd2ZSBkb25lIHRoaXMsIHByZWNpc2Vs
eSBiZWNhdXNlIEdOVSBCb290IGlzIG5vIGxvbmdlciBjb21wZXRpdGlvbiB0byBDYW5vZWJv
b3QgaW4gYW55IHdheTsgR05VIEJvb3Qgc2VlbXMgdG8gaGF2ZSBzdGFsbGVkLCBzbyBDYW5v
ZWJvb3QgaGFzLCB3aXRoIHRoaXMgbW92ZWQsIGVmZmVjdGl2ZWx5IHJlcGxhY2VkIGl0LiBJ
IGRvbid0IHNheSB0aGlzIGFzIGFuIGF0dGFjaywgYnV0IGl0IGlzIGEgZmFjdCB0aGF0IEdO
VSBCb290IGhhcywgYXMgSSB3cml0ZSB0aGlzLCBub3Qgc3VibWl0dGVkIGFueXRoaW5nIHRv
IHRoZWlyIG1haW4gYnJhbmNoIGluIG92ZXIgNCBtb250aHMuIGl0J3Mgbm93IGEgZGVhZCBw
cm9qZWN0LCBhbmQgQ2Fub2Vib290IGlzIHRha2luZyBvdmVyLg0KDQpJIGtub3cgR05VIEJv
b3QgaXMgYWxyZWFkeSBhIHRoaW5nLiBJIGVudmlzaW9uIENhbm9lYm9vdCByZXBsYWNpbmcg
aXQuIE5vdywgYW5zd2VycyB0byBxdWVzdGlvbnMgZnJvbSBnbnVldmFsIGZvcm06DQoNCiog
R2VuZXJhbCBJbmZvcm1hdGlvbg0KKiogRG8geW91IGFncmVlIHRvIGZvbGxvdyBHTlUgcG9s
aWNpZXM/DQogICAgSWYgeW91ciBwcm9ncmFtIGlzIGFjY2VwdGVkIHRvIGJlIHBhcnQgb2Yg
dGhlIEdOVSBzeXN0ZW0sIGl0IG1lYW5zDQogICAgdGhhdCB5b3UgYmVjb21lIGEgR05VIG1h
aW50YWluZXIsIHdoaWNoIGluIHR1cm4gbWVhbnMgdGhhdCB5b3Ugd2lsbA0KICAgIG5lZWQg
dG8gZm9sbG93IEdOVSBwb2xpY2llcyBpbiByZWdhcmRzIHRvIHRoYXQgR05VIHByb2dyYW0u
DQogICAgKFN1bW1hcml6ZWQgYWJvdmUsIHNlZSBtYWludGFpbmVycyBkb2N1bWVudCBmb3Ig
ZnVsbCBkZXNjcmlwdGlvbnMuKQ0KDQpZZXMuIENhbm9lYm9vdCBhbHJlYWR5IGNvbXBsaWVz
IGZ1bGx5IHdpdGggdGhlIEdOVSBGcmVlIFN5c3RlbSBEaXN0cmlidXRpb24gR3VpZGVsaW5l
cy4gVGhlcmUgbWF5IGJlIGEgZmV3IHN0cmFnZ2xlcnMgbGVmdCBvdmVyIGZyb20gd2hlbiBp
dCBmb3JrZWQgZnJvbSBMaWJyZWJvb3QsIGJ1dCB0aGVzZSB3aWxsIHN1cmVseSBiZSBmb3Vu
ZCBkdXJpbmcgcmV2aWV3Lg0KDQpJJ3ZlIGFscmVhZHkgZG9uZSBleHRlbnNpdmUgYXVkaXRp
bmcgbXlzZWxmLCBhcyBoYXMgQ3JhaWcgVG9waGFtIGluIGhpcyBjYXBhY2l0eSBhcyBMaWNl
bnNpbmcgYW5kIENvbXBsaWFuY2Ugb2ZmaWNlciBhdCB0aGUgRlNGLiAoQ2Fub2Vib290IDIw
MjMgcmVsZWFzZXMgd2VyZSBhdWRpdGVkKQ0KDQoqKiBQYWNrYWdlIG5hbWUgYW5kIHZlcnNp
b246DQoNCkNhbm9lYm9vdCAyMDI0MDUxMA0KDQoqKiBBdXRob3IgRnVsbCBOYW1lIDxFbWFp
bD46DQoNCkxlYWggUm93ZTxpbmZvQG1pbmlmcmVlLm9yZz4NCg0KKiogVVJMIHRvIHBhY2th
Z2UgaG9tZSBwYWdlIChpZiBhbnkpOg0KDQpodHRwczovL2Nhbm9lYm9vdC5vcmcvDQoNCioq
IFVSTCB0byBzb3VyY2UgdGFyYmFsbDoNCiAgICAgUGxlYXNlIG1ha2UgYSByZWxlYXNlIHRh
cmJhbGwgZm9yIHB1cnBvc2VzIG9mIGV2YWx1YXRpb24sIHdoZXRoZXINCiAgICAgb3Igbm90
IHlvdSBwdWJsaWNseSByZWxlYXNlIGl0LiAgSWYgeW91IGRvbid0IGhhdmUNCiAgICAgYW55
d2hlcmUgdG8gdXBsb2FkIGl0LCBzZW5kIGl0IGFzIGFuIGF0dGFjaG1lbnQuDQoNCmh0dHBz
Oi8vd3d3Lm1pcnJvcnNlcnZpY2Uub3JnL3NpdGVzL2xpYnJlYm9vdC5vcmcvcmVsZWFzZS9j
YW5vZWJvb3QvMjAyNDA1MTAvY2Fub2Vib290LTIwMjQwNTEwX3NyYy50YXIueHoNCg0KTk9U
RTogU2V2ZXJhbCBjaGFuZ2VzIGhhdmUgYmVlbiBtYWRlIHNpbmNlIHRoYXQgcmVsZWFzZS4g
Q2hlY2sgdGhlIGdpdCBsb2cgZm9yIGNid3d3LmdpdCBhbmQgY2Jtay5naXQgLSBzb21lIG9m
IHRoZXNlIGFyZSByZWxldmFudCBhcyBwYXJ0IG9mIGV2YWx1YXRpb24gKEkgZml4ZWQgc2V2
ZXJhbCBpc3N1ZXMgYWxyZWFkeSwgdGhhdCB5b3UncmUgbGlrZWx5IHRvIGZsYWcgaW4gdGhl
IHRhcmJhbGwpLg0KDQoqKiBCcmllZiBkZXNjcmlwdGlvbiBvZiB0aGUgcGFja2FnZToNCg0K
VGhlIC9DYW5vZWJvb3QvIHByb2plY3QgcHJvdmlkZXMgZnJlZSANCjxodHRwczovL3dyaXRl
ZnJlZXNvZnR3YXJlLm9yZy9sZWFybj4gKC9saWJyZS8pIGJvb3QgZmlybXdhcmUgYmFzZWQg
b24gDQpjb3JlYm9vdCwgcmVwbGFjaW5nIHByb3ByaWV0YXJ5IEJJT1MvVUVGSSBmaXJtd2Fy
ZSBvbiBzcGVjaWZpYyBJbnRlbC9BTUQgDQp4ODYgYW5kIEFSTSBiYXNlZCBtb3RoZXJib2Fy
ZHMgPGh0dHBzOi8vY2Fub2Vib290Lm9yZy9kb2NzL2hhcmR3YXJlLz4sIA0KaW5jbHVkaW5n
IGxhcHRvcCBhbmQgZGVza3RvcCBjb21wdXRlcnMuIEl0IGluaXRpYWxpc2VzIHRoZSBoYXJk
d2FyZSANCihlLmcuwqBtZW1vcnkgY29udHJvbGxlciwgQ1BVLCBwZXJpcGhlcmFscykgYW5k
IHN0YXJ0cyBhIGJvb3Rsb2FkZXIgZm9yIA0KeW91ciBvcGVyYXRpbmcgc3lzdGVtLiBHTlUr
TGludXggPGh0dHBzOi8vY2Fub2Vib290Lm9yZy9kb2NzL2dudWxpbnV4Lz4gDQphbmQgQlNE
IDxodHRwczovL2Nhbm9lYm9vdC5vcmcvZG9jcy9ic2QvPiBhcmUgd2VsbC1zdXBwb3J0ZWQu
IEhlbHAgaXMgDQphdmFpbGFibGUgdmlhICNjYW5vZWJvb3QgPGh0dHBzOi8vd2ViLmxpYmVy
YS5jaGF0LyNjYW5vZWJvb3Q+IG9uIExpYmVyYSANCjxodHRwczovL2xpYmVyYS5jaGF0Lz4g
SVJDLg0KDQoNCiogQ29kZQ0KKiogRGVwZW5kZW5jaWVzOg0KICAgICBQbGVhc2UgbGlzdCB0
aGUgcGFja2FnZSdzIGRlcGVuZGVuY2llcyAoc291cmNlIGxhbmd1YWdlLCBsaWJyYXJpZXMs
IGV0Yy4pLg0KDQpDYW5vZWJvb3QgYnVpbGQgc3lzdGVtIChjYm1rKSB3cml0dGVuIGluIFBP
U0lYIHNoZWxsIHNjcmlwdHMgKHNoKQ0KDQpVdGlscyAodXRpbC8pIHdyaXR0ZW4gaW4gYSBt
aXggb2YgQyBhbmQgR28NCg0KVXBzdHJlYW0gcHJvamVjdHMgc3VjaCBhcyBjb3JlYm9vdCwg
R1JVQiwgU2VhQklPUyBsYXJnZWx5IHdyaXR0ZW4gaW4gQywgd2l0aCBhIGJpdCBvZiBHbyBh
bmQgcHl0aG9uLCBhbHNvIGEgbWlsZCBzZWFzb25pbmcgb2YgeDg2IGFzc2VtYmx5IGxhbmd1
YWdlLCBpbiBhIGZldyBjYXNlcy4NCg0KKiogQ29uZmlndXJhdGlvbiwgYnVpbGRpbmcsIGlu
c3RhbGxhdGlvbjoNCiAgICAgSXQgbWlnaHQgb3IgbWlnaHQgbm90IHVzZSBBdXRvY29uZi9B
dXRvbWFrZSwgYnV0IGl0IG11c3QgbWVldCBHTlUNCiAgICAgc3RhbmRhcmRzLiAgRXZlbiBw
YWNrYWdlcyB0aGF0IGRvIG5vdCByZXF1aXJlIGNvbXBpbGF0aW9uDQogICAgIG11c3QgZm9s
bG93IHRoZXNlIHN0YW5kYXJkcywgc28gaW5zdGFsbGVycyBoYXZlIGEgdW5pZm9ybSB3YXkg
dG8NCiAgICAgZGVmaW5lIHRhcmdldCBkaXJlY3RvcmllcywgZXRjLiAgUGxlYXNlIHNlZToN
CiAgICAgaHR0cDovL3d3dy5nbnUub3JnL3ByZXAvc3RhbmRhcmRzL2h0bWxfbm9kZS9Db25m
aWd1cmF0aW9uLmh0bWwNCiAgICAgaHR0cDovL3d3dy5nbnUub3JnL3ByZXAvc3RhbmRhcmRz
L2h0bWxfbm9kZS9NYWtlZmlsZS1Db252ZW50aW9ucy5odG1sDQoNCkRvZXMgbm90IG1lZXQg
c3RhbmRhcmRzIGF0IGFsbCwgYnV0IG5laXRoZXIgZG9lcyBHTlUgQm9vdCBhbmQgbmVpdGhl
ciBkaWQgdGhlIGVyc3R3aGlsZSBHTlUgTGlicmVib290OyBpdCB3YXMgYWNjZXB0ZWQgYm90
aCB0aGVuIGFuZCBub3cgdGhhdCB0aGUgZGVzaWduIGlzIGRpZmZlcmVudCwgYnV0IHRoYXQg
R05VIG5lZWRlZCBhIHZpYWJsZSBGU0RHLWNvbXBsaWFudCBjb3JlYm9vdCBkaXN0cm8uDQoN
ClRoZSBDYW5vZWJvb3QgYnVpbGQgc3lzdGVtIGlzIGRvY3VtZW50ZWQgaGVyZToNCg0KaHR0
cHM6Ly9jYW5vZWJvb3Qub3JnL2RvY3MvbWFpbnRhaW4vDQoNCioqIERvY3VtZW50YXRpb246
DQogICAgIFdlIHJlcXVpcmUgdXNpbmcgVGV4aW5mbyAoaHR0cDovL3d3dy5nbnUub3JnL3Nv
ZnR3YXJlL3RleGluZm8vKQ0KICAgICBmb3IgZG9jdW1lbnRhdGlvbiwgYW5kIHJlY29tbWVu
ZCB3cml0aW5nIGJvdGggcmVmZXJlbmNlIGFuZCB0dXRvcmlhbA0KICAgICBpbmZvcm1hdGlv
biBpbiB0aGUgc2FtZSBtYW51YWwuICBQbGVhc2Ugc2VlDQogICAgIGh0dHA6Ly93d3cuZ251
Lm9yZy9wcmVwL3N0YW5kYXJkcy9odG1sX25vZGUvR05VLU1hbnVhbHMuaHRtbA0KDQpQYW5k
b2MgTWFya2Rvd24gaXMgdXNlZC4gU2VlOiBjYnd3dy5naXQNCg0KVGhlIFVudGl0bGVkIFN0
YXRpYyBTaXRlIEdlbmVyYXRvciBpcyB1c2VkIHRvIGdlbmVyYXRlIGl0Lg0KDQpUaGlzIGlz
IHdoYXQgR05VIEJvb3QgYWxzbyB1c2VzLCBhbmQgaXQgaGFzIE1hcmtkb3duLCBhbmQgd2Fz
IGFjY2VwdGVkLg0KDQoqKiBJbnRlcm5hdGlvbmFsaXphdGlvbjoNCiAgICAgSWYgeW91ciBw
YWNrYWdlIGhhcyBhbnkgdXNlci12aXNpYmxlIHN0cmluZ3MsIHBsZWFzZSBtYWtlIHRoZW0N
CiAgICAgdHJhbnNsYXRhYmxlIHRvIG90aGVyIGxhbmd1YWdlcyB1c2luZyBHTlUgR2V0dGV4
dDoNCiAgICAgaHR0cDovL3d3dy5nbnUub3JnL3NvZnR3YXJlL2dldHRleHQvDQoNCk5vIGkx
OG4sIGJ1dCB0aGVyZSBhcmUgdHJhbnNsYXRpb25zIG9mIGNlcnRhaW4gcGFnZXMgb24gdGhl
IHdlYnNpdGUsIG1haW50YWluZWQgbWFudWFsbHkuDQoNClNvbWUgb2YgdGhlIHBhY2thZ2Vz
IHRoYXQgQ2Fub2Vib290IHVzZXMgbWF5IGhhdmUgaTE4biwgc3VjaCBhcyBHTlUgR1JVQi4N
Cg0KKiogQWNjZXNzaWJpbGl0eToNCiAgICAgUGxlYXNlIGRpc2N1c3MgYW55YWNjZXNzaWJp
bGl0eSBpc3N1ZXMgIDxodHRwczovL3d3dy5nbnUub3JnL2FjY2Vzc2liaWxpdHkvYWNjZXNz
aWJpbGl0eS5odG1sPg0KICAgICB3aXRoIHlvdXIgcGFja2FnZSwgc3VjaCBhcyB1c2Ugb2Yg
cmVsZXZhbnQgQVBJcy4NCg0KQWNjZXNzaWJpbGl0eSBpc3N1ZXM6IG5vIHNjcmVlbiByZWFk
ZXIgaW4gdGhlIEdSVUIvU2VhQklPUyBib290IG1lbnUsIHRob3VnaCBHUlVCIChjb3JlYm9v
dCBwYXlsb2FkIG9mdGVuIHVzZWQgb24gQ2Fub2Vib290IGluc3RhbGxhdGlvbnMpIGhhcyBh
IG1vcnNlIGNvZGUgZ2VuZXJhdG9yIHdoaWNoIEkgY291bGQgcHJvYmFibHkgcmUtcHVycG9z
ZSBmb3IgYmxpbmQgdXNlcnMuDQoNCioqIFNlY3VyaXR5Og0KICAgICBQbGVhc2UgZGlzY3Vz
cyBhbnkgcG9zc2libGUgc2VjdXJpdHkgaXNzdWVzIHdpdGggeW91ciBwYWNrYWdlOg0KICAg
ICBjcnlwdG9ncmFwaGljIGFsZ29yaXRobXMgYmVpbmcgdXNlZCwgc2Vuc2l0aXZlIGRhdGEg
YmVpbmcgc3RvcmVkLA0KICAgICBwb3NzaWJsZSBlbGV2YXRpb24gb2YgcHJpdmlsZWdlcywg
ZXRjLg0KDQpObyBpc3N1ZXMgdGhhdCBJIGNhbiB0aGluayBvZi4NCg0KQ2Fub2Vib290IGFj
dHVhbGx5IGltcHJvdmVzIHRoZSBzZWN1cml0eSBvbiBzb21lIG9mIGl0cyBwYWNrYWdlcy4g
Rm9yIGV4YW1wbGUgaXQgYWRkcyBBcmdvbjIgS0RGIHN1cHBvcnQgdG8gR05VIEdSVUIsIHNv
IHRoYXQgeW91IGNhbiBib290IGZyb20gTFVLUzIgZm9ybWF0dGVkIC9ib290IHBhcnRpdGlv
bnMuDQoNCiogTGljZW5zaW5nOg0KICAgIEJvdGggdGhlIHNvZnR3YXJlIGl0c2VsZiAqYW5k
IGFsbCBkZXBlbmRlbmNpZXMqICh0aGlyZC1wYXJ0eQ0KICAgIGxpYnJhcmllcywgZXRjLikg
bXVzdCBiZSBmcmVlIHNvZnR3YXJlIGluIG9yZGVyIHRvIGJlIGluY2x1ZGVkIGluDQogICAg
R05VLiAgSW4gZ2VuZXJhbCwgb2ZmaWNpYWwgR05VIHNvZnR3YXJlIHNob3VsZCBiZSByZWxl
YXNlZCB1bmRlciB0aGUNCiAgICBHTlUgR1BMIHZlcnNpb24gMyBvciBhbnkgbGF0ZXIgdmVy
c2lvbiwgYW5kIEdOVSBkb2N1bWVudGF0aW9uIHNob3VsZA0KICAgIGJlIHJlbGVhc2VkIHVu
ZGVyIHRoZSBHTlUgRkRMIHZlcnNpb24gMS4zIG9yIGFueSBsYXRlciB2ZXJzaW9uLg0KDQpD
YW5vZWJvb3QgYnVpbGQgc3lzdGVtIGxhcmdlbHkgR1BMdjMrLCBzb21lIHBhcnRzIGFyZSBH
UEx2Mi1vbmx5Lg0KDQpDb3JlYm9vdCBpcyBsYXJnZWx5IEdQTHYyLg0KDQpHUlVCIGxhcmdl
bHkgR1BMdjMrLCBzb21ldGltZXMgdjIrIG9yIHYyLW9ubHkNCg0KU2VhQklPUyBsYXJnZWx5
IEdQTHYyLCB3aXRoIHNvbWUgdjMgc2Vhc29uaW5nDQoNCiAgICBQbGVhc2Ugc2VlaHR0cDov
L3d3dy5nbnUub3JnL3BoaWxvc29waHkvbGljZW5zZS1saXN0Lmh0bWwgIGZvciBhDQogICAg
cHJhY3RpY2FsIGd1aWRlIHRvIHdoaWNoIGxpY2Vuc2VzIGFyZSBmcmVlIChmb3IgR05VJ3Mg
cHVycG9zZXMpIGFuZA0KICAgIHdoaWNoIGFyZSBub3QuICBQbGVhc2UgZ2l2ZSBzcGVjaWZp
YyB1cmwncyB0byBhbnkgbGljZW5zZXMgaW52b2x2ZWQNCiAgICB0aGF0IGFyZSBub3QgbGlz
dGVkIG9uIHRoYXQgcGFnZS4NCg0KTk9URTogQ2Fub2Vib290IGFscmVhZHkgbGlzdGVkIG9u
IEZTRDoNCg0KaHR0cHM6Ly9kaXJlY3RvcnkuZnNmLm9yZy93aWtpL0Nhbm9lYm9vdA0KDQpG
U0YncyBvd24gY3JhaWd0IGhlYXZpbHkgYXVkaXRlZCBpdCBvdmVyIGEgb25lLXdlZWsgcGVy
aW9kLCBleHRlbnNpdmVseSBzY2FubmluZyBpdCBhbmQgdGhlbiBnb2luZyB0aHJvdWdoIGl0
IGFsbCB3aXRoIG1lLg0KDQoqIFNpbWlsYXIgZnJlZSBzb2Z0d2FyZSBwcm9qZWN0czoNCiAg
ICBQbGVhc2UgZXhwbGFpbiB3aGF0IG1vdGl2YXRlZCB5b3UgdG8gd3JpdGUgeW91ciBwYWNr
YWdlLCBhbmQgc2VhcmNoDQogICAgYXQgbGVhc3QgdGhlIEZyZWUgU29mdHdhcmUgRGlyZWN0
b3J5IChodHRwOi8vd3d3LmdudS5vcmcvZGlyZWN0b3J5LykNCiAgICBmb3IgcHJvamVjdHMg
c2ltaWxhciB0byB5b3Vycy4gIElmIGFueSBleGlzdCwgcGxlYXNlIGFsc28gZXhwbGFpbg0K
ICAgIHdoYXQgdGhlIHByaW5jaXBhbCBkaWZmZXJlbmNlcyBhcmUuDQoNCkdOVSBCb290DQoN
CkkgaW50ZW5kIGZvciBDYW5vZWJvb3QgdG8gcmVwbGFjZSBHTlUgQm9vdCwgYW5kIGZvciBH
TlUgQm9vdCB0byBiZSBkZWNvbW1pc3Npb25lZCwgc2luY2UgaXQgaXMgY3VycmVudGx5IGEg
ZGVhZCBwcm9qZWN0OyBDYW5vZWJvb3QgaXMgdGhlIG9ubHkgRlNERyBjb21wbGlhbnQgY29y
ZWJvb3QgZGlzdHJvIHVuZGVyIGFjdGl2ZSBkZXZlbG9wbWVudC4NCg0KSW4gYWRkaXRpb24s
IGlmIGFjY2VwdGVkLCBJIHN1cHBvc2UgbGlicmVib290LmF0IHdvdWxkIGFsc28gYmUgcmVk
aXJlY3RlZC4gQm90aCBsaWJyZWJvb3QuYXQgYW5kIEdOVSBCb290IHdvdWxkIHJlZGlyZWN0
IHRvIENhbm9lYm9vdC4NCg0KVGhlIGN1cnJlbnQgR05VIEJvb3QgZGV2ZWxvcGVycyBhcmUg
d2VsY29tZSB0byB3b3JrIHdpdGggbWUgYXMgY29udHJpYnV0b3JzIGlmIHRoZXkgd2lzaCwg
YnV0IHRoZXkgbXVzdCBub3QgYmUgbWFkZSBtYWludGFpbmVycyBvZmZpY2lhbGx5OyBJIHdp
bGwgYXNzdW1lIHRoYXQgcnVsZSBhcyBHTlUgQ2Fub2Vib290IG1haW50YWluZXIuDQoNCiog
QW55IG90aGVyIGluZm9ybWF0aW9uLCBjb21tZW50cywgb3IgcXVlc3Rpb25zOg0KDQpodHRw
czovL3RyaXNxdWVsLmluZm8vZW4vZm9ydW0vY2Fub2Vib290LTIwMjQwNTEwLXJlbGVhc2Vk
LWdudS1mc2RnLWNvbXBsaWFudC0xMDAtZnJlZS1zb2Z0d2FyZS1jb3JlYm9vdC1kaXN0cm8t
cmVwbGFjaW5nLXBybw0KDQpUaGlzIGxpbmsgY29udGFpbnMgZGlzY3Vzc2lvbiwgaW5jbHVk
aW5nIGZyb20ganhzZWxmIChsZWFkaW5nIG1lbWJlciBvZiBHTlUgQWR2aXNvcnkgQ29tbWl0
dGVlKS4gVGhlIHN1YnRleHQgaXMgR05VIENhbm9lYm9vdCwgYmVjYXVzZSBqeHNlbGYgd2Fz
IGF3YXJlIG9mIG15IHBsYW4gd2hlbiBoZSBwb3N0ZWQgaGVyZS4NCg0KRWFybHkgZGF5cyB0
aHVzIGZhciwgYnV0IHRoZSBnaXN0IGlzIHRoaXM6IENhbm9lYm9vdCBoYXMgZHJvcHBlZCAx
MDAlIG9mIGl0cyBob3N0aWxpdHkgdG8gRlNGIGFuZCBJJ3ZlIGRlY2lkZWQgdGhhdCBpdCB3
aWxsIHN0YXVuY2hseSAqc3VwcG9ydCogdGhlIEZTRiBpbnN0ZWFkLCBvcGVubHkgcHJvbW90
aW5nIEdOVSBGU0RHIHBvbGljeSBhbmQgZW5jb3VyYWdpbmcgdGhlIHVzZSBvZiBGU0RHIGxp
Y2Vuc2VkIGRpc3Ryb3Mgc3VjaCBhcyBUcmlzcXVlbC4gVGhpcyB3b3VsZCBiZSBqdXN0IGxp
a2UgdGhlIGdvb2Qgb2xkIGRheXMgb2YgR05VIExpYnJlYm9vdCEgVGhpcyBjaGFuZ2UgaXMg
cGVybWFuZW50Lg0KSSB3YXNuJ3QgZ29pbmcgdG8gbWFrZSB0aGlzIHJlcXVlc3QgdG8gZ251
ZXZhbCwgYnV0IHNpbmNlIEdOVSBCb290IGlzbid0IHJlYWxseSBhIHRoaW5nIGFueW1vcmUg
KG5vIGNvbW1pdHMgaW4gb3ZlciA0IG1vbnRocyBvbiB0aGVpciBtYWluIGJyYW5jaCwgYW5k
IGdlbmVyYWxseSBzbG93IGRldmVsb3BtZW50IGJlZm9yZSB0aGVuKSwgSSB0aG91Z2h0OiB3
aHkgbm90Pw0KDQpDYW5vZWJvb3QgaXMgYmVpbmcga2VwdCBzZXBhcmF0ZSBmcm9tIExpYnJl
Ym9vdCBmcm9tIG5vdyBvbi4gSXQgbm8gbG9uZ2VyIHByb21vdGVzIExpYnJlYm9vdC4gV2hl
biBJJ20gd29ya2luZyBvbiBDYW5vZWJvb3QsIEkgc2ltcGx5IGVudGVyIEdOVSBMZWFoIE1v
ZGUsIHdoaWNoIGlzIGEgYnJhaW5tb2RlIHdoZXJlIEkgYmVsaWV2ZSBhYnNvbHV0ZWx5IGlu
IGl0IGFuZCB3aWxsIHN0YW5kIGJ5IGl0IHRvIHRoZSB2ZXJ5IGVuZC4gSSdtIHJlYWxseSBn
b29kIGF0IHRoYXQsIGFuZCBJIGFsc28gZGlkIHRoYXQgd2hlbiBHTlUgTGlicmVib290IHdh
cyBhIHRoaW5nLg0KDQpJJ3ZlIGFscmVhZHkgc3Bva2VuIHRvIHNldmVyYWwgcGVvcGxlIHdo
byBhcmUgaW5mbHVlbnRpYWwgc3VjaCBhcyBNaWtlIEdlcndpdHogYW5kIEJvYiBQcm91bHgs
IGFuZCB0aGV5IGhhdmUgc2FpZCB0aGF0LCBpbiBwcmluY2lwbGUsIHRoZXkgc3VwcG9ydCB0
aGlzIG1vdmUsIHRob3VnaCB0aGV5IGhhdmUgYWxzbyB0b2xkIG1lIHRoYXQgdGhleSB3aWxs
IG5vdCBiZSBpbnZvbHZlZCAob2YgY291cnNlLCBpZiB0aGV5IGRvIHdhbnQgdG8sIEknZCBs
aWtlIHRoYXQpLg0KDQpHTlUgQ2Fub2Vib290Lg0KDQpUaGF0IGlzIHdoYXQgSSB3YW50LCBh
bmQgdGhhdCBpcyB3aGF0IEkgcHJvcG9zZS4gSSB3aWxsIGZvbGxvdyBhbGwgcnVsZXMgYW5k
IGRvIHRoaW5ncyByaWdodC4NCg0KQWxzbzoNCg0KQW5vdGhlciBsaWJyZXhpdCAobGlicmVi
b290IGV4aXQgZnJvbSBHTlUpIHdpbGwgbm90IG9jY3VyLiBDYW5vZWJvb3Qgd2lsbCBiZSBH
TlUgZm9yZXZlciwgaWYgYWNjZXB0ZWQuIEkgbmV2ZXIgdG9sZCBhbnlvbmUgdGhpcyBiZWZv
cmUsIGFuZCBpdCdzIG5vdCBhbiBleGNsdXNlLCBidXQgaXQgaXMgYSBtaXRpZ2F0aW5nIGZh
Y3RvcjogSSB3YXMgZ29pbmcgdGhyb3VnaCBhIHZlcnkgZGlmZmljdWx0IHRpbWUgaW4gbXkg
bGlmZSB3aGVuIExpYnJlYm9vdCBsZWZ0IEdOVSBhbGwgdGhvZXMgeWVhcnMgYWdvLiBJIHdh
cyByZWd1bGFybHkgZHJpbmtpbmcsIGFuZCBJIHdhcyBkcnVuayB3aGVuIEkgb3JpZ2luYWxs
eSBzZW50IHRob2VzIGhvc3RpbGUgbWVzc2FnZXMgdG8gR05VIGluIDIwMTYuIEknbSBub3Qg
bGlrZSB0aGF0IGZvciB5ZWFycyBub3cuIEkgZG9uJ3QgZHJpbmsgYW55bW9yZSwgYW5kIEkg
ZG9uJ3QgZG8gZHJ1Z3MgLSBhbmQgSSBoYXZlbid0IGRvbmUgc28gZm9yIG1hbnkgeWVhcnMg
bm93Lg0KDQpUaGUgd2F5IEkgc2VlIGl0LCB0aGVyZSB3aWxsIGFsd2F5cyBiZSBhIGRlbWFu
ZCBmb3IgYSBmdWxseSBmcmVlIGNvcmVib290IGRpc3RybywgYW5kIENhbm9lYm9vdCBpcyBj
dXJyZW50bHkgdGhlIG9ubHkgdmlhYmxlIHByb2plY3QgaW4gdGhpcyByZWdhcmQuDQoNCkNh
bm9lYm9vdCBpcyBzdXBlcmlvciB0byBHTlUgQm9vdCBmb3IgdGhlc2UgcmVhc29uczoNCg0K
KiBNdWNoIG1vcmUgdXAgdG8gZGF0ZS4gVXNlcyBjb3JlYm9vdCwgR1JVQiBhbmQgU2VhQklP
UyByZXZpc2lvbnMgZnJvbSAyMDI0LCB3aGVyZWFzIEdOVSBCb290IHVzZXMgcmV2cyBmcm9t
IGxhdGUgMjAyMS4NCg0KKiBCdWlsZCBzeXN0ZW0gaXMgbW9yZSBlZmZpY2llbnQ6IDYgc2hl
bGwgc2NyaXB0cyBpbnN0ZWFkIG9mIEdOVSBCb290J3MgNTAsIGFuZCBhYm91dCAxMzAwIGxp
bmVzIG9mIGNvZGUgaW4gdGhlIGJ1aWxkIHN5c3RlbSwgdmVyc3VzIEdOVSBCb290J3MgfjUw
MDAuIEdlbmVyYWxseSBjbGVhbmVyIGNvZGluZyBzdHlsZSBpbiBDYW5vZWJvb3QuDQoNCiog
RGVzcGl0ZSBiZWluZyBzbWFsbGVyLCBDYW5vZWJvb3QgYWN0dWFsbHkgaGFzIG1vcmUgZmVh
dHVyZXMuIFN1Y2ggYXMgYnVpbGRpbmcgb2Ygc2VycHJvZyBpbWFnZXMgKHRvIG1ha2UgY2hl
YXAgU1BJIGZsYXNoZXJzKSwgc3VwcG9ydCBmb3IgYnVpbGRpbmcgVS1Cb290IHBheWxvYWQg
b24gQVJNIGRldmljZXMgKGFuZCB0aGV5IGJvb3QpLCBhbmQgbW9yZSBoYXJkd2FyZSBzdXBw
b3J0Lg0KDQoqIEkndmUgYmVlbiB3b3JraW5nIG9uIHRoaXMgc3R1ZmYgZm9yIG92ZXIgMTAg
eWVhcnMuIEkga25vdyBhbGwgdGhlIG5vb2tzIGFuZCBjcmFubmllcyBvZiBjb3JlYm9vdCwg
YW5kIGhvdyB0byByZWFsbHkgbWFrZSB5b3VyIGJvb3Rsb2FkZXIgc2luZw0KDQoqIEdOVSBC
b290IHVzZXMgTGlicmVib290J3Mgb2xkIGJ1aWxkIHN5c3RlbSBkZXNpZ24sIHdoaWNoIGlz
IHdoeSBpdCdzIG11Y2ggYmlnZ2VyLiBJIGRpZCBhIHNlcmllcyBvZiBhdWRpdHMgaW4gMjAy
MyB0byB2YXN0bHkgaW5jcmVhc2UgdGhlIGNvZGUgcXVhbGl0eSBpbiB0aGUgYnVpbGQgc3lz
dGVtLg0KDQoqIEdOVSBCb290IGlzIGdvaW5nIHRvIGJlY29tZSBtb3JlIGNvbXBsZXgsIGJl
Y2F1c2UgdGhleSB3YW50L3dhbnRlZCB0byByZXdyaXRlIGl0IGFsbCBpbiBHdWlsbGUgYW5k
IHVzZSB0aGUgR1VpeCBwYWNrYWdlIG1hbmFnZXIgdG8gYnVpbGQgZXZlcnl0aGluZy4gV2hp
bGUgdGhpcyB3b3VsZCBtYWtlIGluZGl2aWR1YWwgYnVpbGRpbmcgZWFzaWVyLCBpdCB3b3Vs
ZCB2YXN0bHkgaW5jcmVhc2UgdGhlIG1haW50ZW5hbmNlIGJ1cmRlbiBhbmQgaW50cm9kdWNl
IG1hbnkgbW92aW5nIHBhcnRzIHRvIHRoZSBwcm9qZWN0LCBtYWtpbmcgaXQgdW5tYWludGFp
bmFibGUgb3ZlciB0aW1lLiBDYW5vZWJvb3QncyBkZXNpZ24gaXMgbXVjaCBzaW1wbGVyIGFu
ZCBJJ20gYWxzbyB3b3JraW5nIG9uIGJvb3RzdHJhcHBpbmcgKGUuZy4gbXVzbC1jcm9zcy1t
YWtlIGludGVncmF0aW9uKQ0KDQoqIEdOVSBCb290IGxhY2tzIG1hbnkgb2YgTGlicmVib290
J3MgbmV3ZXIgc2VjdXJpdHkgZmVhdHVyZXMsIHN1Y2ggYXMgQXJnb24yIEtERiBzdXBwb3J0
IGZvciBMVUtTMiBib290DQoNClNvLCBiYXNpY2FsbHksIENhbm9lYm9vdCBpcyBtdWNoIGVh
c2llciBhbmQgYmV0dGVyLg0KDQpJIGFjdHVhbGx5IGRpZCBpbml0aWFsbHkgdHJ5IHRvIGhl
bHAgR05VIEJvb3QgaW5zdGVhZC4gVGhlIHByb2JsZW0gd2l0aCBHTlUgQm9vdCBpcyB0aGF0
IGl0J3MgYmFzZWQgb24gYSByZWFsbHkgb2xkIExpYnJlYm9vdCB2ZXJzaW9uIGFuZCBoYXNu
J3QgYmVlbiBjaGFuZ2VkIG11Y2ggc2luY2UsIGFuZCB0aGV5J3ZlIGJhc2ljYWxseSBiZWVu
IGluICJkZXZlbG9wbWVudCBoZWxsIi4gSSBzZW50IHRoZW0gZXh0ZW5zaXZlIHBhdGNoZXMg
ZmdpeGluZyBidWlsZCBpc3N1ZXMsIHNvIHRoYXQgaXQgYnVpbGRzIG9uIG1vZGVybiBkaXN0
cm9zLCBhbmRoIEkgc2VudCB0aGVtIHBhdGNoZXMgdXBkYXRpbmcgaXQgdG8gbmV3ZXIgdXBz
dHJlYW0gcmV2aXNpb25zIGUuZy4gY29yZWJvb3QsIGJ1dCBub25lIG9mIG15IHBhdGNoZXMg
d2VyZSByZXZpZXdlZC4gSSBkb24ndCB0aGluayB0aGUgY3VycmVudCBkZXZlbG9wZXJzIGFy
ZSB1cCB0byB0aGUgdGFzaywgYW5kIHRoaXMgaXMgbm90IGFuIGluc3VsdDsgdGhleSB3b3Jr
ZWQgdW5kZXIgbWUgYXMgTGlicmVib290IGNvbnRyaWJ1dG9ycyBpbiB0aGUgcGFzdCwgYW5k
IHRoZXkgb25seSBldmVyIHdvcmtlZCBvbiBtaW5vciB0YXNrcywgdGhleSBuZXZlciBkaWQg
YW55dGhpbmcgYmlnLiBJJ3ZlIHNhaWQgaW4gdGhlIHBhc3QgdGhhdCBwZXJoYXBzIEkgc2hv
dWxkIGJlIGFwcG9pbnRlZCBhcyBsZWFkZXIgb2YgR05VIEJvb3QgaW5zdGVhZCwgYnV0IEkg
aGF2ZSBteSBDYW5vZWJvb3QgcHJvamVjdCB3aGljaCBoYXMgc3VycGFzc2VkIGl0IHRlY2hu
aWNhbGx5IGluIGV2ZXJ5IHdheSwgc28gbm93IEkgd2FudCBhIEdOVSBDYW5vZWJvb3QuDQoN
ClVwY29taW5nIHdvcmsgb24gQ2Fub2Vib290Og0KDQoqIE1vcmUgQVJNIGNocm9tZWJvb2tz
LCB3aGljaCBBbHBlciBOZWJpIFlhc2FrIChsaWJyZWJvb3QgZGV2ZWxvcGVyKSBpcyB3b3Jr
aW5nIG9uDQoNCiogTW9yZSBEZWxsIExhdGl0dWRlcyAoR000NSBtb2R1bGVzKS4gSSdtIHdv
cmtpbmcgb24gdGhlc2UsIGJhc2VkIG9uIHRoZSBEZWxsIEU2NDAwIHBvcnQNCg0KKiBNYXRl
IEt1a3JpIChjb3JlYm9vdCBkZXZlbG9wZXIpIGlzIHdvcmtpbmcgb24gYW4gZXhwbG9pdCBv
ZiBJbnRlbCBTQS0wMDA4NiB0byBnYWluIHVuc2lnbmVkIGNvZGUgZXhlY3V0aW9uIG9uIElu
dGVsIE1FIHYxMSwgZm9yIFNreWxha2UgYm9hcmRzLCBidXQgSSdtIHRvbGQgdGhhdCBzaW1p
bGFyIGV4cGxvaXRzIGFyZSBwb3NzaWJsZSBhbmQgd2lsbCBiZSB3b3JrZWQgb24sIGZvciBv
bGRlciBzYW5keWJyaWRnZSwgaXZ5YnJpZGdlIGFuZCBoYXN3ZWxsIGhhcmR3YXJlIChlLmcu
IFRoaW5rUGFkIFgyMjAsIFgyMzAsIFQ0NDBwKSAtIGN1cnJlbnRseSwgdGhlIG9ubHkgYmxv
YnMgbmVlZGVkIG9uIHRob3NlIGJvYXJkcyBhcmUgSW50ZWwgTUUgYW5kIG1pY3JvY29kZSwg
dGhvdWdoIHRoZXkgY2FuIGJvb3Qgd2l0aG91dCBtaWNyb2NvZGUuIFRoZXJlJ3MgYSBjaGFu
Y2UgdGhhdCBpbiB0aGUgbmV4dCBmZXcgeWVhcnMsIHdlIHdpbGwgaGF2ZSB3aGF0IEkgY2Fs
bCB0aGUgSW50ZWwgRnJlZWRvbSBFbmdpbmUsIGEgZnVsbCBmcmVlIHJlcGxhY2VtZW50IG9m
IEludGVsIE1FLiBUaGlzIHdvdWxkIGJlY29tZSBwYXJ0IG9mIEdOVSwgaWYgR05VIGFjY2Vw
dHMgQ2Fub2Vib290IHRvZGF5Lg0KDQoqIExpbnV4LWxpYnJlIHBheWxvYWQgd2l0aCBtdXNs
IGxpYmMgYW5kIGJ1c3lib3gsIGFuZCBVLVJvb3QsIHRvIHByb3ZpZGUgYm9vdGluZyBvZiBs
aW51eCBrZXJuZWxzIG9uIGRpc2sgYW5kIG92ZXIgdGhlIG5ldHdvcmssIGZyb20gdGhlIGZs
YXNoLiAoR05VK0xpbnV4IHN5c3RlbSBpbiBmbGFzaCBiYXNpY2FsbHkpLCB3aXRoIG1hbnkg
c2VjdXJpdHkgZmVhdHVyZXMgc3VjaCBhcyBtZWFzdXJlZCBib290LCBhbmQgbmF0aXZlIHN1
cHBvcnQgZm9yIFpGUyBmaWxlIHN5c3RlbS4NCg0KU29tZSBvciBhbGwgb2YgdGhlIGFib3Zl
LCBhbmQgbW9yZSwgd2lsbCBiZSBwcmVzZW50IGluIENhbm9lYm9vdCB0aGlzIHllYXIuDQoN
ClNvIGhvdyBhYm91dCBpdD8NCg0KTWFueSBwZW9wbGUgd2lsbCBiZSBzdXJwcmlzZWQgYnkg
dGhpcyBlbWFpbC4gQnV0IGlmIHlvdSBwdXQgeW91ciB0cnVzdCBpbiBtZSwgSSBwcm9taXNl
IEkgd29uJ3QgZGlzYXBwb2ludC4gSSB3aWxsIG9mIGNvdXJzZSBtYWtlIGEgc2F2YW5uYWgg
YWNjb3VudCBhcyBwYXJ0IG9mIHRoaXMsIGFuZCB1c2UgaXQsIGlmIGFjY2VwdGVkLg0KDQot
LSANCkNvbXBhbnkgZGlyZWN0b3IsIE1pbmlmcmVlIEx0ZA0KUmVnaXN0ZXJlZCBpbiBFbmds
YW5kLCBOby4gOTM2MTgyNiB8IFZBVCBOby4gR0IyMDIxOTA0NjINClJlZ2lzdGVyZWQgT2Zm
aWNlOiAxOSBIaWx0b24gUm9hZCwgQ2FudmV5IElzbGFuZCwgRXNzZXggU1M4IDlRQSwgVUsN
Cg0K
--------------0qkEOS0KaVvTlm4TzFss1Nsy
Content-Type: text/html; charset=UTF-8
Content-Transfer-Encoding: quoted-printable

<!DOCTYPE html>
<html>
  <head>

    <meta http-equiv=3D"content-type" content=3D"text/html; charset=3DUTF=
-8">
  </head>
  <body>
    <pre>
Hi

This is sent to GNU Eval team, but CC'd to others. My main recipient is t=
he GNU Eval team.

I want GNU Canoeboot. This is my official contact with GNU, as the lead d=
eveloper and founder of the Canoeboot project, a fully free coreboot dist=
ro based on Libreboot.

Canoeboot has recently removed all opposition to the FSF and decided to s=
taunchly promote it instead, in addition to FSDG. This is part of a gener=
al desire I've had since the start of the year, to seek reconcilliation w=
ith the FSF and GNU project, after the drama that ensued first with libre=
boot.org vs libreboot.at, and then libreboot.org vs GNU Boot.

This change is permanent, whether GNU accepts my proposal today; even if =
Canoeboot does not become GNU Canoeboot, it will continue to operate as i=
t does now. I recently did a release which is staunchly pro-FSF. I've don=
e this, precisely because GNU Boot is no longer competition to Canoeboot =
in any way; GNU Boot seems to have stalled, so Canoeboot has, with this m=
oved, effectively replaced it. I don't say this as an attack, but it is a=
 fact that GNU Boot has, as I write this, not submitted anything to their=
 main branch in over 4 months. it's now a dead project, and Canoeboot is =
taking over.

I know GNU Boot is already a thing. I envision Canoeboot replacing it. No=
w, answers to questions from gnueval form:

* General Information
** Do you agree to follow GNU policies?
   If your program is accepted to be part of the GNU system, it means
   that you become a GNU maintainer, which in turn means that you will
   need to follow GNU policies in regards to that GNU program.
   (Summarized above, see maintainers document for full descriptions.)

Yes. Canoeboot already complies fully with the GNU Free System Distributi=
on Guidelines. There may be a few stragglers left over from when it forke=
d from Libreboot, but these will surely be found during review.

I've already done extensive auditing myself, as has Craig Topham in his c=
apacity as Licensing and Compliance officer at the FSF. (Canoeboot 2023 r=
eleases were audited)

** Package name and version:

Canoeboot 20240510

** Author Full Name &lt;Email&gt;:

Leah Rowe <a class=3D"moz-txt-link-rfc2396E" href=3D"mailto:info@minifree=
=2Eorg">&lt;info@minifree.org&gt;</a>

** URL to package home page (if any):

<a class=3D"moz-txt-link-freetext" href=3D"https://canoeboot.org/">https:=
//canoeboot.org/</a>

** URL to source tarball:
    Please make a release tarball for purposes of evaluation, whether
    or not you publicly release it.  If you don't have
    anywhere to upload it, send it as an attachment.

<a class=3D"moz-txt-link-freetext" href=3D"https://www.mirrorservice.org/=
sites/libreboot.org/release/canoeboot/20240510/canoeboot-20240510_src.tar=
=2Exz">https://www.mirrorservice.org/sites/libreboot.org/release/canoeboo=
t/20240510/canoeboot-20240510_src.tar.xz</a>

NOTE: Several changes have been made since that release. Check the git lo=
g for cbwww.git and cbmk.git - some of these are relevant as part of eval=
uation (I fixed several issues already, that you're likely to flag in the=
 tarball).

** Brief description of the package:

</pre>
    <p>The <em>Canoeboot</em> project provides <a
        href=3D"https://writefreesoftware.org/learn">free</a> (<em>libre<=
/em>)
      boot firmware based on coreboot, replacing proprietary BIOS/UEFI
      firmware on <a href=3D"https://canoeboot.org/docs/hardware/">specif=
ic
        Intel/AMD x86 and ARM based motherboards</a>, including laptop
      and desktop computers. It initialises the hardware (e.g.=C2=A0memor=
y
      controller, CPU, peripherals) and starts a bootloader for your
      operating system. <a href=3D"https://canoeboot.org/docs/gnulinux/">=
GNU+Linux</a>
      and <a href=3D"https://canoeboot.org/docs/bsd/">BSD</a> are
      well-supported. Help is available via <a
        href=3D"https://web.libera.chat/#canoeboot">#canoeboot</a> on <a
        href=3D"https://libera.chat/">Libera</a> IRC.</p>
    <pre>

* Code
** Dependencies:
    Please list the package's dependencies (source language, libraries, e=
tc.).

Canoeboot build system (cbmk) written in POSIX shell scripts (sh)

Utils (util/) written in a mix of C and Go

Upstream projects such as coreboot, GRUB, SeaBIOS largely written in C, w=
ith a bit of Go and python, also a mild seasoning of x86 assembly languag=
e, in a few cases.

** Configuration, building, installation:
    It might or might not use Autoconf/Automake, but it must meet GNU
    standards.  Even packages that do not require compilation
    must follow these standards, so installers have a uniform way to
    define target directories, etc.  Please see:
    <a class=3D"moz-txt-link-freetext" href=3D"http://www.gnu.org/prep/st=
andards/html_node/Configuration.html">http://www.gnu.org/prep/standards/h=
tml_node/Configuration.html</a>
    <a class=3D"moz-txt-link-freetext" href=3D"http://www.gnu.org/prep/st=
andards/html_node/Makefile-Conventions.html">http://www.gnu.org/prep/stan=
dards/html_node/Makefile-Conventions.html</a>

Does not meet standards at all, but neither does GNU Boot and neither did=
 the erstwhile GNU Libreboot; it was accepted both then and now that the =
design is different, but that GNU needed a viable FSDG-compliant coreboot=
 distro.

The Canoeboot build system is documented here:

<a class=3D"moz-txt-link-freetext" href=3D"https://canoeboot.org/docs/mai=
ntain/">https://canoeboot.org/docs/maintain/</a>

** Documentation:
    We require using Texinfo (<a class=3D"moz-txt-link-freetext" href=3D"=
http://www.gnu.org/software/texinfo/">http://www.gnu.org/software/texinfo=
/</a>)
    for documentation, and recommend writing both reference and tutorial
    information in the same manual.  Please see
    <a class=3D"moz-txt-link-freetext" href=3D"http://www.gnu.org/prep/st=
andards/html_node/GNU-Manuals.html">http://www.gnu.org/prep/standards/htm=
l_node/GNU-Manuals.html</a>

Pandoc Markdown is used. See: cbwww.git

The Untitled Static Site Generator is used to generate it.

This is what GNU Boot also uses, and it has Markdown, and was accepted.

** Internationalization:
    If your package has any user-visible strings, please make them
    translatable to other languages using GNU Gettext:
    <a class=3D"moz-txt-link-freetext" href=3D"http://www.gnu.org/softwar=
e/gettext/">http://www.gnu.org/software/gettext/</a>

No i18n, but there are translations of certain pages on the website, main=
tained manually.

Some of the packages that Canoeboot uses may have i18n, such as GNU GRUB.=


** Accessibility:
    Please discuss any <a
    href=3D"https://www.gnu.org/accessibility/accessibility.html">accessi=
bility issues</a>
    with your package, such as use of relevant APIs.

Accessibility issues: no screen reader in the GRUB/SeaBIOS boot menu, tho=
ugh GRUB (coreboot payload often used on Canoeboot installations) has a m=
orse code generator which I could probably re-purpose for blind users.

** Security:
    Please discuss any possible security issues with your package:
    cryptographic algorithms being used, sensitive data being stored,
    possible elevation of privileges, etc.

No issues that I can think of.

Canoeboot actually improves the security on some of its packages. For exa=
mple it adds Argon2 KDF support to GNU GRUB, so that you can boot from LU=
KS2 formatted /boot partitions.

* Licensing:
   Both the software itself *and all dependencies* (third-party
   libraries, etc.) must be free software in order to be included in
   GNU.  In general, official GNU software should be released under the
   GNU GPL version 3 or any later version, and GNU documentation should
   be released under the GNU FDL version 1.3 or any later version.

Canoeboot build system largely GPLv3+, some parts are GPLv2-only.

Coreboot is largely GPLv2.

GRUB largely GPLv3+, sometimes v2+ or v2-only

SeaBIOS largely GPLv2, with some v3 seasoning

   Please see <a class=3D"moz-txt-link-freetext" href=3D"http://www.gnu.o=
rg/philosophy/license-list.html">http://www.gnu.org/philosophy/license-li=
st.html</a> for a
   practical guide to which licenses are free (for GNU's purposes) and
   which are not.  Please give specific url's to any licenses involved
   that are not listed on that page.

NOTE: Canoeboot already listed on FSD:

<a class=3D"moz-txt-link-freetext" href=3D"https://directory.fsf.org/wiki=
/Canoeboot">https://directory.fsf.org/wiki/Canoeboot</a>

FSF's own craigt heavily audited it over a one-week period, extensively s=
canning it and then going through it all with me.

* Similar free software projects:
   Please explain what motivated you to write your package, and search
   at least the Free Software Directory (<a class=3D"moz-txt-link-freetex=
t" href=3D"http://www.gnu.org/directory/">http://www.gnu.org/directory/</=
a>)
   for projects similar to yours.  If any exist, please also explain
   what the principal differences are.

GNU Boot

I intend for Canoeboot to replace GNU Boot, and for GNU Boot to be decomm=
issioned, since it is currently a dead project; Canoeboot is the only FSD=
G compliant coreboot distro under active development.

In addition, if accepted, I suppose libreboot.at would also be redirected=
=2E Both libreboot.at and GNU Boot would redirect to Canoeboot.

The current GNU Boot developers are welcome to work with me as contributo=
rs if they wish, but they must not be made maintainers officially; I will=
 assume that rule as GNU Canoeboot maintainer.

* Any other information, comments, or questions:

<a class=3D"moz-txt-link-freetext" href=3D"https://trisquel.info/en/forum=
/canoeboot-20240510-released-gnu-fsdg-compliant-100-free-software-coreboo=
t-distro-replacing-pro">https://trisquel.info/en/forum/canoeboot-20240510=
-released-gnu-fsdg-compliant-100-free-software-coreboot-distro-replacing-=
pro</a>

This link contains discussion, including from jxself (leading member of G=
NU Advisory Committee). The subtext is GNU Canoeboot, because jxself was =
aware of my plan when he posted here.

Early days thus far, but the gist is this: Canoeboot has dropped 100% of =
its hostility to FSF and I've decided that it will staunchly *support* th=
e FSF instead, openly promoting GNU FSDG policy and encouraging the use o=
f FSDG licensed distros such as Trisquel. This would be just like the goo=
d old days of GNU Libreboot! This change is permanent.
I wasn't going to make this request to gnueval, but since GNU Boot isn't =
really a thing anymore (no commits in over 4 months on their main branch,=
 and generally slow development before then), I thought: why not?

Canoeboot is being kept separate from Libreboot from now on. It no longer=
 promotes Libreboot. When I'm working on Canoeboot, I simply enter GNU Le=
ah Mode, which is a brainmode where I believe absolutely in it and will s=
tand by it to the very end. I'm really good at that, and I also did that =
when GNU Libreboot was a thing.

I've already spoken to several people who are influential such as Mike Ge=
rwitz and Bob Proulx, and they have said that, in principle, they support=
 this move, though they have also told me that they will not be involved =
(of course, if they do want to, I'd like that).

GNU Canoeboot.

That is what I want, and that is what I propose. I will follow all rules =
and do things right.

Also:

Another librexit (libreboot exit from GNU) will not occur. Canoeboot will=
 be GNU forever, if accepted. I never told anyone this before, and it's n=
ot an excluse, but it is a mitigating factor: I was going through a very =
difficult time in my life when Libreboot left GNU all thoes years ago. I =
was regularly drinking, and I was drunk when I originally sent thoes host=
ile messages to GNU in 2016. I'm not like that for years now. I don't dri=
nk anymore, and I don't do drugs - and I haven't done so for many years n=
ow.

The way I see it, there will always be a demand for a fully free coreboot=
 distro, and Canoeboot is currently the only viable project in this regar=
d.

Canoeboot is superior to GNU Boot for these reasons:

* Much more up to date. Uses coreboot, GRUB and SeaBIOS revisions from 20=
24, whereas GNU Boot uses revs from late 2021.

* Build system is more efficient: 6 shell scripts instead of GNU Boot's 5=
0, and about 1300 lines of code in the build system, versus GNU Boot's ~5=
000. Generally cleaner coding style in Canoeboot.

* Despite being smaller, Canoeboot actually has more features. Such as bu=
ilding of serprog images (to make cheap SPI flashers), support for buildi=
ng U-Boot payload on ARM devices (and they boot), and more hardware suppo=
rt.

* I've been working on this stuff for over 10 years. I know all the nooks=
 and crannies of coreboot, and how to really make your bootloader sing

* GNU Boot uses Libreboot's old build system design, which is why it's mu=
ch bigger. I did a series of audits in 2023 to vastly increase the code q=
uality in the build system.

* GNU Boot is going to become more complex, because they want/wanted to r=
ewrite it all in Guille and use the GUix package manager to build everyth=
ing. While this would make individual building easier, it would vastly in=
crease the maintenance burden and introduce many moving parts to the proj=
ect, making it unmaintainable over time. Canoeboot's design is much simpl=
er and I'm also working on bootstrapping (e.g. musl-cross-make integratio=
n)

* GNU Boot lacks many of Libreboot's newer security features, such as Arg=
on2 KDF support for LUKS2 boot

So, basically, Canoeboot is much easier and better.

I actually did initially try to help GNU Boot instead. The problem with G=
NU Boot is that it's based on a really old Libreboot version and hasn't b=
een changed much since, and they've basically been in "development hell".=
 I sent them extensive patches fgixing build issues, so that it builds on=
 modern distros, andh I sent them patches updating it to newer upstream r=
evisions e.g. coreboot, but none of my patches were reviewed. I don't thi=
nk the current developers are up to the task, and this is not an insult; =
they worked under me as Libreboot contributors in the past, and they only=
 ever worked on minor tasks, they never did anything big. I've said in th=
e past that perhaps I should be appointed as leader of GNU Boot instead, =
but I have my Canoeboot project which has surpassed it technically in eve=
ry way, so now I want a GNU Canoeboot.

Upcoming work on Canoeboot:

* More ARM chromebooks, which Alper Nebi Yasak (libreboot developer) is w=
orking on

* More Dell Latitudes (GM45 modules). I'm working on these, based on the =
Dell E6400 port

* Mate Kukri (coreboot developer) is working on an exploit of Intel SA-00=
086 to gain unsigned code execution on Intel ME v11, for Skylake boards, =
but I'm told that similar exploits are possible and will be worked on, fo=
r older sandybridge, ivybridge and haswell hardware (e.g. ThinkPad X220, =
X230, T440p) - currently, the only blobs needed on those boards are Intel=
 ME and microcode, though they can boot without microcode. There's a chan=
ce that in the next few years, we will have what I call the Intel Freedom=
 Engine, a full free replacement of Intel ME. This would become part of G=
NU, if GNU accepts Canoeboot today.

* Linux-libre payload with musl libc and busybox, and U-Root, to provide =
booting of linux kernels on disk and over the network, from the flash. (G=
NU+Linux system in flash basically), with many security features such as =
measured boot, and native support for ZFS file system.

Some or all of the above, and more, will be present in Canoeboot this yea=
r.

So how about it?

Many people will be surprised by this email. But if you put your trust in=
 me, I promise I won't disappoint. I will of course make a savannah accou=
nt as part of this, and use it, if accepted.
</pre>
    <p></p>
    <pre class=3D"moz-signature" cols=3D"72">--=20
Company director, Minifree Ltd
Registered in England, No. 9361826 | VAT No. GB202190462
Registered Office: 19 Hilton Road, Canvey Island, Essex SS8 9QA, UK</pre>=

  </body>
</html>

--------------0qkEOS0KaVvTlm4TzFss1Nsy--

--------------qXtx1QnbZel60GYrRxArVU6C
Content-Type: application/pgp-keys; name="OpenPGP_0x5C654067D383B1FF.asc"
Content-Disposition: attachment; filename="OpenPGP_0x5C654067D383B1FF.asc"
Content-Description: OpenPGP public key
Content-Transfer-Encoding: quoted-printable

-----BEGIN PGP PUBLIC KEY BLOCK-----

xsFNBGWN15sBEADECGPEe37tdU3xe7OshKU19xVOPuJRMveCO5DHfv/lsZMXLWXw
MMpbG+2xSMQZcdZc0HCUq6TQE9fU0rA3kcFz0miMOuB2WJbYy9guvg9pAjLa0LUy
b2T/HPDDy0ifYtqrOzwETwWRiWQcTHjJ0knwNReaEterpPki1MbK79EwSuQBIgq9
lQ611qLn5SmE7sBRB5kze7q3KdTvY/CTfvOpVizgRF8kqqG4r4XkI0dTyrvC3i3E
ub3F3YPWNjN06rECG6wO+TPzRo7em+0CdPYDgtqq4Srf050KNZsVt10Plty5VpJm
2GfoXFh6SZBO1zSbBpTGU+7vBsR731ye2ouQdcIs06Qi4wHmJ71liqJwxZ0ju2F5
edC7jDzdk4jAIaCiSiU+iGg28RsxoUdLkJl5Q4yW507Gr0psHIBJBAWJo1i75qKV
hrmN25xjJLv0MjgaR7RgT1T7uuX1KPuo8NbHbRlkIv7987NeJzgbUzzpka2MJjTE
d1ova9kPyICVmKCfBnT+bO3vfJAuQXRlf3qjXSLsxCD7Jmu07if0jXFIvjy/nC4H
0QPlwd/sVS7Svfn4rEGEnulrtBvVdOr6I+LmDedbSsSlYNlqagdyGsdKZfWSGxhj
fz4oAkVy+y39s1qAnM5191m+u72dmnQPtxI8lEH/G+j+hK8NxDvV5ri3owARAQAB
zR1MZWFoIFJvd2UgPGluZm9AbWluaWZyZWUub3JnPsLBlAQTAQoAPhYhBIux99KM
92ltv09xklxlQGfTg7H/BQJljdebAhsDBQkJZgGABQsJCAcCBhUKCQgLAgQWAgMB
Ah4BAheAAAoJEFxlQGfTg7H/PKoQAIB8z2Rg+R0417YRTBXvbVG5kPpKOO3DWUaQ
CJx6uypBUpgw2giKDsDz59c2vNaADs7Zh5xQ+2bzB+jkCjVSuzguApT3gxTnICvL
eM72d5ZEF6Q8/YC6s9IiIHssCujbxtNN5yDU/Kn9Qd3gb+Bn9t/ZYT+L/SGLU3Ze
rq57lSt8ixU7JOvAolgqRzaPwTFvi1GPZbE5Gynj9riTxZc3KLFROWYNiiO33T+X
17TepTUaubkoA3DgWcQ6tw2dsJ8MT0DtZH8KlU/ufq0NBfFIw79uCQ+m/GbiW9Sm
KtppayLUJyJndlf5fQ/NaNJw9RS+yF5ellGKWWAwh+Drv0PITzJ1dLeWOF5degty
n9+HaMSMRIs5G6m69UxZmLJ9TQF1Lt1u0l1EH7yHIAf69MX4nlcDIx6moIJgo2UJ
9u2D7odDUKlPG8X4lbc5cvGx9l/Hy6WF8dOYUiU1avU65uhggaNXGXC6JbDiChCe
uWTnzKXqlvae8rU9jSpDBSQFlOOgvi3NifDLM7xFByDtFabnJHniNO1B6kS0V0nv
6sJlRuB584kQUQ+PaRUj8QvXjsLvYhxYTw2G7jzQkXEOBZq3jenVzAg5ejOB7mTN
oZOYOWoFY7XDRUGOmGDvH2qQGVNhedhSBJilAiu6K0NyjkaFAGufvgXDjLImPav/
kWdbTDvNzsFNBGWN15sBEADj4Dhn29Z0LVU+B1Wc8U0wdV7NbDIjbhEvJ0Yc+FTN
orQq7avY9jTkGtcnKPUti6cJxuQOZPxaIzP1mMK9BlsGbB8bTJ7oqpsIXuvGKOOZ
QMb7i+qEIfJw6ZAXSwuKq4xBciJU52WdX8OaSWJ6KZX74yrE1SXW+7yj3ENZNWuT
N2kAPF595XYpZwTAaJRy/ojfORu8WFXvo5osZJI1TlrJeDFecBntPkCgvI1VPTF3
fUU3lGZc8rVascaGaJ6tBd5mTx/RrRyJrrJMEd1dca0MHV4WIDbNeINpbEhS2Sbq
Oj/9q9z8tfmleqXVjb+gKjSDpD8Nsxv9+2gq29tevMLqZ5R3NA4jj4rOnyIuq+YF
tkZuBuEc04UX3ApixdAQ0jINhIBfT8YcMC+wwTvl4jG4Rhzrc4jOX8igOe9wkstb
l7JxGJU28x4htskAyM2CMxPaUrorm64cU2S0UoJGrvLfjItGud8dyM+RgtaO5wTF
NO2A0dnq2/thIIgoEaiQfKMq2ilISsnf6x8as0FV3c7mGO8pDJlRwjoNg/89CMhx
VbgO+5JQ+JHLfoMA++R+QaYy1ZLY51h5Ax6OtVWpZh67EuVDLx/5EFiyM25UTpqv
H7t0ae2oLbAwQtIv1fRLhHP4aVSsP793of92/1lpybbpRD7/7ruVQ64d+/N49db6
mQARAQABwsF8BBgBCgAmFiEEi7H30oz3aW2/T3GSXGVAZ9ODsf8FAmWN15sCGwwF
CQlmAYAACgkQXGVAZ9ODsf8EqQ/8CjAQTza1pvd/GmKYHtldSA1odPB7AQkB+j4o
yu5gDqtM/fFRv3uGVYLcyZwC3XF69KL+NpcUYG22RQWokt+z3OiVYfP5LyKjXe2c
PMS2cmyXBHEcCP8QSffNQNoC2VYzaVXH4P6cBVmkDG3yPtQ2cH1Al6jhMS4Fa0TC
e6kgA7qROSoapdZuwbxEE7FeaYZIXQUBLpe2C6SexljD65DLhbDE5p4N56VbncO6
IAqr9JQSlEXBgvgXGhXqfOmZkmCjXJU7Sgy8sIyF4b1uEOkcWxUUfvfrO1yrcWyx
vTlZdPCJy802B7UVFPWTbKwFoyIq5Lr8wk2npzAmDy/hKZlIV72Vk0eIcrTCfpbj
0GehrIIYcpW7Z2IUtETNkyuQs+OScIdrP3PItajNwktQJrhz+UGbF9MuT+CHuruh
w5KzymkzcnEzCNaYvY4eG8IXP6VpX1GCs9eUvCWEnjP0OkmhQniWvS3vAB7DfZSm
0NAoDX0ZvNzzIRbiM5uhYqDUSIsmOwUiJLjveLysmIRaAc/K/Euml0obCUJfanD7
KSs7SZYp24ZTDNsvsWbsk1y8QvAGkZBfuykfRCMxsmGRyN2hS6H/ftttCUrj8Qn1
/hJDBegEUJgYDItHpE7KJeBHMFNUB2sTHPbzx+a5WPYxeYJevAeTwAg0/nRobXRz
ER7BLIU=3D
=3D29JQ
-----END PGP PUBLIC KEY BLOCK-----

--------------qXtx1QnbZel60GYrRxArVU6C--

--------------K2qUk5UHVXzKPCuKPN8riXWW--

--------------QXCa2wGHkDkyD669GgjjqNUT
Content-Type: application/pgp-signature; name="OpenPGP_signature.asc"
Content-Description: OpenPGP digital signature
Content-Disposition: attachment; filename="OpenPGP_signature.asc"

-----BEGIN PGP SIGNATURE-----

wsF5BAABCAAjFiEEi7H30oz3aW2/T3GSXGVAZ9ODsf8FAmZA10IFAwAAAAAACgkQXGVAZ9ODsf9i
zQ//YateZS8rcc4CKaqGwGpwOq+hepIigkQznYXi5c1p5gHdcVExi4RzkzfdwfOEmqzpMYfX7ivd
+zlgyEx0u6v/fDE2gtTUbT6p/17PI+aKw/mubMeA42jRTuey3jmOUf6fek5crpMkZyd//Q9iXGj1
LGQ6pZFhb2hk5RNCYU4n8Lv55LW0tlW4jToagkCxQ403ji6YXLugLbTfgctnP17dkuIvwO7vKL3D
xzzNa6joluoEvVPKwRVBycaWTaGLYVYXPO4CBiX9ZxaNWN1RZaxBL3t9ZHL3WZndzK4kymJtStAJ
TYjOGRQt0rQSh1DoNwSK//vFOMaJXjalQhmC6Qrf/1ee6sdTwg8V1SficuseJBkZ400RDowYUwg9
BFdzkRMVhHH+oauxOE+lhzPYMwteYTggy5oob/M5DCpZMSG5rz1ySx+1WtTANwkQ7XmLxbV7DVxN
vHR3FCvnsBvW7h5vqNWfUCkfIDeHKXPP+p85T/FW+BLYJeyAvPSPOnoDemsfowXxzkm1z/nc01ok
Vg4zARtSCjYXZZheD+0CgXZPDEnXavQC4ol4bD+/AZA3J8G24H6qf+QLs7cTD5putPwdwFTNZGJt
Br68vl8pzoLx6Rxu+e2JSbflhL7gNAkdCEbpClIQ+uU8fG/O69OaMWkoYEn1QzmOvH+YcvXWVPOK
nc0=
=JKOf
-----END PGP SIGNATURE-----

--------------QXCa2wGHkDkyD669GgjjqNUT--
```

That is all.

I anticipate that precisely nothing will happen; however, I am deadly serious,
and I would be quite pleased if they say yes.


```
flashprog -p linux_spi:dev=/dev/canoe0.0,canoespeed=gnu -w canoegnucanoegnu.gnu.rom
```

Follow-up
=========

In addition to the original message, I also sent this message to GNU Eval:

some people have actually asked me if i should contribute to GNU Boot, instead of trying to replace it with GNU Canoeboot. They have asked this outside of this email discussion, but some here may be wondering that.

I'd like now to address it. Here goes:

I actually submitted extensive patches to gnuboot in January 2024:

<https://lists.gnu.org/archive/html/gnuboot-patches/2024-01/index.html>

<https://web.archive.org/web/20240511074211/https://lists.gnu.org/archive/html/gnuboot-patches/2024-01/index.html>

The patches were never reviewed, let alone merged, but they:

* Replace "Libreboot" with "GNU Boot" in several places on the documentation (**THIS IS THE ONLY PATCH THEY MERGED**)
* Fix major build issues, allowing GNU Boot to build on modern distros (GNU Boot only builds on Trisquel 10, my patches make it build on 12. also Gentoo-libre and Arch/Parabola)
* Add Dell Latitude E6400 support
* Fix hang in GRUB caused when there is a stuck key, by disabling the "Unknown key" spew message
* ESP and btrfs subvol support in grub.cfg
* Fixes building KGPE-D16 on newer distros, by skipping GNAT which isn't needed
* Add gru bob support (rk3399 chromebooks, with free EC and *no* microcode) - ditto gru kevin
* Keyboard fix for GRUB: force it to use scancode set 2 translated, instead of untranslated set 2 to work around buggy ECs such as Dell Latitude E6400
* Avoid spewing the Unknown prefix message in GRUB
* Adds the dell-flash-unlock tool from Nicholas Chin, allowing internal flashing from factory BIOS to GNU Boot, on the E6400
* cache cbfstool/ifdtool builds to speed up build time
* better caching of coreboot rom images during build, to speed up build time
* Prevent future GRUB build errors by disabling -Werror
* Support for *building* U-Boot as a coreboot payload, on gru bob/kevin chromebooks (GNU Boot only archives it, doesn't build it)

A second patch set that I sent does the above, and also:

* Updates GRUB, coreboot and SeaBIOS to newer revisions from late 2023 (GNU Boot uses late 2021 revisions)
* Adds Argon2 KDF support, for booting from LUKS2-encrypted /boot (GNU Boot can't boot from encrypted LUKS2 /boot without this patch)
* Reduced the number of modules in GRUB to only those needed, saving 100KB of space in flash
* Update memtest86+ to v6.x instead of 5.x

I sent all of these patches to GNU Boot while bored, and it only took me 1 day to implement all of them, re-using what I had done in Canoeboot months beforehand

My patches fixed all of the fundamental issues with GNU Boot, without rewriting the build system; they use an older version of the Libreboot build system, prior to my re-write of the latter half of 2023 (my re-write makes the build system much smaller and more efficient.

All of the above improvements *and much more* is in Canoeboot. In terms of development, Canoeboot is about 2 years ahead of Canoeboot; Canoeboot has since far surpassed the improvements sent to GNU Boot in January, so even if they did merge them now, they'd still be behind.

I may be missing a thing or two, above, but one thing I'm not missing is my strong and stable commitment to the free software movement, even when I'm dealing with hostile project maintainers who won't even consider my patches.

This is why I will no longer assist the GNU Boot project. Because I tried to help them; and this wasn't my first attempt to help, either.

Whether GNU accepts Canoeboot or not, Canoeboot will continue to press full speed ahead. I say now it's 2 years ahead of GNU Boot;

Next year it'll be 3 years ahead.
