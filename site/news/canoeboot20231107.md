% Canoeboot 20231107 released!
% Leah Rowe in Canoe Leah Mode™
% 7 November 2023

Canoeboot is a *special fork* of [Libreboot](https://libreboot.org/), providing
a de-blobbed configuration on *fewer motherboards*; Libreboot supports more
hardware, and much newer hardware. More information can be found on Canoeboot's
[about](../about.html) page and by reading Libreboot's [Binary Blob Reduction Policy](https://libreboot.org/news/policy.html).

Open source BIOS/UEFI firmware
------------------------------

*This* new release, Canoeboot 20231107, released today 7 November 2023, is
based on the recent [Libreboot 20231106](https://libreboot.org/news/libreboot20231106.html) release.
The previous release was [Canoeboot 20231103](canoeboot20231103.md), released
on 3 November 2023. Today's release has focused
on minor bug fixes, plus tweaks to the GRUB payload. It imports certain fixes
from the Libreboot 20231106 release, relative to [Libreboot 20231101](https://libreboot.org/news/libreboot20231101.html).

Canoeboot provides boot firmware for supported x86/ARM machines, starting a
bootloader that then loads your operating system. It replaces proprietary
BIOS/UEFI firmware on x86 machines, and provides an *improved* configuration
on [ARM-based chromebooks](../docs/install/chromebooks.html) supported
(U-Boot bootloader, instead of Google's depthcharge bootloader). On x86
machines, the GRUB and SeaBIOS coreboot
payloads are officially supported, provided in varying configurations per
machine. It provides an [automated build system](../docs/maintain/) for the
[configuration](../docs/build/) and [installation](../docs/install/) of coreboot
ROM images, making coreboot easier to use for non-technical people. You can find
the [list of supported hardware](../docs/hardware/) in Canoeboot documentation.

Canoeboot's main benefit is *higher boot speed*,
[better](../docs/linux/encryption.md) 
[security](../docs/linux/grub_hardening.md) and more
customisation options compared to most proprietary firmware. As a
[libre](https://writefreesoftware.org/) software project, the code can be
audited, and coreboot does regularly audit code. The other main benefit
is [*freedom* to study, adapt and
share the code](https://writefreesoftware.org/), a freedom denied by most boot
firmware, but not Canoeboot! Booting Linux/BSD is also [well](../docs/linux/) 
[supported](../docs/bsd/).

Work done since last release
---------------------------

This is largely a bugfix release. Most notably, boot issues on GM45 thinkpads
present in the 20231103 release have been resolved.

### Dell E6400 on its own tree

Canoeboot contains a DDR2 raminit patch for Dell Latitude E6400, that increases
reliability on coldboot, but it negatively affects other GM45 machines that use
DDR3 RAM instead.

This board is no longer provided by `coreboot/default`. Instead, it is provided
by `coreboot/dell`, and the offending patch has been moved there, along with
other required patches.

This means that the Dell Latitude E6400 still works, and quite reliably, but
the patch for it will not impact other boards. In some special circumstances,
Canoeboot 20231103 randomly crashed or rebooted with certain memory modules,
when using on GM45 ThinkPads (ROM images for those machines were then deleted
from those release archives). Today's Canoeboot release solves that problem,
so these machines can be used reliably once again (and ROM images are provided,
in this Canoeboot 20231107 release).

### Coreboot, GRUB, U-Boot and SeaBIOS revisions

Canoeboot 20231107 and 20231103 are both based on these revisions:

* Coreboot (default): commit ID `d862695f5f432b5c78dada5f16c293a4c3f9fce6`, 12 October 2023
* Coreboot (dell): commit ID `d862695f5f432b5c78dada5f16c293a4c3f9fce6`, 12 October 2023
  (the `dell` tree does not exist in Canoeboot 20231103, only 20231107)
* Coreboot (fam15h\_udimm): commit ID `1c13f8d85c7306213cd525308ee8973e5663a3f8`, 16 June 2021
* GRUB: commit ID `e58b870ff926415e23fc386af41ff81b2f588763`, 3 October 2023
* SeaBIOS: commit ID `1e1da7a963007d03a4e0e9a9e0ff17990bb1608d`, 24 August 2023
* U-Boot: commit ID `4459ed60cb1e0562bc5b40405e2b4b9bbf766d57`, 2 October 2023

Several other fixes and tweaks have been made, in addition to this and the
E6400 patch mentioned above.

Build system tweaks
----------------

These changes were made:

* Documentation now included under `docs/`, not `src/docs/`, in releases,
  including this one.
* nvmutil: Support `make install`, so now nvmutil can be *installed* to the host.
  Patch courtesy of Riku Viitanen.
* Use the UK Mirror Service (University of Kent) mirror for GCC downloads and
  other GNU toolchain components, when building coreboot. The GNU HTTP server
  returns 302 status, redirecting to mirrors, but this 302 redirect often fails.
* Dell Latitude E6400: Set VRAM to maximum (256MB), rather than 32MB.
* GRUB: Don't print an error message about missing modules. We remove a lot of
  modules in GRUB, because we only need a few, but many GRUB configuration files
  provided by distros will just load GRUB modules frivilously, that we don't
  have. In almost all cases, the user can still boot even in such situations.
* GRUB: Don't print the missing prefix error on screen (normally seen when
  running GRUB in text mode). The prefix is actually being set, and these
  messages are benign. The message just annoys/confuses some people, so it has
  been hidden by modifying GRUB not to display it at all.
* GM45 and i945 coreboot configurations were re-made from scratch. In testing,
  certain defaults set by upstream were being overridden unnecessarily by cbmk,
  so they were re-made from scratch. When Canoeboot updates the coreboot
  revision, `make oldconfig` is used on each configuration file, which is
  usually OK, but every now and then we re-do them again. (the alternative is
  to use defconfigs, not full configs, but that has other drawbacks)
* Untitled static site generator *documentation* also included in the release.
* Untitled static site generator now included in releases, alongside the
  included Canoeboot documentation (website files, and images).

The following additional commits were picked, which are present in Libreboot's
Git repository *after* the Libreboot 20231106 release upon which
Canoeboot 20231107 is based:

```
c4d90087 add grub mods: diskfilter,hashsum,loadenv,setjmp
d0d6decb re-add grub modules: f2fs, json, read, scsi, sleep
86608721 nvmutil: print usage
f12f5c3a nvmutil: fix makefile
```

This is a very conservative changelog, because this is largely a bugfix release.

Hardware supported in this release
--------------------------------

All of the following are believed to *boot*, but if you have any issues,
please contact the Canoeboot project. They are:

### Servers (AMD, x86)

-   [ASUS KFSN4-DRE motherboard](../docs/hardware/kfsn4-dre.md)
-   [ASUS KGPE-D16 motherboard](../docs/hardware/kgpe-d16.md)

### Desktops (AMD, Intel, x86)

-   [Gigabyte GA-G41M-ES2L motherboard](../docs/hardware/ga-g41m-es2l.md)
-   [Acer G43T-AM3](../docs/hardware/acer_g43t-am3.md)
-   [Intel D510MO and D410PT motherboards](../docs/hardware/d510mo.md)
-   [Apple iMac 5,2](../docs/hardware/imac52.md)
-   [ASUS KCMA-D8 motherboard](../docs/hardware/kcma-d8.md)
-   [Intel D945GCLF](../docs/hardware/d945gclf.md)

### Laptops (Intel, x86)

-   **[Dell Latitude E6400](../docs/hardware/e6400.md) (easy to flash, no disassembly, similar
    hardware to X200/T400)**
-   ThinkPad X60 / X60S / X60 Tablet
-   ThinkPad T60 (with Intel GPU)
-   [Lenovo ThinkPad X200 / X200S / X200 Tablet](../docs/hardware/x200.md)
-   Lenovo ThinkPad X301
-   [Lenovo ThinkPad R400](../docs/hardware/r400.md)
-   [Lenovo ThinkPad T400 / T400S](../docs/hardware/t400.md)
-   [Lenovo ThinkPad T500](../docs/hardware/t500.md)
-   [Lenovo ThinkPad W500](../docs/hardware/t500.md)
-   [Lenovo ThinkPad R500](../docs/hardware/r500.md)
-   [Apple MacBook1,1 and MacBook2,1](../docs/hardware/macbook21.md)

### Laptops (ARM, with U-Boot payload)

-   [ASUS Chromebook Flip C101 (gru-bob)](../docs/install/chromebooks.md)
-   [Samsung Chromebook Plus (v1) (gru-kevin)](../docs/install/chromebooks.md)

Downloads
---------

You can find this release on the downloads page. At the time of this
announcement, some of the rsync mirrors may not have it yet, so please check
another one if your favourite one doesn't have it.

Errata
------

### Update on 12 November 2023:

This file was also overlooked, and is still present in the release tarball:

* `src/vendorcode/amd/agesa/f12/Proc/GNB/Nb/Family/LN/F12NbSmuFirmware.h`

This has now been removed, in the Canoeboot git repository (`cbmk.git`), and
this file will absent, in the next release after Canoeboot 20231107. Thanks go
to Denis Carikli who reported this. The patch to fix it is here:

<https://codeberg.org/canoeboot/cbmk/commit/70d0dbec733c5552f8cd6fb711809935c8f3d2f3>

This fix will be present in the next release.
