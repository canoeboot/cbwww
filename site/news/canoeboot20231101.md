% Canoeboot 20231101 released!
% Leah Rowe in Canoe Leah Mode™
% 1 November 2023

A corresponding [Libreboot 20231101
release](https://libreboot.org/news/libreboot20231101.html) is also available.
Simultaneous same-day release!

Introduction
============

*This* new release, Canoeboot 20231101, released today 1 November 2023, is
based on the [Libreboot 20231101](https://libreboot.org/news/libreboot20231101.html)
release, porting changes in it on top of
[Canoeboot 20231026](canoeboot20231026.md) as a base. The previous
release was Canoeboot 20231026, released on 26 October 2023.

Canoeboot provides boot firmware for supported x86/ARM machines, starting a
bootloader that then loads your operating system. It replaces proprietary
BIOS/UEFI firmware on x86 machines, and provides an *improved* configuration
on [ARM-based chromebooks](../docs/install/chromebooks.html) supported
(U-Boot bootloader, instead of Google's depthcharge bootloader). On x86
machines, the GRUB and SeaBIOS coreboot
payloads are officially supported, provided in varying configurations per
machine. It provides an [automated build system](../docs/maintain/) for the
[configuration](../docs/build/) and [installation](../docs/install/) of coreboot
ROM images, making coreboot easier to use for non-technical people. You can find
the [list of supported hardware](../docs/hardware/) in Canoeboot documentation.

Canoeboot's main benefit is *higher boot speed*,
[better](../docs/gnulinux/encryption.md) 
[security](../docs/gnulinux/grub_hardening.md) and more
customisation options compared to most proprietary firmware. As a
[libre](https://writefreesoftware.org/learn) software project, the code can be
audited, and coreboot does
regularly audit code. The other main benefit is [*freedom* to study, adapt and
share the code](https://writefreesoftware.org/), a freedom denied by most boot
firmware, but not Canoeboot! Booting Linux/BSD is also [well](../docs/gnulinux/) 
[supported](../docs/bsd/).

Canoeboot is maintained in parallel with Libreboot, and by the same developer,
Leah Rowe, who maintains both projects; Canoeboot implements the [GNU Free
System Distribution Guideline](https://www.gnu.org/distros/free-system-distribution-guidelines.en.html)
as policy, whereas Libreboot implements its own [Binary Blob Reduction
Policy](https://libreboot.org/news/policy.html).

Work done since last release
============================

This changelog is based on the Libreboot 20231101 changelog; changes from
Libreboot 20231101 that are suitable for Canoeboot have been included in
this release, and so, this changelog has been modified (based on the
Libreboot one):

Coreboot, GRUB, U-Boot and SeaBIOS revisions
------------------------------------

Canoeboot 20231026 and 20231101 are both based on these revisions:

* Coreboot (default): commit ID `d862695f5f432b5c78dada5f16c293a4c3f9fce6`, 12 October 2023
* Coreboot (fam15h\_udimm): commit ID `1c13f8d85c7306213cd525308ee8973e5663a3f8`, 16 June 2021
* GRUB: commit ID `e58b870ff926415e23fc386af41ff81b2f588763`, 3 October 2023
* SeaBIOS: commit ID `1e1da7a963007d03a4e0e9a9e0ff17990bb1608d`, 24 August 2023
* U-Boot: commit ID `4459ed60cb1e0562bc5b40405e2b4b9bbf766d57`, 2 October 2023

However, Canoeboot 20231101 has added several new patches on top of several
of these, that fix certain bugs or improve certain functionalities. More
information is available elsewhere in this page.

Build system tweaks
===================

These changes were made:

* i945 bootblock copy: during ROM building, the bootblock must be copied from
  the main 64KB block, to the backup block, for use with `bucts`, but this
  wasn't done in the last release; this regression has now been fixed.
* Re-add SeaGRUB support - enabled on Dell Latitude E6400.
* Export `LC_COLLATE` and `LC_ALL`, setting it to `C`, to ensure
  consistent sorting; the Canoeboot build system heavily relies on sorting
  by alphanumerical order with numbers and capital letters first, e.g. when
  applying patches.
* GRUB config: Re-wrote the text on some entries to make them easier to
  understand for novice users.
* GRUB: Don't spew the "Unknown key" message on unknown/invalid key presses -
  otherwise, a faulty keyboard with stuck keys will make the message spew
  repeatedly, making GRUB unusable.
* Revert a heapsize patch in coreboot that broke S3 suspend/resume in some
  boards - if you do have issues, please send a bug report.
* Dell Latitude E6400: fixed keyboard initialisation in GRUB. GRUB
  was using scancode set 2 without translation. The EC on Dell Latitude E6400
  allows to set the scancodes and reports back what you set, but only ever
  actually uses scancode set 1 (XT) - so, the fix makes GRUB always use set 2
  with translation (treated as 1). This is the same behaviour used in SeaBIOS.
  This fixes the machine in GRUB, and doesn't break other boards as per testing.
* Fix coldboot on Dell Latitude E6400, courtesy of a patch by Angel Pons (IRC
  inick `hell` on coreboot IRC) - previously, this machine booted unreliably,
  though reboot was fine. Angel wrote a patch for another board, that also
  works on the E6400 quite nicely.
* QEMU ARM64: video console now enabled in U-Boot (courtesy Alper Nebi Yasak)
* Dependencies scripts: updated a few of them, for newer versions of distros.

This is only about 1 week's worth of changes; this Canoeboot release is
largely a bugfix release.

Hardware supported in this release
==================================

All of the following are believed to *boot*, but if you have any issues,
please contact the Canoeboot project. They are:

### Servers (AMD, x86)

-   [ASUS KFSN4-DRE motherboard](../docs/hardware/kfsn4-dre.md)
-   [ASUS KGPE-D16 motherboard](../docs/hardware/kgpe-d16.md)

Desktops (AMD, Intel, x86)
-----------------------

-   [Gigabyte GA-G41M-ES2L motherboard](../docs/hardware/ga-g41m-es2l.md)
-   [Acer G43T-AM3](../docs/hardware/acer_g43t-am3.md)
-   [Intel D510MO and D410PT motherboards](../docs/hardware/d510mo.md)
-   [Apple iMac 5,2](../docs/hardware/imac52.md)
-   [ASUS KCMA-D8 motherboard](../docs/hardware/kcma-d8.md)
-   [Intel D945GCLF](../docs/hardware/d945gclf.md)

### Laptops (Intel, x86)

-   **[Dell Latitude E6400](../docs/hardware/e6400.md) (easy to flash, no disassembly, similar
    hardware to X200/T400)**
-   ThinkPad X60 / X60S / X60 Tablet
-   ThinkPad T60 (with Intel GPU)
-   [Lenovo ThinkPad X200 / X200S / X200 Tablet](../docs/hardware/x200.md)
-   Lenovo ThinkPad X301
-   [Lenovo ThinkPad R400](../docs/hardware/r400.md)
-   [Lenovo ThinkPad T400 / T400S](../docs/hardware/t400.md)
-   [Lenovo ThinkPad T500](../docs/hardware/t500.md)
-   [Lenovo ThinkPad W500](../docs/hardware/t500.md)
-   [Lenovo ThinkPad R500](../docs/hardware/r500.md)
-   [Apple MacBook1,1 and MacBook2,1](../docs/hardware/macbook21.md)

### Laptops (ARM, with U-Boot payload)

-   [ASUS Chromebook Flip C101 (gru-bob)](../docs/install/chromebooks.md)
-   [Samsung Chromebook Plus (v1) (gru-kevin)](../docs/install/chromebooks.md)

Downloads
=========

You can find this release on the downloads page. At the time of this
announcement, some of the rsync mirrors may not have it yet, so please check
another one if your favourite one doesn't have it.

Post-release errata
===================

The following binary blobs were overlooked, and are still present in the
release archive for Canoeboot 20231101 and 20231026; this mistake was
corrected, in the [Canoeboot 20231103 release](canoeboot20231103.md), so you
should use that if you don't want these files. They are, thus:

* `src/coreboot/default/3rdparty/stm/Test/FrmPkg/Core/Init/Dmar.h`
* `src/coreboot/fam15h_rdimm/src/vendorcode/intel/fsp1_0/baytrail/absf/minnowmax_1gb.absf`
* `src/coreboot/fam15h_rdimm/src/vendorcode/intel/fsp1_0/baytrail/absf/minnowmax_2gb.absf`
* `src/coreboot/fam15h_udimm/src/vendorcode/intel/fsp1_0/baytrail/absf/minnowmax_1gb.absf`
* `src/coreboot/fam15h_udimm/src/vendorcode/intel/fsp1_0/baytrail/absf/minnowmax_2gb.absf`
* `src/pico-sdk/lib/tinyusb/hw/mcu/nordic/nrf5x/s140_nrf52_6.1.1_API/include/ble.h`
* `src/pico-sdk/lib/tinyusb/hw/mcu/nordic/nrf5x/s140_nrf52_6.1.1_API/include/ble_err.h`
* `src/pico-sdk/lib/tinyusb/hw/mcu/nordic/nrf5x/s140_nrf52_6.1.1_API/include/ble_gap.h`
* `src/pico-sdk/lib/tinyusb/hw/mcu/nordic/nrf5x/s140_nrf52_6.1.1_API/include/ble_gatt.h`
* `src/pico-sdk/lib/tinyusb/hw/mcu/nordic/nrf5x/s140_nrf52_6.1.1_API/include/ble_gattc.h`
* `src/pico-sdk/lib/tinyusb/hw/mcu/nordic/nrf5x/s140_nrf52_6.1.1_API/include/ble_gatts.h`
* `src/pico-sdk/lib/tinyusb/hw/mcu/nordic/nrf5x/s140_nrf52_6.1.1_API/include/ble_hci.h`
* `src/pico-sdk/lib/tinyusb/hw/mcu/nordic/nrf5x/s140_nrf52_6.1.1_API/include/ble_l2cap.h`
* `src/pico-sdk/lib/tinyusb/hw/mcu/nordic/nrf5x/s140_nrf52_6.1.1_API/include/ble_ranges.h`
* `src/pico-sdk/lib/tinyusb/hw/mcu/nordic/nrf5x/s140_nrf52_6.1.1_API/include/ble_types.h`
* `src/pico-sdk/lib/tinyusb/hw/mcu/nordic/nrf5x/s140_nrf52_6.1.1_API/include/nrf_error.h`
* `src/pico-sdk/lib/tinyusb/hw/mcu/nordic/nrf5x/s140_nrf52_6.1.1_API/include/nrf_error_sdm.h`
* `src/pico-sdk/lib/tinyusb/hw/mcu/nordic/nrf5x/s140_nrf52_6.1.1_API/include/nrf_error_soc.h`
* `src/pico-sdk/lib/tinyusb/hw/mcu/nordic/nrf5x/s140_nrf52_6.1.1_API/include/nrf_nvic.h`
* `src/pico-sdk/lib/tinyusb/hw/mcu/nordic/nrf5x/s140_nrf52_6.1.1_API/include/nrf_sdm.h`
* `src/pico-sdk/lib/tinyusb/hw/mcu/nordic/nrf5x/s140_nrf52_6.1.1_API/include/nrf_soc.h`
* `src/pico-sdk/lib/tinyusb/hw/mcu/nordic/nrf5x/s140_nrf52_6.1.1_API/include/nrf_svc.h`
* `src/pico-sdk/lib/tinyusb/hw/mcu/nordic/nrf5x/s140_nrf52_6.1.1_API/include/nrf52/nrf_mbr.h`

Thanks go to Craig Topham, who is the Copyright and Licensing Associate at the
Free Software Foundation; you can find his entry on the [FSF staff
page](https://www.fsf.org/about/staff-and-board). Craig is the one who reported
these.

The Canoeboot 20231026 and 20231101 release tarballs will not be altered, but
errata has now been added to the announcement pages for those releases, to let
people know of the above issue.

You are advised, therefore, to use the [Canoeboot 20231103
release](canoeboot20231103.md).

Update on 12 November 2023:
---------------------------

This file was also overlooked, and is still present in the release tarball:

* `src/vendorcode/amd/agesa/f12/Proc/GNB/Nb/Family/LN/F12NbSmuFirmware.h`

This has now been removed, in the Canoeboot git repository (`cbmk.git`), and
this file will absent, in the next release after Canoeboot 20231107. Thanks go
to Denis Carikli who reported this. The patch to fix it is here:

<https://codeberg.org/canoeboot/cbmk/commit/70d0dbec733c5552f8cd6fb711809935c8f3d2f3>
