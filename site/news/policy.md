% Binary Blob Extermination Policy
% Leah Rowe
% 10 January 2025

This page describes Canoeboot's policy of *de-blobbing*. It is in stark
contrast to [Libreboot's Binary Blob Reduction
Policy](https://libreboot.org/news/policy.html); Libreboot removes proprietary
code, replacing it with Free Software whenever possible, but also supports
much newer hardware than Canoeboot, and certain vendor code is still required
on many newer machines. In practise, the level of software freedom you get with
Libreboot is far greater than you would otherwise get, and Libreboot *also*
deletes every blob, on the same hardware that Canoeboot supports.

The only minor difference, on boards that both projects support, is that
Libreboot includes CPU microcode updates by default. This is required, for
stability; Canoeboot can be unstable at times, on some machines, since it
excludes microcode updates entirely.

Canoeboot, then, is a more dogmatic approach to the same problem, of how to
provide users with fully free boot firmware. It is dogmatic, to the point of
being *pedantic*, but it provides a viable solution for Free Software purists.

The policy that you're about to read, the one Canoeboot uses, is the same one
that Libreboot previously used. Libreboot adopted the *Binary Blob Reduction
Policy* on 17 November 2022, but Libreboot has existed since December 2013.
Canoeboot started in October 2023, because of a minority of users that still
demanded such a project exist as the old Libreboot did.

Free as in freedom!
-----------------

Canoeboot intentionally *de-blobs* coreboot, which is to say that it does not
include binary blobs. The coreboot software otherwise requires binary blobs on
some of the systems that it has support for. Canoeboot's version of coreboot is
entirely *free*, on its consequently reduced set of supported motherboards.

It was decided that a formal policy should be written, because there is quite
a bit of nuance that would otherwise not be covered. Canoeboot's policies in
this regard were previously ill defined.

It is important to define *how* Canoeboot distinguishes binary blobs, and how
they are removed. You can also read more about the de-blobbing process
on the [about](../about.md) page.

Background information
----------------------

Canoeboot concerns itself only with what goes in the main boot flash IC, but
there are other pieces of firmware to take into consideration, as covered
in the [Canoeboot FAQ](../faq.md#what-other-firmware-exists-outside-of-canoeboot).

Most critical of these are:

* Embedded controller firmware
* HDD/SSD firmware
* Intel Management Engine / AMD PSP firmware

Specific binary blobs are also problematic, on most coreboot systems, but they
differ per machine. Canoeboot *excludes* binary blobs in releases, so it only
supports a handful of machines from coreboot.

For information about Intel Management Engine and AMD PSP, refer to the FAQ.

So what *is* Canoeboot's policy?
-------------------------

Canoeboot follows a very conservative and *light touch* approach, when it comes
to deblobbing coreboot.

Canoeboot only excludes *software* binary blobs, plus CPU microcode updates.
*In practise, it is mostly microcode
updates that Canoeboot's build system deletes, along with coreboot Git history
so that no traces remain of old revisions; older revisions had many blobs in
the main repository, but modern coreboot moved almost all of them to third
party submodule repositories.*.

*Non-software* blobs are permitted, so long as they are in an easily understood
and/or well-documented format. For example, DDR training data is permitted
(patterns used during memory controller initialization, specifically training,
where the precise timings for the RAM are brute-forced); this is not software.

SPD data stored in the coreboot Git repository is in all cases some format
that's simply more efficient to store as a binary, in a format that is in fact
known/understood (see: coreboot source code and data sheets); in many cases,
there's only *one* correct way to write such data, making even the question of
copyright a moot point. Data is data, and code is code; the two are *separate*.

Non-software blobs must be redistributable under a free license, and must not
be encumbered by DRM, or they will not be included in Canoeboot.

Logic (in coreboot) for *loading or executing* binary blobs should not
be removed/disabled. Canoeboot merely *excludes* the blobs themselves. Most
of the blobs that Canoeboot removes (when downloading coreboot, in the build
system) are CPU microcode updates; Canoeboot leaves the code for loading
microcode updates intact, and you can in fact insert microcode updates into
your ROM image. This behaviour is intentional, and must not be removed. The
only job Canoeboot has is to not *distribute* those blobs itself!

*That's all*. Furthermore, Canoeboot must only support systems where *all* of
the main boot flash can be free. For example, ivybridge and sandybridge intel
platforms are completely libre in coreboot, but you still need neutered Intel
ME firmware in the flash, making those machines unsuitable for Canoeboot.

Other firmware, such as Embedded Controller firmware, is currently outside the
scope of the Canoeboot project, but not due to lack of desire; rather, these
are not yet possible on most supported or otherwise capable platforms, at least
not with free software. Other examples of firmware outside of the main boot
flash is covered in the Canoeboot FAQ.

More detailed insight about microcode
---------------------------------

To be clear: it is preferable that microcode be free. The microcode on Intel
and AMD systems *are* non-free. Facts and feelings rarely coincide; the
purpose of this section is to spread *facts*.

Not including CPU microcode updates is an absolute disaster for system
stability and security, and yet, this is one of Canoeboot's key policies.

The CPU already has microcode burned into mask ROM. The microcode configures
logic gates in the CPU, to implement an instruction set, via special *decoders*
which are fixed-function; it is not possible, for example, to implement a RISCV
ISA on an otherwise x86 processor. It is only possible for the microcode to
implement x86, or *broken* x86, and the default microcode is almost always
*broken x86* on Intel/AMD CPUs; it is inevitable, due to the complexity of
these processors.

These processors provide a way to supply microcode *updates*. These updates
are volatile, and consequently must be applied during every boot cycle. The
updates fix stability/reliability/security bugs, and their *absence*
is *technically incorrect*, but Canoeboot excludes them anyway, because that is
Canoe policy. Examples of where these updates fix bugs: on ASUS KCMA-D8/KGPE-D16
and ThinkPad X200/T400/T500/W500/X200T/X200/R500/X301, the updates make
hardware-based virtualization (via `kvm`) completely stable, where it would
otherwise lead to a kernel panic. They allow those same thinkpads to be run with
high CPU usage and I/O (RAM usage), without crashing (otherwise, it's very
likely to encounter a kernel panic caused by a
[Machine Check Exception](../faq.html#machine-check-exceptions-on-some-montevina-penryn-cpu-laptops)).

Not including these updates will result in an unstable/undefined state. Intel
themselves define which bugs affect which CPUs, and they define workarounds, or
provide fixes in microcode. Based on this, software such as the Linux kernel
can work around those bugs/quirks. Also, upstream versions of the Linux kernel
can update the microcode at boot time (however, it is recommend still to do it
from coreboot, for more stable memory controller initialization or “raminit”).
Similar can be said about AMD CPUs.

Here are some examples of where lack of microcode updates affected Canoeboot,
forcing Canoeboot to work around changes made upstream in coreboot, changes
that were *good* and made coreboot behave in a more standards-compliant manner
as per Intel specifications. Canoeboot had to *break* coreboot to retain
certain other functionalities, on some GM45/ICH9M thinkpads:

<https://browse.libreboot.org/lbmk.git/plain/resources/coreboot/default/patches/0012-fix-speedstep-on-x200-t400-Revert-cpu-intel-model_10.patch?id=9938fa14b1bf54db37c0c18bdfec051cae41448e>

<https://browse.libreboot.org/lbmk.git/plain/resources/coreboot/default/patches/0018-Revert-cpu-intel-Configure-IA32_FEATURE_CONTROL-for-.patch?id=4b7be665968b67463ec36b9afc7e8736be0c9b51>

These patches revert *bug fixes* in coreboot, fixes that happen to break other
functionality but only when microcode updates are excluded. The most
technically correct solution is to *not* apply the above patches, and instead
supply microcode updates!

Pick your poison. Canoeboot does not disable the mechanism in coreboot to load
these updates. At boot time, coreboot can supply such updates to the CPU, if
present in CBFS. Canoeboot merely excludes them, but you can add them to your
Canoeboot ROM image. *Libreboot* includes CPU microcode
updates **by default**,
because there's no other way to achieve stability, and they fix security updates.

You *need* microcode updates, or you will have a broken CPU; broken, because
it literally behaves differently than it's supposed to, so software will have
unpredictable bugs that could even cause data corruption - or worse.
