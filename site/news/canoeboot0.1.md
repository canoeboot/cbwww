% Canoeboot v0.1 released!
% Leah Rowe in Canoe Leah Mode™
% 27 January 2024

This is a special release. It is *not* based on the recent
[Libreboot 20240126 release](https://libreboot.org/news/libreboot20240126.html).
No, it is very special indeed.

I have recently sent GNU Boot some patches for their 0.1 RC3 release, fixing
it so that it compiles on modern distros. Their release only compiled on really
old distros like Debian 10 or Trisquel 10. I made it compile on the latest
Gentoo, Arch and Debian(Sid) as of 14 January 2024. I also added Dell Latitude
E6400, gru\_bob and gru\_kevin chromebooks. I also added several initialisation
fixes for keyboards in their GRUB payload, in addition to EFI System Partition
support in their `grub.cfg` - in other words, I've backported several fixes
and improvements from Canoeboot and Libreboot, to their project.

I did this, purely for fun, to see if it was technically feasible. And it was.
I sent these patches and they are now under review by the GNU people.

As you may know from reading this Canoeboot website, Canoeboot is vastly more
up to date than GNU Boot, using revisions 2 years newer (from 2023), whereas
GNU Boot uses old 2021 coreboot, GRUB and SeaBIOS revisions. It does not contain
improvements such as GRUB argon2 support.

Well, purely for fun, I made this special Canoeboot v0.1 release, which re-uses
the same *old* 2021 revisions as GNU Boot 0.1 RC3, but with my special fixes
as mentioned above (so, it has E6400/gru\_bob/gru\_kevin, and builds on modern
distros). However, that release is compiled using Canoeboot's build system,
which is vastly more efficient than the GNU Boot one (about twice as fast, and
less error prone, due to optimisations made during the four Libreboot build
system audits of 2023).

You can find the Canoeboot v0.1 release on the mirrors, alongside regular
releases. It should boot and work perfectly, albeit it on those very old code
revisions. It is advised that you still use the November 2023 Canoeboot
release, for the time being. A proper Canoeboot update, based on
Libreboot 20240126 (which uses Coreboot revisions from January 2024) will be
done at a date in the near future.

Anyway, the fixes that I did were sent to the GNU Boot mailing list. Check
the `gnuboot-patches` mailing list archive from 16 January 2024.

GNU Boot 0.1 RC3 fixes:
<https://git.disroot.org/vimuser/gnuboot/commits/branch/0.1-fix-build-v3>

Canoeboot v0.1 branch:
<https://codeberg.org/canoeboot/cbmk/commits/branch/v0.1>

I also did another GNU Boot branch for fun, that updates it to the
October 2023 revisions used in Libreboot/Canoeboot releases from November 2023:
<https://git.disroot.org/vimuser/gnuboot/commits/branch/20240113-update-revs-3> \
...these patches were also sent, but it seems they still prefer to use my
Libreboot 20220710 release.

The GNU Boot 0.1 RC3 release is essentially Libreboot 20220710, with a few
minor changes, and Canoeboot v0.1 is essentially Libreboot 20220710 aswell,
but with *substantial* build system design changes (but the overall code
is identical, when analysing the binaries).

PS: I use a new GPG signing key on Libreboot releases now. Check the Libreboot
download page for it. At the time of writing, the new key is not listed on
the Canoeboot page but I used that key.


