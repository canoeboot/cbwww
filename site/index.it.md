---
title: Libero e Open Source BIOS/UEFI firmware
x-toc-enable: true
...

Canoeboot deriva da [Libreboot](https://libreboot.org/).
Il progetto *Canoeboot* fornisce avvio [libero](https://writefreesoftware.org/learn)
grazie al firmware basato su coreboot, sostituendo cosi', firmware BIOS/UEFI proprietario
su [alcune schede madri basate su Intel/AMD x86 o ARM](docs/install/#which-systems-are-supported-by-canoeboot),
in computer fissi e portatili. Inizializza l'hardware (controller di
memoria, CPU, periferiche) e avvia un bootloader per il tuo sistema operativo.
[Linux](docs/linux/) e [BSD](docs/bsd/) sono ben supportati.
L'aiuto e' disponibile sul canale IRC [\#canoeboot](https://web.libera.chat/#canoeboot)
su [Libera](https://libera.chat/).

<img tabindex=1 class="r" src="https://av.canoeboot.org/t60logo.jpg" /><span class="f"><img src="https://av.canoeboot.org/t60logo.jpg" /></span>

Canoeboot provides GNU boot loader "[GRUB](docs/linux/)" and SeaBIOS payloads
on x86/x86\_64
Intel/AMD motherboards, and a [U-Boot UEFI payload](docs/uboot/) *for coreboot*
on ARM64(Aarch64) motherboards.
An [x86/x86\_64 U-Boot UEFI payload](docs/uboot/uboot-x86.md) is also available
on some boards. The x86, x86\_64 and arm64 U-Boot payloads provide a lightweight
UEFI boot implementation, which can boot many Linux distros and BSD systems.
The SeaBIOS and GRUB payloads also boot Linux/BSD systems. Which one you use
depends on your preferences. Canoeboot's [design](docs/maintain/) incorporates
all of these boot methods in a single image, so you can choose which one you use
at boot time, and more payloads (e.g. Linux kexec payload) are planned for
future releases.

**ULTIMO RILASCIO: L'ultimo rilascio e' Canoeboot 20250107, rilasciato il 7 January 2025.
Vedi: [Canoeboot 20250107 annuncio di rilascio](news/canoeboot20250107.md).**

Per quale ragione utilizzare *Canoeboot*?
-----------------------------------------

Canoeboot ti permette [liberta'](https://writefreesoftware.org/learn) che non potresti ottenere
con altri firmware di boot, velocita' di avvio maggiori
e [migliore sicurezza](docs/linux/grub_hardening.md).
E' estremamente flessibile e [configurabile](docs/maintain/) per la maggior parte dei casi.

*Noi* crediamo nella liberta' di [studiare, condividere, modificare and usare
il software](https://writefreesoftware.org/), senza restrizione alcuna,
in quanto e' uno dei fondamentali diritti umani che chiunque deve avere.
In questo contesto, *il software libero* conta. La tua liberta' conta. La formazione personale conta.
[Il diritto di riparare](https://en.wikipedia.org/wiki/Right_to_repair) conta.
Molte persone usano firmware di boot proprietario (non-libero), anche se usano
un sistema operativo libero.
Firmware proprietari spesso [contengono](faq.html#intel) [vulnerabilita'](faq.html#amd),
e possono essere difettosi. Il progetto canoeboot venne fondato nel ottobre 2023, con lo scopo
prefissato di permettere che il firmware coreboot sia accessibile anche
per utenti con scarsa formazione tecnica.

Il progetto Canoeboot fa uso di [coreboot](https://www.coreboot.org/) per
[l'inizializzazione hardware](https://doc.coreboot.org/getting_started/architecture.html).
Coreboot e' notoriamente difficile da installare per utenti che hanno una scarsa formazione tecnica;
gestisce solo l'inizializzazione di base e successivamente carica un programma come
[payload](https://doc.coreboot.org/payloads.html) (ad esempio.
[GRUB](https://www.gnu.org/software/grub/),
[Tianocore](https://www.tianocore.org/)), i quali possono essere configurati a piacere.
*Canoeboot risolve questo problema*; e' una *distribuzione di coreboot* con un
[sistema di compilazione automatizzato](docs/build/) che produce *immagini ROM* complete, per una
installazione piu' robusta. Viene fornito con apposita documentazione.

Canoeboot non deriva da coreboot
--------------------------------

<img tabindex=1 class="l" style="max-width:25%;" src="https://av.vimuser.org/uboot-canoe.png" /><span class="f"><img src="https://av.vimuser.org/uboot-canoe.png" /></span>

In effetti, Canoeboot tenta di essere il piu' possibile simile alla versione *ufficiale* di coreboot,
per ogni scheda, ma con diversi tipi di configurazione forniti automaticamente dal sistema di
compilazione automatico di Canoeboot.

Esattamente come *Debian* e' una *distribuzione Linux*, Canoeboot e' una
*distribuzione coreboot*. Per fare un immagine ROM da zero, hai bisogno di esperienza necessaria
nel configurare coreboot, GRUB e qualunque altra cosa ti serve. Con *Canoeboot*,
che puoi scaricare da Git o da un archivio di codici sorgenti, puoi far partire `make`,
e questo mettera' su automaticamente le immagini ROM richieste. Un sistema di compilazione automatico,
chiamato `cbmk` (Canoeboot MaKe), mettera' su quelle immagini ROM automaticamente, senza troppi
interventi da parte dell'utente. Le configurazioni di base sono gia' state previste in precedenza.

Se avresti voluto compilare coreboot normalmente senza il sistema di compilazione
automatico di Canoeboot, ti troveresti ad affrontare molte piu' difficolta senza adeguate competenze
tecniche per produrre una configurazione funzionante.

I rilasci binari di Canoeboot forniscono immagini ROM precompilate,
che puoi semplicemente installare senza troppe conoscenze tecniche o abilita'
particolari ad eccezione del seguire [semplici istruzioni scritte per chiunque](docs/install/).
