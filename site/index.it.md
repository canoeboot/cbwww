---
title: Progetto Canoeboot
x-toc-enable: true
...

Il progetto *Canoeboot* fornisce avvio [libero e open source](https://writefreesoftware.org/learn)
grazie al firmware basato su coreboot, sostituendo cosi', firmware BIOS/UEFI proprietario
su [alcune schede madri basate su Intel/AMD x86 o ARM](docs/hardware/),
in computer fissi e portatili. Inizializza l'hardware (controller di
memoria, CPU, periferiche) e avvia un bootloader per il tuo sistema operativo.
[GNU+Linux](docs/gnulinux/) e [BSD](docs/bsd/) sono ben supportati.
L'aiuto e' disponibile sul canale IRC [\#canoeboot](https://web.libera.chat/#canoeboot)
su [Libera](https://libera.chat/).

<img tabindex=1 class="r" src="https://av.canoeboot.org/t60logo.jpg" /><span class="f"><img src="https://av.canoeboot.org/t60logo.jpg" /></span>

**ULTIMO RILASCIO: L'ultimo rilascio e' Canoeboot 20231107, rilasciato il 7 novembre 2023.
Vedi: [Canoeboot 20231107 annuncio di rilascio](news/canoeboot20231107.md).**

Canoeboot was *originally* named [nonGeNUine Boot](news/nongenuineboot20230717.html),
provided as a proof of concept for the [GNU Boot](https://libreboot.org/news/gnuboot.html)
or *gnuboot* project to use a more modern Libreboot base, but
they went in their own direction instead. Canoeboot development was continued,
and it maintains sync with the Libreboot project, as a parallel development
effort. See: [How are Canoeboot releases engineered?](about.md#how-releases-are-engineered)

Canoeboot adheres to the *GNU Free System Distribution Guidelines* as policy,
whereas Libreboot adheres to its own [Binary Blob Reduction
Policy](https://libreboot.org/news/policy.html). Canoeboot and Libreboot
are *both* maintained by the same person, Leah Rowe, sharing code back and forth.

Per quale ragione utilizzare *Canoeboot*?
-----------------------------------------

Canoeboot ti permette [liberta'](https://writefreesoftware.org/learn) che non potresti ottenere
con altri firmware di boot, velocita' di avvio maggiori
e [migliore sicurezza](docs/gnulinux/grub_hardening.md).
E' estremamente flessibile e [configurabile](docs/maintain/) per la maggior parte dei casi.

*Noi* crediamo nella liberta' di [studiare, condividere, modificare and usare
il software](https://writefreesoftware.org/), senza restrizione alcuna,
in quanto e' uno dei fondamentali diritti umani che chiunque deve avere.
In questo contesto, *il software libero* conta. La tua liberta' conta. La formazione personale conta.
[Il diritto di riparare](https://yewtu.be/watch?v=Npd_xDuNi9k) conta.
Molte persone usano firmware di boot proprietario (non-libero), anche se usano
[un sistema operativo libero](https://www.openbsd.org/).
Firmware proprietari spesso [contengono](faq.html#intel) [vulnerabilita'](faq.html#amd),
e possono essere difettosi. Il progetto canoeboot venne fondato nel ottobre 2023, con lo scopo
prefissato di permettere che il firmware coreboot sia accessibile anche
per utenti con scarsa formazione tecnica.

Il progetto Canoeboot fa uso di [coreboot](https://www.coreboot.org/) per
[l'inizializzazione hardware](https://doc.coreboot.org/getting_started/architecture.html).
Coreboot e' notoriamente difficile da installare per utenti che hanno una scarsa formazione tecnica;
gestisce solo l'inizializzazione di base e successivamente carica un programma come
[payload](https://doc.coreboot.org/payloads.html) (ad esempio.
[GRUB](https://www.gnu.org/software/grub/),
[Tianocore](https://www.tianocore.org/)), i quali possono essere configurati a piacere.
*Canoeboot risolve questo problema*; e' una *distribuzione di coreboot* con un
[sistema di compilazione automatizzato](docs/build/) che produce *immagini ROM* complete, per una
installazione piu' robusta. Viene fornito con apposita documentazione.

Canoeboot non deriva da coreboot
--------------------------------

<img tabindex=1 class="l" style="max-width:25%;" src="https://av.canoeboot.org/dip8/adapter.jpg" /><span class="f"><img src="https://av.canoeboot.org/dip8/adapter.jpg" /></span>

In effetti, Canoeboot tenta di essere il piu' possibile simile alla versione *ufficiale* di coreboot,
per ogni scheda, ma con diversi tipi di configurazione forniti automaticamente dal sistema di
compilazione automatico di Canoeboot.

Esattamente come *Alpine Linux* e' una *distribuzione Linux*, Canoeboot e' una
*distribuzione coreboot*. Per fare un immagine ROM da zero, hai bisogno di esperienza necessaria
nel configurare coreboot, GRUB e qualunque altra cosa ti serve. Con *Canoeboot*,
che puoi scaricare da Git o da un archivio di codici sorgenti, puoi far partire `make`,
e questo mettera' su automaticamente le immagini ROM richieste. Un sistema di compilazione automatico,
chiamato `cbmk` (Canoeboot MaKe), mettera' su quelle immagini ROM automaticamente, senza troppi
interventi da parte dell'utente. Le configurazioni di base sono gia' state previste in precedenza.

Se avresti voluto compilare coreboot normalmente senza il sistema di compilazione
automatico di Canoeboot, ti troveresti ad affrontare molte piu' difficolta senza adeguate competenze
tecniche per produrre una configurazione funzionante.

I rilasci binari di Canoeboot forniscono immagini ROM precompilate,
che puoi semplicemente installare senza troppe conoscenze tecniche o abilita'
particolari ad eccezione del seguire [semplici istruzioni scritte per chiunque](docs/install/).
