---
title: Contatti
x-toc-enable: true
...

Supporto utenti
---------------

IRC o Reddit sono consigliati, sebbene preferiamo che usi il canale IRC
per avere o per offrire supporto tecnico. Continua a leggere per avere
ulteriori informazioni.

Mailing list
------------

No mailing lists at present.

Discussione sullo sviluppo
--------------------------

Per ora dai un occhiata sulla
[pagina Git](git.md) per avere maggiori informazioni su come puoi
assistere con lo sviluppo.

Su quella stessa pagina puoi trovare informazioni su come inviare
correzioni (patches) tramite pull requests.

Canale IRC
----------

IRC e' il modo principale per contattare chi collabora con il progetto Canoeboot.
Il canale ufficiale e' `#canoeboot` su Libera IRC.

Webchat:
<https://web.libera.chat/#canoeboot>

Libera e' una tra le piu' grandi reti IRC usate per i progetti di software libero.
Maggiori informazioni le trovi qui: <https://libera.chat/>

Puoi usare il client IRC che preferisci (come weechat or irssi) usando le seguenti informazioni:

* Server: `irc.libera.chat`
* Canale: `#canoeboot`
* Porta (TLS): `6697`
* Porta (non-TLS): `6667`

Ti suggeriamo di usare la porta `6697` e ablitare la cifratura TLS...

Inoltre ti suggeriamo di abilitare l'autenticazione SASL. Le pagine web
di Libera spiegano come:

* Guida per WeeChat: <https://libera.chat/guides/weechat>
* Guida per Irssi: <https://libera.chat/guides/irssi>
* Guida per HexChat: <https://libera.chat/guides/hexchat>

Comunque dovresti sempre controllare la documentazione del tuo client IRC preferito.

Reti sociali online
-------------------

Canoeboot esiste ufficialmente in molte piattaforme.

### Mastodon

Il fondatore e sviluppatore principale, Leah Rowe, e' su Mastodon:

* <https://mas.to/@libreleah>

### Posta elettronica

Leah puo' essere contattata anche via email a questo indirizzo:
[leah@libreboot.org](mailto:leah@libreboot.org)

### Reddit

Usato principalmente come canale di supporto e per annunciare notizie:
<https://www.reddit.com/r/canoeboot/>
